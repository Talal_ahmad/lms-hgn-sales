<!-- old "floor's rooms store" method some commented code -->
// dd($request->all());
        // dd($request->floors[0]['rooms'][0]['images']);
        // dd($request->floors[0]['rooms'][0]['images'][0]->getClientOriginalName());
        // dd($request->floors[0]['rooms'][0]['images'][0]->hashName());
        // dd($request->floors[0]['rooms'][0]['images'][0]->extension());

        // dd(md5(rand(1000,10000)));

        // $image = $request->floors[0]['rooms'][0]['images'][0];
        // $imageNewName = $image->hashName(); // Generate a unique, random name (it includes extension)

        // $path = $image->storeAs("images/rooms", $imageNewName, "public");
        // $imagesNameArray[] = $imageNewName;
        // dd($path, $imagesNameArray);


// $roomObj = $floorObj->rooms()->create($room);


// store Other Images
                        if (!empty ($room['other_images'])) {

                            $imagesNameArray = [];

                            foreach ($room['other_images'] as $image) {

                                // $ext = $image->extension();
                                // $imageNewName = md5(rand(1000,10000)) + date_create() + '-' + $ext;

                                $imageNewName = $image->hashName(); // Generate a unique, random name (it includes extension)

                                // storeAs() accepts 'path relative to disk root path', 'file name', 'disk name'
                                // returns the path as sepcified in storeAs() method (i.e., 'path relative to disk root path') but not the complete absolute or relative path
                                $path = $image->storeAs("images/rooms", $imageNewName, "public");
                                $imagesNameArray[] = $imageNewName;
                            }

                            $roomObj->other_images = implode("|", $imagesNameArray);

                        }
<!-- old "floor's rooms edit" method some commented code -->

$data = [];
        // $data["propertyType"] = $property->propertyType->name;
        // $data["floor"] = $floor;
        // $data["floor"] = $floor->load(["rooms", "tables"]); // will send one empty string of "empty relationship model"

        // if propertyType is "Hotel", send roomTypes
        if ($property->propertyType->name === "Hotel") {

            $data["roomTypes"] = RoomType::all(["id", "name"]);
            $data["floor"] = $floor->load("rooms");
            // $data["rooms"] = $floor->rooms;  // with it, rooms are sent 2 times, one with "$floor->rooms (rooms)" and one with "$floor (floor.rooms)"


// "storage_path()" gives the absolute "File System Path" but the "asset()" gives the "url" for using in "img,link,script, etc" tags in frontend
// for "asset()", you need to create a Symbolic Link other than the default "public" one present in "filesystems.php"

// dd(storage_path('app/public/images/rooms/'));
        // dd(storage_path('app/public/images/rooms/fu32i4WZ11lPnZ21xtMOWs2ddCWWGS5IZ1R8M7a7.jpg'));

<!-- old "floor's rooms update" method deleted code -->
// $roomObj = $floor->rooms()->where('id', $room['id'])->first();

// // update Cover Image
    // function update_cover_image($floor, $roomId, $coverImage) {

    //     dd($floor);
    //     $path = storage_path('app/public/images/rooms/');

    //     // check if old Cover Image is already present in "database"
    //     $room = $floor->rooms()->where('id', $roomId)->first();
    //     // dd($room);
    //     // dd($room->cover_image);

    //     if (!empty($coverImage)) {                        

    //         if ($room->cover_image) {
    //             // delete old Cover Image, if it is already present in "storage"
    //             if(Storage::disk('public')->exists($path . $room->cover_image)) {  
    //                 Storage::disk('public')->delete($path .  $room->cover_image);
    //             }
    //         }
    //         // change image name and Store it in Storage and return "image name"
    //         $coverImageName = "cover-" . $coverImage->hashName(); // Generate a unique, random name (it includes extension)
    //         $coverImage->storeAs("images/rooms", $coverImageName, "public");
    //         return $coverImageName;
    //     }
    //     return $room->cover_image;
    //     // return $room->cover_image ?? null;
    // }

// update Other Images
                    // function update_other_images($floor, $roomId, $otherImages) {

                    //     $path = storage_path('app/public/images/rooms/');

                    //     if (!empty($otherImages)) {                        

                    //         // check if old Other Images are already present in "database"
                    //         $room = $floor->rooms()->where('id', $roomId)->first();

                    //         if ($room->other_images) {
                    //             foreach (explode("|", $room->other_images) as $imageName) {
                    //                 // delete old Other Images, if they are already present in "storage"
                    //                 if(Storage::disk('public')->exists($path . $imageName)) {  
                    //                     Storage::disk('public')->delete($path .  $imageName);
                    //                 }
                    //             }
                    //         }
                    //         // change images name and Store them in Storage and return "images name string"
                    //         $imagesNameArray = [];

                    //         foreach ($otherImages as $image) {
                    //             $imageNewName = $image->hashName(); // Generate a unique, random name (it includes extension)
                    //             $image->storeAs("images/rooms", $imageNewName, "public");
                    //             $imagesNameArray[] = $imageNewName;
                    //         }
                    //         return implode("|", $imagesNameArray);
                    //     }
                    // }

// dd($request->rooms, ['result' => 'rooms are not present in request']);

<!-- trying with separate Images Table to store or update Room's Images -->
// $roomObj->images()->updateOrCreate(
                            //     ["room_id" => $roomObj->id],
                            //     [
                            //         "cover" => $roomObj->images->cover,
                            //         // convert the array into string with separator and store that names' string
                            //         "name" => implode("|", $imagesNameArray)
                            //     ],
                            // );

<!-- $floor->rooms()->updateOrCreate(
                            ['id' => $room['id'] ?? 0],
                            [
                                'room_type_id' => $room['room_type_id'],
                                'name' => $room['name'],
                                'capacity' => $room['capacity'],
                                'price' => $room['price'],
                                'cover_image' => $coverImageName,
                                'other_images' => $otherImagesName,
                                // 'cover_image' => null,
                                // 'cover_image' => update_cover_image($floor, $room['id'], $room['cover_image']),
                                // 'other_images' => update_other_images($floor, $room['id'], $room['other_images']),
                            ]
                            // $room
                        ); -->

<!-- old 'floor update' method using try catch for request validation -->
// public function update(Request $request, Property $property, Floor $floor)
    // {
    //     try{

    //         $this->validate($request , [
    //                 'name' => [
    //                     'required',
    //                     Rule::unique('floors', 'name')->where('property_id', $property->id)->ignore($request->route()->parameter('floor')),
    //                 ],
    //                 'rooms' => 'nullable|array',
    //                 'rooms.*.room_type_id' => 'required|exists:room_types,id',
    //                 'rooms.*.name' => [
    //                     'required',
    //                     new UniqueFloorChildsNames(),
    //                 ],
    //                 'tables' => 'nullable|array',
    //                 'tables.*.name' => [
    //                     'required',
    //                     new UniqueFloorChildsNames(),
    //                 ],
    //                 'tables.*.seating_capacity' => 'nullable|integer',
    //         ]);

    //         if ($floor->property_id != $property->id) {
    //             abort(401);
    //         }

    //         DB::transaction(function () use ($request, $floor) {
    //             $floor->update($request->validated());

    //             $roomIds = collect($request->rooms)->pluck('id')->filter();
    //             $floor->rooms()->whereNotIn('id', $roomIds)->delete();

    //             foreach ($request->rooms as $room) {
    //                 $floor->rooms()->updateOrCreate(
    //                     ['id' => $room['id'] ?? 0],
    //                     $room
    //                 );
    //             }

    //             $tableIds = collect($request->tables)->pluck('id')->filter();
    //             $floor->tables()->whereNotIn('id', $tableIds)->delete();
    //             foreach ($request->tables as $table) {
    //                 $floor->tables()->updateOrCreate(
    //                     ['id' => $table['id'] ?? 0],
    //                     $table
    //                 );
    //             }
    //         });
    //         return response()->json([
    //             'message' => 'Floor Updated Successfully!',
    //         ]);

    //     }catch(\Exception | ValidationException $e){
    //         if($e instanceof ValidationException){
    //             return ['code'=>'422','errors' => $e->errors()];
    //         }
    //         else{
    //             return ['code'=>'500','error_message'=>$e->getMessage()];
    //         }
    //     }
    // }


<!-- old 'floor store' method using try catch for request validation -->
  <!--    public function store(Request $request, Property $property)
 {
        try{

            $this->validate($request , [
                'floors' => 'required|array',
                'floors.*.name' => [
                    'required',
                    new UniqueFloorNames(),
                    Rule::unique('floors', 'name')->where('property_id', $property->id),
                ],
                'floors.*.rooms' => 'nullable|array',
                'floors.*.rooms.*.room_type_id' => 'required|exists:room_types,id',
                'floors.*.rooms.*.name' => [
                    'required',
                    new UniqueChildsNamesPerFloor(),
                ],
                'floors.*.tables' => 'nullable|array',
                'floors.*.tables.*.name' => [
                    'required',
                    new UniqueChildsNamesPerFloor(),
                ],
                'floors.*.tables.*.seating_capacity' => 'nullable|integer',
            ]);

            DB::transaction(function () use ($request, $property) {
                foreach ($request->floors as $floor) {
                    $floorObj = $property->floors()->create($floor);
                    foreach ($floor['rooms'] as $room) {
                        $floorObj->rooms()->create($room);
                    }
                    foreach ($floor['tables'] as $table) {
                        $floorObj->tables()->create($table);
                    }
                }
            });
            return response()->json([
                'message' => 'Floors Created Successfully!',
            ]);

        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    } -->
