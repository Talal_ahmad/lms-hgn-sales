/*
Give the service worker access to Firebase Messaging.
Note that you can only use Firebase Messaging here, other Firebase libraries are not available in the service worker.
*/
importScripts('https://www.gstatic.com/firebasejs/10.1.0/firebase-app-compat.js');
importScripts('https://www.gstatic.com/firebasejs/10.1.0/firebase-messaging-compat.js');
   
/*
Initialize the Firebase app in the service worker by passing in the messagingSenderId.
* New configuration for app@pulseservice.com
*/
const firebaseConfig = {
    apiKey: "AIzaSyBEoly6Evcli-wl1fgdUT9iTlyJhfzlQd8",
    authDomain: "corporate-connect-clinix.firebaseapp.com",
    databaseURL: "https://corporate-connect-clinix-default-rtdb.firebaseio.com",
    projectId: "corporate-connect-clinix",
    storageBucket: "corporate-connect-clinix.appspot.com",
    messagingSenderId: "937155965085",
    appId: "1:937155965085:web:61a549e935b33d772de474",
    measurementId: "G-SG0LMS41QY"
};
firebase.initializeApp(firebaseConfig);
/*
Retrieve an instance of Firebase Messaging so that it can handle background messages.
*/
const messaging = firebase.messaging();
messaging.onBackgroundMessage((payload) => {
    console.log(
      '[firebase-messaging-sw.js] Received background message ',
      payload
    );
    // Customize notification here
    const notificationTitle = 'Background Message Title';
    const notificationOptions = {
      body: 'Background Message body.',
      icon: '/firebase-logo.png'
    };
  
    self.registration.showNotification(notificationTitle, notificationOptions);
  });
// messaging.setBackgroundMessageHandler(function(payload) {
//     console.log(
//         "[firebase-messaging-sw.js] Received background message ",
//         payload,
//     );
//     /* Customize notification here */
//     const notificationTitle = "Background Message Title";
//     const notificationOptions = {
//         body: "Background Message body.",
//         icon: "/itwonders-web-logo.png",
//     };
  
//     return self.registration.showNotification(
//         notificationTitle,
//         notificationOptions,
//     );
// });