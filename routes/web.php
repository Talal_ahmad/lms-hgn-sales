<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FloorController;
use App\Http\Controllers\HouseKeeperController;
use App\Http\Controllers\MenuCategoryController;
use App\Http\Controllers\MenuItemController;
use App\Http\Controllers\MenuServingController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PropertyController;
use App\Http\Controllers\PropertyDashboardController;
use App\Http\Controllers\PropertyTypeController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\ReservationController;
use App\Http\Controllers\CompanyReferenceController;
use App\Http\Controllers\ReferenceController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\CustomerTypeController;
use App\Http\Controllers\BookingRoomController;
use App\Http\Controllers\RoomConditionController;
use App\Http\Controllers\RoomTypeController;
use App\Http\Controllers\RoomServiceController;
use App\Http\Controllers\RoomStatusController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VehicleTypeController;
use App\Http\Controllers\ThirdPartyVehicleController;
use App\Http\Controllers\VehicleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
 */

Route::middleware('auth')->group(function () {
    Broadcast::routes();
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::get('/', [DashboardController::class, 'index'])->name('dashboard')->can('view-main-dashboard');
    Route::resource('/users', UserController::class);
    Route::post('UserToken/{id}', [UserController::class, 'UserToken']);
    Route::post('users_status/{id}', [UserController::class, 'users_status']);
    // Role
    Route::resource('roles', RoleController::class);
    Route::post('store-role-permissions/{role}', [RoleController::class, 'storeRolePermissions'])->name('store-role-permissions');
    // Permission
    Route::resource('permissions', PermissionController::class);

    // PropertyTypes (we don't want the admin to add 'Property Types' of his own, as these are Static and just Seeded using 'PropertyTypeSeeder')
    // Route::resource('property_types', PropertyTypeController::class);

    // Property
    Route::resource('properties', PropertyController::class);
    // Floors

    Route::prefix('properties/{property}')->group(function () {
        Route::middleware(['role.based.redirect'])->group(function () {
            Route::resource('room-condition', RoomConditionController::class);
        });
        Route::get('/', [PropertyDashboardController::class, 'index'])->name('property.dashboard')->can('view-dashboard');
        Route::get('getFloorRooms',[FloorController::class,'getFloorRooms']);
        Route::Resource('floors', FloorController::class);
        Route::Resource('menu-categories', MenuCategoryController::class);
        Route::Resource('menu-servings', MenuServingController::class);
        Route::Resource('menu-items', MenuItemController::class);
        Route::Resource('sales', OrderController::class);
        Route::get('kitchen-orders', [OrderController::class, 'getKitchenListing']);
        Route::get('mark-order-ready/{order}', [OrderController::class, 'markReady']);
        Route::get('tables-view', [OrderController::class, 'tablesForMakingOrder']);
        Route::get('sale-report', [ReportController::class, 'saleReport'])->name('property.sale-report');
        Route::get('police-report', [ReportController::class, 'policeReport'])->name('property.police-report');
        Route::Resource('reservations', ReservationController::class);

        // Hotel
        Route::Resource('customer-type', CustomerTypeController::class);
        Route::Resource('room-type', RoomTypeController::class);
        Route::Resource('company-reference', CompanyReferenceController::class);
        Route::Resource('reference', ReferenceController::class);
        Route::Resource('booking', BookingRoomController::class);
        Route::get('Createbooking/{id}', [BookingRoomController::class, 'Createbooking']);
        Route::post('arrivalbooking/{id}', [BookingRoomController::class, 'arrivalbooking']);
        Route::post('departurebooking/{id}', [BookingRoomController::class, 'departurebooking']);
        // Route::get('booking/{id}', [PropertyDashboardController::class, 'booking']);
        Route::get('Editbooking/{id}', [BookingRoomController::class, 'Editbooking']);
        Route::get('bookingsummary', [BookingRoomController::class, 'Summary'])->name('bookingsummary');
        Route::get('customerlist', [BookingRoomController::class, 'customerlist'])->name('customerlist');
        Route::post('cancelbooking/{id}', [BookingRoomController::class, 'cancelbooking'])->name('cancelbooking');

        Route::Resource('housekeeper', HouseKeeperController::class);
        Route::get('housekeeper/getrooms', [HouseKeeperController::class, 'getRooms'])->name('housekeeper.getRooms');
        Route::Resource('room-services', RoomServiceController::class);
        Route::Resource('room-status', RoomStatusController::class);
        Route::get('get-customer/{id}', [BookingRoomController::class, 'getCustomer'])->name('get-customer');
        Route::get('customer-history/{id}', [BookingRoomController::class, 'customer_history'])
            ->name('customer.history');
            Route::post('rentbooking/{id}', [BookingRoomController::class, 'rentbooking']);
        // vehicle
        Route::Resource('vehicle-type', VehicleTypeController::class);
        Route::Resource('vehicle', VehicleController::class);
        Route::Resource('third_party_vehicle', ThirdPartyVehicleController::class);
        // calender
        Route::get('calender', [BookingRoomController::class, 'calender'])->name('calender');
    });
});

require __DIR__ . '/auth.php';
