<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pick_and_drops', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('booking_id');
            $table->string('pick', 255);
            $table->string('drop', 255);
            $table->dateTime('pickDateTime');
            $table->decimal('fare', 10, 2);
            $table->timestamps();


            // Setting up the foreign key relationship
            //   $table->foreign('booking_id')->references('id')->on('booking_rooms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pick_and_drops');
    }
};
