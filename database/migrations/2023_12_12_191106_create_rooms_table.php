<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id();
            $table->foreignId('floor_id')->constrained();
            $table->foreignId('room_type_id')->constrained();
            $table->string('name');
            $table->tinyInteger('capacity');
            $table->integer('price');
            $table->string('cover_image', 100)->nullable();
            $table->string('other_images', 100)->nullable();
            $table->enum('status', ['available', 'occupied', 'Booked'])->default('available');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rooms');
    }
};
