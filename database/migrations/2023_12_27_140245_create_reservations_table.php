<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['pre-booking', 'runtime-booking'])->default('runtime-booking');
            $table->foreignId('customer_id')->constrained();
            $table->foreignId('table_id')->nullable()->constrained();
            $table->foreignId('room_id')->nullable()->constrained();
            $table->integer('number_of_persons')->default(1);
            $table->dateTime('start_date_time');
            $table->dateTime('end_date_time');
            $table->tinyText('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('reservations');
    }
};
