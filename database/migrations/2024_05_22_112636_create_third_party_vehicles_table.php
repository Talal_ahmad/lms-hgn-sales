<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('third_party_vehicles', function (Blueprint $table) {
            $table->id();
            $table->string('company_name');
            $table->string('email_address')->unique();
            $table->string('phone_number');
            $table->string('address');
            $table->string('company_owner_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('third_party_vehicles');
    }
};
