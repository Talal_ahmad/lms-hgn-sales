<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            // $table->foreignId('property_id')->constrained();
            $table->string('property_id', 255)->index();
            $table->enum('type', ['foc', 'walk-in', 'staff'])->default('walk-in');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('gender');
            $table->string('guest_category')->nullable();
            $table->dateTime('cnic_expired')->nullable();
            $table->string('nationality')->nullable();
            $table->string('phone_number');
            $table->string('country_code');
            $table->date('dob')->nullable();
            $table->string('cnic')->nullable();
            $table->string('address')->nullable();
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('customers');
    }
};
