<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AssignPermissionSeeder extends Seeder
{
    /**
     *   If You added Any New Permission
     *   Run Command : php artisan db:seed --class=AssignPermissionSeeder
     */

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Giving Super Admin Permissions
        $admin_permissions = Permission::get();

        $admin_role = Role::where('name', 'Super Admin')->first();

        $admin_role->permissions()->sync($admin_permissions);

        // Giving House Keeper Permissions
        $house_keeper_permissions = Permission::where('parent_name', 'Room Condition Management')->get();

        $house_keeper_role = Role::where('name', 'House Keeper')->first();

        $house_keeper_role->permissions()->sync($house_keeper_permissions);
    }
}