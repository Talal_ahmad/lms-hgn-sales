<?php

namespace Database\Seeders;

use App\Models\Room;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Room::Create([
            'floor_id' => 1,
            'room_type_id' => 3,
            'name' => 'demo floor room 1',
        ]);

        // $rooms = range(1, 10);

        // foreach ($rooms as $key => $value) {
        //     Room::Create([
        //         'floor_id' => 1,
        //         'room_type_id' => 3,
        //         'name' => 'demo floor room ' . $value,
        //     ]);
        // }    
    }
}
