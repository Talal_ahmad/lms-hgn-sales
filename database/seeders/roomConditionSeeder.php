<?php

namespace Database\Seeders;
use App\Models\RoomCondition;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class roomConditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        RoomCondition::Create([
            'room_id' => 1,
            'room_service_id' => 7,
            'status' => 'satisfied',
        ]);
    }
}
