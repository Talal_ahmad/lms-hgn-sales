<?php

namespace Database\Seeders;

use App\Models\HouseKeeperRoom;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class HouseKeeperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $houseKeepers = [
            // ['name' => 'Demo Super Admin', 'user_id' => 1, 'floor_id' => 1],
            ['name' => 'Demo House Keeper', 'user_id' => 2, 'floor_id' => 1, 'room_id' => 1],
        ];

        foreach ($houseKeepers as $houseKeeper) {
            HouseKeeperRoom::create([
                'user_id' => $houseKeeper['user_id'],
                'floor_id' => $houseKeeper['floor_id'],
                'room_id' => $houseKeeper['room_id'],
            ]);
        }

    }
}
