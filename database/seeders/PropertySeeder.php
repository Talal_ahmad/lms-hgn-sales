<?php

namespace Database\Seeders;

use App\Models\Property;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $propertyTypes = [
            'Mall',
            'Hotel',
            'Restaurant',
        ];

        foreach ($propertyTypes as $key => $type) {
            Property::Create([
                'property_type_id' => $key+1,
                'name' => 'demo ' . $type,
            ]);
        }    
    }
}
