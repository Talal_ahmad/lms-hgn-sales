<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     *   If You added Any New Permission
     * 
     *   then Truncate tables: first 'role_has_permissions', then secondly 'permissions'
     *   then Run Seeders: 'PermissionSeeder', 'AssignPermissionSeeder'
     * 
     *   PermissionSeeder:
     *   Run Command : php artisan db:seed --class=PermissionSeeder
     * 
     *   AssignPermissionSeeder:
     *   Run Command : php artisan db:seed --class=AssignPermissionSeeder
     */

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // @can('floors-list')
        // @endcan

        $permissions = [
            'Dashboard Management' => ['view-main-dashboard', 'view-dashboard'],
            'User Management' => ['users-list', 'add-user', 'edit-user', 'view-user', 'delete-user'],
            'Role Management' => ['roles-list', 'add-role', 'edit-role', 'view-role', 'delete-role'],
            'Permission Management' => ['permissions-list'],
            'Property Type Management' => ['property-type-list', 'add-property-type', 'edit-property-type', 'view-property-type', 'delete-property-type'],
            'Property Management' => ['properties-list', 'add-property', 'edit-property', 'view-property', 'delete-property', 'properties-for-dashboard'],
            'Floor Management' => ['floors-list', 'add-floor', 'edit-floor', 'view-floor', 'delete-floor'],
            'Reservation Management' => ['reservations-list', 'add-reservation', 'edit-reservation', 'view-reservation', 'delete-reservation'],
            'Menu Category Management' => ['menu-categories-list', 'add-menu-category', 'edit-menu-category', 'view-menu-category', 'delete-menu-category'],
            'Menu Serving Management' => ['menu-servings-list', 'add-menu-serving', 'edit-menu-serving', 'view-menu-serving', 'delete-menu-serving'],
            'Menu Item Management' => ['menu-items-list', 'add-menu-item', 'edit-menu-item', 'view-menu-item', 'delete-menu-item'],
            'Sale Management' => ['sales-list', 'add-sale', 'edit-sale', 'view-sale', 'delete-sale'],
            'Kitchen Management' => ['kitchens-list', 'add-kitchen', 'edit-kitchen', 'view-kitchen', 'delete-kitchen'],
            'Order Management' => ['order-list', 'add-order', 'edit-order', 'view-order', 'delete-order'],
            'Report' => ['sale-report'],
            'Pos Management' => ['pos-list', 'add-pos', 'edit-pos', 'view-pos', 'delete-pos'],
            'Room Booking Management' => ['room-booking-list', 'add-room-booking', 'edit-room-booking', 'view-room-booking', 'delete-room-booking'],
            'Room Service Management' => ['room-service-list', 'add-room-service', 'edit-room-service', 'view-room-service', 'delete-room-service'],
            'Room Condition Management' => ['room-condition-list', 'add-room-condition', 'edit-room-condition', 'view-room-condition'],
            'House Keeper Management' => ['house-keeper-list', 'add-house-keeper', 'edit-house-keeper', 'view-house-keeper', 'delete-house-keeper'],
            'Room Status Management' => ['room-status-list', 'add-room-status', 'edit-room-status', 'view-room-status', 'delete-room-status'],
            'Room Type Management' => ['room-type-list', 'add-room-type', 'edit-room-type', 'view-room-type', 'delete-room-type'],
            'Customer Type Management' => ['customer-type-list', 'add-customer-type', 'edit-customer-type', 'view-customer-type', 'delete-customer-type'],
            'Company Reference Management' => ['company-reference-list', 'add-company-reference', 'edit-company-reference', 'view-company-reference', 'delete-company-reference'],
            'Reference Management' => ['reference-list', 'add-reference', 'edit-reference', 'view-reference', 'delete-reference'],
        ];
        $data = [];
        foreach ($permissions as $parent => $permission) {
            foreach ($permission as $value) {
                $display_name = ucwords(str_replace('-', ' ', $value));
                if (!Permission::where('parent_name', $parent)->where('display_name', $display_name)->where('name', $value)->where('guard_name', 'web')->exists()) {
                    $data[] = [
                        'parent_name' => $parent,
                        'display_name' => $display_name,
                        'name' => $value,
                        'guard_name' => 'web',
                        'created_at' => now(),
                    ];
                }
            }
        }
        Permission::insert($data);
    }
}
