<?php

namespace Database\Seeders;

use App\Models\PropertyType;
use Illuminate\Database\Seeder;

class PropertyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $propertyTypes = [
            'Mall',
            'Hotel',
            'Restaurant',
        ];

        foreach ($propertyTypes as $type) {
            PropertyType::updateOrCreate([
                'name' => $type,
            ]);
        }
    }
}
