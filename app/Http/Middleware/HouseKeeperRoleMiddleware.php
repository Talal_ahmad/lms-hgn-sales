<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\RedirectResponse;

class HouseKeeperRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        // if (!$request->user() || !$request->user()->hasRole('House Keeper')) {
        //     // If the user is not a housekeeper, abort with a 403 error
        //     abort(403, 'Unauthorized action.');
        // }
        
        // // If the user is a housekeeper, but the requested route is not allowed,
        // // redirect them to the room-condition route
        // if (!$this->isAllowedRoute($request)) {
        //     // dd('not allowed');
        //     return redirect()->route('room-condition.index');
        // }

        // return $next($request);
        
        if (!$request->user() || !$request->user()->hasRole('House Keeper')) {
            abort(403, 'Unauthorized action.');
        }
        
        return $next($request);

    }
    
    /**
     * Check if the requested route is allowed for the housekeeper role.
     *
     * @param \Illuminate\Http\Request $request
     * @return bool
     */
    // protected function isAllowedRoute(Request $request): bool
    // {
    //     // Define the allowed routes for the housekeeper role
    //     $allowedRoutes = [
    //         'room-condition.index',
    //         'room-condition.create',
    //         'room-condition.store',
    //         'room-condition.show',
    //         'room-condition.edit',
    //         'room-condition.update',
    //         'room-condition.destroy',
    //         // Add other resource routes here
    //     ];
    //     // dd($request->route()->getName());
    //     // Check if the requested route is allowed
    //     return in_array($request->route()->getName(), $allowedRoutes);
    //     // dd(in_array($request->route()->getName(), $allowedRoutes));
    // }
    
}
