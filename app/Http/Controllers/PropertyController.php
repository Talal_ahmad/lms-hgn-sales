<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PropertyRequest;
use App\Models\Property;
use App\Models\PropertyType;
use DataTables;
use DB;


class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $propertyTypes = PropertyType::all();
        $properties = Property::join('property_types', 'property_types.id', '=', 'properties.property_type_id')->select('properties.*','property_types.name as type_name');
        if($request->ajax()){
            return DataTables::of($properties)->addIndexColumn()->make(true);
        }
        return view('admin.property.index', compact('propertyTypes'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PropertyRequest $request)
    {
        try{
            DB::transaction(function () use ($request) {
                Property::create($request->validated());
            });
            return ['code'=>'200', 'status'=>'success'];

        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Property $property)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Property $property)
    {
        return response()->json($property);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PropertyRequest $request, Property $property)
    {
        try{
            DB::transaction(function () use ($request, $property) {
                $property->update($request->validated());
            });
            return ['code'=>'200', 'status'=>'success'];

        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Property $property)
    {
        try{
            $property->delete();
            return ['code'=>'200', 'status'=>'success'];

        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message' => $e->getMessage()];
            }
        }
    }
}
