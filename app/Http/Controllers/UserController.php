<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Property;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use DataTables;
use DB;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $users = User::with('roles')->get();
        $properties = Property::all();
        $roles = Role::all();

        if ($request->ajax()) {
            return DataTables::of($users)->addIndexColumn()->make(true);
        }

        return view('admin.user.index', compact('users', 'roles', 'properties'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UserRequest $request)
    {

        try {
            DB::transaction(function () use ($request) {
                $validatedData = $request->validated();

                // $user = User::create($request->validated());
                $user = new User();
                $user->name = $validatedData['name'];
                $user->email = $validatedData['email'];
                $user->password = Hash::make($validatedData['password']);
                $user->property_id = $validatedData['property_id'];

                // Save the user
                $user->save();

                if ($request->has('role')) {
                    $role = Role::find($request->role);
                    $user->assignRole($role);
                }
            });
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    public function edit(User $user)
    {
        $role = DB::table('model_has_roles')->where('model_id', $user->id)->pluck('role_id');
        return response()->json([
            'user' => $user,
            'role' => $role,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UserRequest $request, User $user)
    {
        // try {
        //     // $user->update($request->validated());
        //     // if ($request->has('role')) {
        //     //     $role = Role::find($request->role);
        //     //     $user->assignRole($role);
        //     // }
        //     // else{
        //     //     $user->syncRoles([]);
        //     // }
        //     // $user->properties()->sync($request->properties);
        //     // return ['code'=>'200', 'status'=>'success'];

        // }
        try {
            DB::transaction(function () use ($request, $user) {
                // Validate the request data
                $validatedData = $request->validated();

                // Update the user with validated data
                $user->update([
                    'name' => $validatedData['name'],
                    'email' => $validatedData['email'],
                    'password' => Hash::make($validatedData['password']),
                    'property_id' => $validatedData['property_id'],
                ]);

                // If the request has 'role', assign role to the user
                if ($request->has('role')) {
                    $role = Role::find($request->role);
                    $user->syncRoles($role ? [$role] : []); // Sync roles, including an empty array if no role is provided
                } else {
                    $user->syncRoles([]); // Sync empty array to remove all roles
                }

                // Sync properties
                $properties = $request->input('properties', []); // Get properties from the request, default to empty array
                $user->properties()->sync($properties); // Sync properties
            });

            // Return success response
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        try {
            $user->delete();
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    public function users_status(Request $request)
    {
        // Validate the incoming request
        $validatedData = $request->validate([
            'id' => 'required|integer|exists:users,id',
            'status' => 'required|integer|min:0|max:1',
        ]);

        // Find the user by ID
        $user = User::find($request->id);

        // If user is found, update the status
        if ($user) {
            $user->status = $validatedData['status'];
            $user->save(); // Save the updated status to the database

            // Return a success response
            return response()->json([
                'status_update' => true,
            ], 200);
        } else {
            // If the user is not found, return an error response
            return response()->json([
                'status_update' => false,
                'message' => 'User not found.',
            ], 404);
        }
    }
    public function UserToken(request $request, $id)
    {
        // dd($id);
        $user = User::find($id);
        $user->UserToken = $request->currentToken;
        $user->update();
        return response()->json(['success' => true]);
    }
}
