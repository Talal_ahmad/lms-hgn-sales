<?php

namespace App\Http\Controllers;

use App\Models\Reference;
use App\Models\Property;
use App\Http\Requests\ReferenceRequest;
use DataTables;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ReferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Property $property)
    {
        $Reference = Reference::all();
        // dd($Reference);
        if ($request->ajax()) {
            return DataTables::of($Reference)->addIndexColumn()->make(true);
        }
        return view('admin.Reference.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ReferenceRequest $request)
    {
        try {
            DB::transaction(function () use ($request) {
                Reference::create($request->validated());
            });
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Property $property, Reference $reference)
    {
        return response()->json($reference);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ReferenceRequest $request, Property $property, Reference $reference)
    {
        try {

            DB::transaction(function () use ($request, $reference) {
                $reference->update($request->validated());
            });

            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Property $property, Reference $reference)
    {
        try {
            $reference->delete();

            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
