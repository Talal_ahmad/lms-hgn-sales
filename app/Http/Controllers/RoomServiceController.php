<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoomServiceRequest;
use App\Models\Property;
use App\Models\RoomService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use DB;

class RoomServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Property $property)
    {
        $room_services = RoomService::query();
        
        // dd($room_services);
        if ($request->ajax()) {
            return DataTables::of($room_services)->addIndexColumn()->make(true);
        }

        return view('admin.roomServices.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request, Property $property)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RoomServiceRequest $request, Property $property)
    {
        // dd($request->validated());
 
        try {
            DB::transaction(function () use ($request) {
                RoomService::create($request->validated());
            });

            return ['code' => '200', 'status' => 'success'];

        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, Property $property, RoomService $roomService)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, Property $property, RoomService $roomService)
    {
    //    dd($roomService);

       return response()->json($roomService);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(RoomServiceRequest $request, Property $property, RoomService $roomService)
    {
        try {
            // dd($roomService);

            DB::transaction(function () use ($request, $roomService) {
                $roomService->update($request->validated());
            });

            return ['code' => '200', 'status' => 'success'];

        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Property $property, RoomService $roomService)
    {
        try {
            
            $roomService->delete();

            return ['code' => '200', 'status' => 'success'];

        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
