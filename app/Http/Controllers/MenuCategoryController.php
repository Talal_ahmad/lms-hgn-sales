<?php

namespace App\Http\Controllers;

use App\Http\Requests\MenuCategoryRequest;
use App\Models\Property;
use DataTables;
use DB;
use App\Models\MenuCategory;
use Illuminate\Http\Request;

class MenuCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Property $property)
    {
        $menu_Category = $property->menuCategories;

        if ($request->ajax()) {
            return DataTables::of($menu_Category)->addIndexColumn()->make(true);
        }
        return view('admin.menuCategory.index', compact('property'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MenuCategoryRequest $request)
    {
        try {
            $propertyId = $request->input('property_id');

            $property = Property::findOrFail($propertyId);
            // Merge the property_id into the request data
            $request->merge(['property_id' => $property->id]);

            DB::transaction(function () use ($request) {
                MenuCategory::create($request->validated());
            });
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(MenuCategory $menuCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Property $property, MenuCategory $menuCategory)
    {
        return response()->json($menuCategory);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MenuCategoryRequest $request, Property $property, MenuCategory $menuCategory)
    {
        try {

            $requestData = $request->merge(['property_id' => $property->id]);

            DB::transaction(function () use ($requestData, $menuCategory) {
                $menuCategory->update($requestData->all());
            });
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Property $property, MenuCategory $menuCategory)
    {
        try {
            $menuCategory->delete();
    
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
