<?php

namespace App\Http\Controllers;

// use DataTables;
use Yajra\DataTables\Facades\DataTables;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Validation\ValidationException;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth'); 
    }
    
    public function index(Request $request)
    {
        if($request->ajax()){
            return DataTables::eloquent(Role::query())->make(true);
        }

        return view('admin.roles&permission.role');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'name' => 'required',
            ]);

            $role = new Role();
            $role->name = $request->input('name');
            $role->save();
            return ['code'=>'200','message'=>'success'];
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Role $role)
    {
        $permissions_parent_name = Permission::groupBy('parent_name')->get();
        $role_permissions = $role->permissions->pluck('id')->toArray();
        return view('admin.roles&permission.rolehaspermissions', compact('role', 'permissions_parent_name', 'role_permissions'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $role = Role::find($id);
        return response([
            'role' => $role,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try{
            $this->validate($request , [
                'name' => 'required',           
            ]); 
            $role = Role::find($id);
            $role->name = $request->name;
            $role->update();
            return ['code'=>'200','message'=>'success'];
        }catch(\Exception | validationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=> $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try{
            $role = Role::find($id);
            $user_id = DB::table('model_has_roles')->where('role_id' , $id)->get();
            if(count($user_id) > 0){
                return ['code'=>'300', 'message'=>'This Role is Assigned to Some Users Please change it first!'];
            }
            $role->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }

    public function storeRolePermissions(Request $request, Role $role)
    {
        $permissions = array_map('intval', $request->permissions);
        $role->syncPermissions($permissions);

        return redirect('roles')->with(['message' => 'Permissions Assigned Successfully!']);
    }
}
