<?php

namespace App\Http\Controllers;

use App\Models\Property;
use App\Models\Customer;
use App\Models\CustomerType;
use App\Models\BookingRoom;
use App\Models\Room;
use App\Models\Floor;
use App\Models\RoomType;
use App\Models\CompanyReference;
use App\Models\PickAndDrop;
use App\Models\Vehicle;
use App\Models\VehicleBooking;
use App\Models\Reference;
use App\Rules\CheckRoomBookingTimeOverlap;
use App\Models\User;
use DataTables;
use Spatie\Permission\Models\Role;
use DB;
use App\Http\Requests\BookingRoomRequest;
use App\Models\RoomCondition;
use Illuminate\Http\Request;
use Carbon\Carbon;


class BookingRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Property $property)
    {
        // $rooms = $property->floors()->with('rooms')->get()->pluck('rooms')->flatten();
        // $floors = $property->floors()->with(['rooms', 'rooms.bookings.customer'])->get();
        $floors = Floor::where('property_id', $property->id)
            ->with(['rooms' => function ($query) {
                $query->with(['bookings' => function ($query) {
                    $query->where('cancel', 0)
                        ->where(function ($query) {
                            $query->WhereNull('departureDateTime');
                        });
                }, 'bookings.customer']);
            }])
            ->get();
        $vehicles = Vehicle::get();
        // dd($vehicle);

        return view('admin.booking.index', compact('floors', 'vehicles'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request, Property $property, $id)
    {
        // return view('admin.booking.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(BookingRoomRequest $request)
    {
        try {
            // dd($request->all());
            $this->validate($request, [
                'fromDateTime' => ['required', new CheckRoomBookingTimeOverlap($request->input('room_id'), $request->input('fromDateTime'), $request->input('toDateTime'))],
            ]);

            DB::transaction(function () use ($request) {
                // Extract property ID
                $property = explode(" ", $request->property_id);

                // Create a new customer using the customer data from the request
                $customerData = $request->only(['name', 'gender', 'guest_category', 'cnic_expired', 'phone_number', 'country_code', 'dob', 'cnic', 'nationality', 'address', 'property_id']);
                // Check if email exists in the request and it's not null, then add email field
                if ($request->filled('email')) {
                    $customerData['email'] = $request->input('email');
                }
                $customer = Customer::create($customerData);
                // dd($customer);
                // Check if email exists in the request and it's not null, then create a user
                if ($request->filled('email')) {
                    $userData = [
                        'name' => $request->input('name'),
                        'email' => $request->input('email'),
                        'phone_number' => $request->input('phone_number'),
                        'cnic' => $request->input('cnic'),
                        'address' => $request->input('address'),
                        'customer_id' => $customer->id,
                        'property_id' => $property,
                    ];
                    // Create a new user with the customer_id
                    $user = User::create($userData);
                    // Assign the default role (customer) to the user
                    $customerRole = Role::where('name', 'Customer')->first();
                    $user->assignRole($customerRole);
                }
                // Store complementary food data in JSON format
                $complementaryFood = [
                    'breakfast' => $request->has('complementary_food.breakfast') ? 'yes' : 'no',
                    'lunch' => $request->has('complementary_food.lunch') ? 'yes' : 'no',
                    'dinner' => $request->has('complementary_food.dinner') ? 'yes' : 'no',
                ];
                // Encode the complementary food array as JSON string
                $complementaryFoodJson = json_encode($complementaryFood);

                // Create the booking using the validated data and the customer_id
                $bookingData = $request->all();
                $bookingData['customer_id'] = $customer->id;
                $bookingData['complementary_food'] = $complementaryFoodJson;
                if (isset($user)) {
                    $bookingData['user_id'] = $user->id;
                }
                $booking = BookingRoom::create($bookingData);

                if ($request->DropCheckbox) {
                    $pickAndDropData = $request->only(['pick', 'drop', 'PickDateTime', 'fare']);
                    $pickAndDropData['booking_id'] = $booking->id;
                    PickAndDrop::create($pickAndDropData);
                }

                // Additional logic if needed within the transaction
                $room = Room::find($request->input('room_id'));
                if ($room) {
                    $room->update(['status' => 'Booked']);
                }
            });

            // Redirect to a page or return a response indicating success
            return redirect('/properties/' . $request->property_id . '/booking')->with('success', 'Booking created successfully');
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Property $property, $id)
    {
        try {
            // dd($request->all());
            // $this->validate($request, [
            //     'fromDateTime' => ['required', new CheckRoomBookingTimeOverlap($request->input('room_id'), $request->input('fromDateTime'), $request->input('toDateTime'))],
            // ]);
            DB::transaction(function () use ($request, $id) {
                // dd($request->shift_toDateTime);
                $property = explode(" ", $request->property_id);

                if ($request->has('shiftCheckbox')) {
                    $booking = BookingRoom::findOrFail($id);
                    $booking->update([
                        'room_id' => $request->input('room_id'),
                        'shift_room_id' => $request->input('shift_room_id'),
                        'toDateTime' => Carbon::now(), // Update toDateTime to current time and date using Carbon
                        'departureDateTime' => Carbon::now(), // Update departureDateTime to current time and date using Carbon
                    ]);
                    $room_condition = RoomCondition::where('room_id', $request->room_id)
                        ->where('room_service_id', 7)
                        ->first();

                    $room_condition->update([
                        'status' => 'dirty',
                        'description' => 'dirty',
                    ]);
                    $room = Room::find($request->input('room_id'));
                    if ($room) {
                        $room->update(['status' => 'occupied']);
                    }



                    // Add a new booking record for the shifted room
                    $newBooking = BookingRoom::create([
                        'user_id' => $booking->user_id,
                        'room_id' => $request->input('shift_room_id'),
                        'customer_id' => $booking->customer_id,
                        'customer_type_id' => $booking->customer_type_id,
                        'reference_id' => $booking->reference_id,
                        'company_reference_id' => $booking->company_reference_id,
                        'male_count' => $booking->male_count,
                        'female_count' => $booking->female_count,
                        'child_count' => $booking->child_count,
                        'infant_count' => $booking->infant_count,
                        'stay_length' => $booking->stay_length,
                        'fromDateTime' => Carbon::now(), // Set fromDateTime to current time and date using Carbon
                        'toDateTime' => $request->shift_toDateTime, // Set shift_toDateTime same as the original booking
                        'arrivalDateTime' => Carbon::now(), // Set arrivalDateTime to current time and date using Carbon
                        'complementary_food' => $booking->complementary_food, // Set arrivalDateTime to current time and date using Carbon
                        'description' => $booking->description, // Set description to the same as the original booking
                    ]);
                    $room = Room::find($request->input('shift_room_id'));
                    if ($room) {
                        $room->update(['status' => 'Booked']);
                    }

                    // Update or create pick and drop record
                    if ($request->filled('DropCheckbox')) {
                        PickAndDrop::updateOrCreate(
                            ['booking_id' => $newBooking->id],
                            [
                                'pick' => $request->input('pick'),
                                'drop' => $request->input('drop'),
                                'pickDateTime' => $request->input('PickDateTime'),
                                'fare' => $request->input('fare')
                            ]
                        );
                    } else {
                        // Delete pick and drop record if exists
                        PickAndDrop::where('booking_id', $newBooking->id)->delete();
                    }
                } else {
                    // Update customer information
                    $booking = BookingRoom::findOrFail($id);
                    $booking->update($request->all());

                    // Update complementary food options
                    $complementaryFood = [
                        'breakfast' => $request->has('complementary_food.breakfast') ? 'yes' : 'no',
                        'lunch' => $request->has('complementary_food.lunch') ? 'yes' : 'no',
                        'dinner' => $request->has('complementary_food.dinner') ? 'yes' : 'no',
                    ];
                    $complementaryFoodJson = json_encode($complementaryFood);
                    $booking->update(['complementary_food' => $complementaryFoodJson]);

                    $customer = Customer::findOrFail($booking->customer_id);
                    $customer->update($request->only(['name', 'email', 'gender', 'guest_category', 'cnic_expired', 'phone_number', 'dob', 'cnic', 'nationality', 'address', 'property_id']));


                    // Update or delete pick and drop information based on DropCheckbox
                    if ($request->filled('DropCheckbox')) {
                        // Update or create pick and drop record
                        $pickAndDrop = PickAndDrop::updateOrCreate(
                            ['booking_id' => $booking->id],
                            [
                                'pick' => $request->input('pick'),
                                'drop' => $request->input('drop'),
                                'pickDateTime' => $request->input('PickDateTime'),
                                'fare' => $request->input('fare')
                            ]
                        );
                    } else {
                        // Delete pick and drop record if exists
                        PickAndDrop::where('booking_id', $booking->id)->delete();
                    }

                    if ($request->filled('email')) {
                        $user = User::where('id', $booking->user_id)->first();
                        // dd($user);
                        if ($user) {
                            $user->update([
                                'name' => $request->input('name'),
                                'email' => $request->input('email'),
                                'phone_number' => $request->input('phone_number'),
                                'cnic' => $request->input('cnic'),
                                'address' => $request->input('address'),
                                'property_id' => $property,
                            ]);
                        } else {
                            $user = User::create([
                                'name' => $request->input('name'),
                                'email' => $request->input('email'),
                                'phone_number' => $request->input('phone_number'),
                                'cnic' => $request->input('cnic'),
                                'address' => $request->input('address'),
                                'property_id' => $property,
                            ]);
                        }
                        // Update booking with user_id
                        $booking->update(['user_id' => $user->id]);
                    } else {
                        // Update customer with null email
                        $customer->update(['email' => null]);
                        // Update booking with null user_id
                        $booking->update(['user_id' => null]);
                    }
                    // $customer = Customer::create($customerData);
                    // Update user information
                    // $user = User::findOrFail($booking->user_id);
                    // $user->update([
                    //     'name' => $request->input('name'),
                    //     'email' => $request->input('email'),
                    //     'phone_number' => $request->input('phone_number'),
                    //     'cnic' => $request->input('cnic'),
                    //     'address' => $request->input('address'),
                    //     'property_id' => $property,
                    // ]);


                    // // Additional logic if needed within the transaction
                    // $room = Room::find($request->input('room_id'));
                    // if ($room) {
                    //     $room->update(['status' => 'Booked']);
                    // }
                }
            });

            // Redirect to a page or return a response indicating success
            return redirect('/properties/' . $property->id . '/booking')->with('success', 'Booking updated successfully');
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Property $property, $id)
    {
        try {
            // Find the booking
            $booking = BookingRoom::findOrFail($id);

            // Find the associated user and customer
            $user = User::findOrFail($booking->user_id);
            $customer = Customer::findOrFail($booking->customer_id);

            // Delete the booking
            $booking->delete();

            // Delete the associated user and customer
            $user->delete();
            $customer->delete();

            // Update the status of the room
            // Assuming you have a Room model with a status field
            $room = Room::findOrFail($booking->room_id);
            $room->status = 'available';
            $room->save();

            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Failed to delete booking');
        }
    }
    public function Createbooking(Request $request, Property $property, $id)
    {
        $CustomerTypes = CustomerType::all();
        $customers = Customer::all();
        $References = Reference::all();
        $CompanyReferences = CompanyReference::all();
        $property_id = $property->id;
        // dd($customers);
        return view('admin.booking.create', compact('CustomerTypes', 'References', 'id', 'CompanyReferences', 'property_id', 'customers'));
    }
    public function Editbooking(Request $request, Property $property, $id)
    {
        $booking = BookingRoom::join('customers', 'booking_rooms.customer_id', 'customers.id')
            ->join('customer_types', 'booking_rooms.customer_type_id', 'customer_types.id')
            ->leftJoin('references', 'booking_rooms.reference_id', 'references.id')
            ->leftJoin('company_references', 'booking_rooms.company_reference_id', 'company_references.id')
            ->where('booking_rooms.id', $id)
            ->select(
                'customers.*',
                'booking_rooms.customer_type_id',
                'booking_rooms.complementary_food',
                'booking_rooms.female_count',
                'booking_rooms.male_count',
                'booking_rooms.child_count',
                'booking_rooms.infant_count',
                'booking_rooms.stay_length',
                'booking_rooms.description',
                'booking_rooms.fromDateTime',
                'booking_rooms.toDateTime',
                'booking_rooms.arrivalDateTime',
                'booking_rooms.departureDateTime',
                'booking_rooms.company_reference_id',
                'booking_rooms.reference_id',
                'booking_rooms.room_id',
                'booking_rooms.id as booking_id',
            )
            ->first();
        // Decode the complementary food options from JSON
        $complementaryFood = json_decode($booking->complementary_food, true);
        // dd($booking);
        // Fetch the related PickAndDrop data
        $pickAndDrop = PickAndDrop::where('booking_id', $booking->booking_id)->first();

        $CustomerTypes = CustomerType::all();
        $References = Reference::all();
        $CompanyReferences = CompanyReference::all();
        $rooms = Room::join('floors', 'rooms.floor_id', '=', 'floors.id')
            ->join('room_types', 'rooms.room_type_id', '=', 'room_types.id')
            ->leftJoin('room_conditions', function ($join) {
                $join->on('rooms.id', '=', 'room_conditions.room_id')
                    ->where('room_conditions.room_service_id', '=', 7)
                    ->where('room_conditions.status', ['dirty', 'need_repairing']);
            })
            ->whereNotIn('rooms.id', [$booking->room_id])
            ->whereNull('room_conditions.id') // Only include rooms where room_conditions are not met
            ->select(
                'rooms.*',
                'floors.name as floor_name',
                'room_types.name as room_type_name'
            )
            ->get();
        // dd($rooms);
        $property_id = $property->id;
        return view('admin.booking.edit', compact('CustomerTypes', 'References', 'id', 'CompanyReferences', 'property_id', 'booking', 'rooms', 'complementaryFood', 'pickAndDrop'));
    }
    public function Summary(Request $request, Property $property)
    {
        $booking = BookingRoom::join('rooms', 'booking_rooms.room_id', 'rooms.id')
            ->join('customers', 'booking_rooms.customer_id', 'customers.id')
            ->join('customer_types', 'booking_rooms.customer_type_id', 'customer_types.id')
            ->leftJoin('references', 'booking_rooms.reference_id', 'references.id')
            ->leftJoin('company_references', 'booking_rooms.company_reference_id', 'company_references.id')
            ->select(
                'rooms.name',
                'booking_rooms.*',
                'customers.name as custommer_name',
                'customers.email',
                'customers.cnic',

            )->get();
        if ($request->ajax()) {
            return DataTables::of($booking)->addIndexColumn()->make(true);
        }
        // dd($booking);
        return view('admin.booking.summary', compact('booking'));
    }

    public function arrivalbooking(Request $request, Property $property)
    {
        try {
            $booking = BookingRoom::find($request->booking_id);
            $booking->update($request->only(['arrivalDateTime']));

            return redirect('/properties/' . $property->id . '/booking')->with('success', 'Booking updated successfully');
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }
    public function departurebooking(Request $request, Property $property)
    {
        try {
            $booking = BookingRoom::find($request->booking_id);
            $room_condition = RoomCondition::where('room_id', $request->room_id)
                ->where('room_service_id', 7)
                ->first();
            $room = Room::find($request->room_id)->first();
            $room->update(['status' => 'occupied']);
            $booking->update($request->only(['departureDateTime']));
            // dd($room_condition);

            $room_condition->update([
                'status' => 'dirty',
                'description' => 'dirty',
            ]);

            return redirect('/properties/' . $property->id . '/booking')->with('success', 'Booking updated successfully');
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }
    public function rentbooking(Request $request, Property $property)
    {
        try {
            // dd($request->all());

            $VehicleBooking = $request->only(['vehicle_id', 'fromRentDateTime', 'toRentDateTime', 'package_type', 'booking_id', 'property_id']);

            VehicleBooking::create($VehicleBooking);

            return redirect('/properties/' . $property->id . '/booking')->with('success', 'Vehicle Booking Created successfully');
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }
    public function cancelbooking(Property $property, $id)
    {
        try {
            $booking = BookingRoom::findOrFail($id);
            $booking->cancel = 1;
            $booking->save();


            $room_condition = RoomCondition::where('room_id', $booking->room_id)
                ->where('room_service_id', 7)
                ->first();
            $room = Room::where('id', $booking->room_id)
                ->first();

            $room->update([
                'status' => 'occupied',
            ]);
            $room_condition->update([
                'status' => 'dirty',
                'description' => 'dirty',
            ]);



            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Failed to delete booking');
        }
    }
    public function getCustomer(Property $property, $id)
    {
        // Fetch the customer data based on the provided ID
        $customer = Customer::find($id);

        if (!$customer) {
            // If customer not found, return a JSON response with an error message
            return response()->json(['error' => 'Customer not found'], 404);
        }

        // If customer found, return a JSON response with the customer data
        return response()->json($customer);
    }
    public function customer_history(Property $property, $id)
    {
        $customer_history = BookingRoom::join('customers', 'booking_rooms.customer_id', 'customers.id')
            ->join('rooms', 'booking_rooms.room_id', 'rooms.id')
            ->where('customer_id', $id)
            ->where('property_id', $property->id)
            ->select(
                'booking_rooms.*',
                'rooms.name as room_name',
                'customers.name',
                'customers.email',
                'customers.cnic'
            )->get();
        // dd($customer_history);

        // If customer found, return a JSON response with the customer data
        return response()->json($customer_history);
    }
    public function calender(Request $request, Property $property)
    {
        // $roomTypes = RoomType::all();

        if ($request->ajax()) {
            $rooms = Room::join('floors', 'rooms.floor_id', 'floors.id')
                ->join('room_types', 'rooms.room_type_id', 'room_types.id')
                ->leftjoin('booking_rooms', 'rooms.id', 'booking_rooms.room_id')
                ->where('floors.property_id', $property->id)
                ->WhereNull('booking_rooms.departureDateTime')
                ->Where('booking_rooms.cancel', '0')
                ->whereNot('rooms.status', 'available')
                ->select(
                    // 'rooms.*',
                    // 'floors.name as floor_name',
                    // 'room_types.name as room_type_name',
                    // 'booking_rooms.fromDateTime as fromDateTime',
                    // 'booking_rooms.toDateTime as toDateTime',
                    'rooms.id',
                    'rooms.name as title',
                    'booking_rooms.fromDateTime as start',
                    'booking_rooms.toDateTime as end',
                    // 'rooms.all_day',
                    'rooms.name as calendar',

                )
                ->get();

            $events = $rooms->map(function ($room) {
                return [
                    'id' => $room->id,
                    'url' => '', // Add URL if needed
                    'title' => $room->title,
                    'start' => $room->start,
                    'end' => $room->end,
                    // 'allDay' => $room->all_day,
                    'extendedProps' => [
                        'calendar' => $room->calendar,
                    ],
                ];
            });
            // dd($events);

            return response()->json(['events' => $events]);
        }
        $floors = Floor::where('property_id', $property->id)
            ->with(['rooms'])
            ->get();
        // dd($rooms);

        return view('admin.booking.calender', compact('floors'));
    }
}
