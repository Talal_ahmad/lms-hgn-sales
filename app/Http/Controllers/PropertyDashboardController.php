<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\BookingRoom;
use App\Models\User;
use Spatie\Permission\Models\Role;
use App\Models\Property;
use App\Models\Floor;
use App\Models\Order;
use App\Models\Room;
use App\Models\RoomCondition;
use DB;
use DataTables;


class PropertyDashboardController extends Controller
{
    public function index(Request $request, Property $property)
    {

        $gender_wise = Customer::where('property_id', $property->id)
            ->selectRaw('SUM(CASE WHEN gender = "female" THEN 1 ELSE 0 END) as female_count')
            ->selectRaw('SUM(CASE WHEN gender = "male" THEN 1 ELSE 0 END) as male_count')
            ->selectRaw('SUM(CASE WHEN gender = "other" THEN 1 ELSE 0 END) as other_count')
            ->selectRaw('COUNT(*) as total_count')
            ->first();

        $female_count = $gender_wise->female_count;
        $male_count = $gender_wise->male_count;
        $other_count = $gender_wise->other_count;
        $total_count = $gender_wise->total_count;

        if ($female_count !== null && $male_count !== null && $other_count !== null && $total_count !== null) {
            // Calculate percentages
            $female_percentage = ($female_count / $total_count) * 100;
            $male_percentage = ($male_count / $total_count) * 100;
            $other_percentage = ($other_count / $total_count) * 100;

            $data['gender']['female_percentage'] = $female_percentage;
            $data['gender']['male_percentage'] = $male_percentage;
            $data['gender']['other_percentage'] = $other_percentage;

            // Generate comma-separated string of percentages
            $data_string = implode(',', [
                $data['gender']['female_percentage'],
                $data['gender']['male_percentage'],
                $data['gender']['other_percentage']
            ]);
        } else {
            $data_string = 0;
        }

        $checkIn_count = BookingRoom::join('customers', 'booking_rooms.customer_id', 'customers.id')
            ->where('customers.property_id', $property->id)
            ->whereNull('booking_rooms.departureDateTime')
            ->whereNotNull('booking_rooms.arrivalDateTime')
            ->count();
        $booking_count = BookingRoom::join('customers', 'booking_rooms.customer_id', 'customers.id')
            ->join('rooms', 'booking_rooms.room_id', 'rooms.id')
            ->where('customers.property_id', $property->id)
            ->where('rooms.status', 'Booked')
            ->whereNotNull('booking_rooms.fromDateTime')
            ->whereNotNull('booking_rooms.toDateTime')
            // ->whereNull('booking_rooms.arrivalDateTime')
            ->whereNull('booking_rooms.departureDateTime')
            ->count();
        $customer_count = Customer::where('property_id', $property->id)->count();
        $room_count = Room::Where('status', 'available')->count();

        $house_keeper_dirty = RoomCondition::join('room_services', 'room_conditions.room_service_id', 'room_services.id')
            ->where('room_conditions.room_service_id', 7)
            ->selectRaw('SUM(CASE WHEN room_conditions.status = "dirty" THEN 1 ELSE 0 END) as dirty_count')
            ->selectRaw('SUM(CASE WHEN room_conditions.status = "satisfied" THEN 1 ELSE 0 END) as satisfied_count')
            ->first();

        $house_keeper_repair_count = RoomCondition::join('room_services', 'room_conditions.room_service_id', 'room_services.id')
            ->whereIn('room_conditions.room_service_id', range(1, 6))
            ->where('room_conditions.status', 'need_repairing')
            ->distinct('room_conditions.room_id')
            ->count();
        // dd($house_keeper_repair_count);


        $total_customers = Customer::where('property_id', $property->id)->count();
        $total_orders = Order::where('property_id', $property->id)->count();
        $total_booking = BookingRoom::join('customers', 'booking_rooms.customer_id', 'customers.id')
            ->where('property_id', $property->id)->count();
        $customer = Customer::where('property_id', $property->id)->get();

        $roomCounts = Room::join('floors', 'rooms.floor_id', 'floors.id')
            ->where('floors.property_id', $property->id)
            ->selectRaw('SUM(CASE WHEN rooms.status = "available" THEN 1 ELSE 0 END) as available_count')
            ->selectRaw('SUM(CASE WHEN rooms.status = "Booked" THEN 1 ELSE 0 END) as booked_count')
            ->selectRaw('COUNT(*) as total_count')
            ->first();
        $availableCount = $roomCounts->available_count;
        $bookedCount = $roomCounts->booked_count;
        $totalRoomCount = $roomCounts->total_count;


        // Calculate total room count
        $totalCount = $availableCount + $bookedCount;

        // Calculate average percentages
        $avgAvailablePercentage = $totalCount > 0 ? round(($availableCount / $totalCount) * 100) : 0;
        $avgBookedPercentage = $totalCount > 0 ? round(($bookedCount / $totalCount) * 100) : 0;

        $roomTypeCounts = Room::join('room_types', 'rooms.room_type_id', 'room_types.id')
            ->select(
                'room_types.name as room_type',
                DB::raw('count(*) as total_rooms'),
                DB::raw('sum(case when rooms.status = "Booked" then 1 else 0 end) as booked_rooms'),
                DB::raw('sum(case when rooms.status = "available" then 1 else 0 end) as available_rooms')
            )
            ->groupBy('room_types.name')
            ->get();
        // dd($roomTypeCounts);


        foreach ($roomTypeCounts as $key => $mf) {
            $totalRooms = $mf->total_rooms;
            $bookedRooms = $mf->booked_rooms;

            // Avoid division by zero
            $mf->avg_booking_rate = ($totalRooms > 0) ? ($bookedRooms / $totalRooms) * 100 : 0;

            $data[$mf->test][] = $mf->booked_rooms;
            $tests[] = !empty($mf->room_type) ? $mf->room_type : '';
            $values[] = !empty($mf->avg_booking_rate) ? $mf->avg_booking_rate : 0;
        }
        $tests_name = "'" . implode("','", !empty($tests) ? $tests : []) . "'";
        $tests_values = implode(',', !empty($values) ? $values : []);


        // dd($tests_values);


        if ($request->ajax()) {
            return DataTables::of($customer)->addIndexColumn()->make(true);
        }
        return view('admin.property.dashboard', get_defined_vars());
    }
}
