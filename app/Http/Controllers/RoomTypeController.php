<?php

namespace App\Http\Controllers;

use App\Models\Property;
use App\Models\RoomType;
use App\Http\Requests\RoomTypeRequest;
use DataTables;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class RoomTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Property $property)
    {
        $RoomType = RoomType::all();
        // dd($RoomType);
        if ($request->ajax()) {
            return DataTables::of($RoomType)->addIndexColumn()->make(true);
        }
        return view('admin.roomType.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RoomTypeRequest $request)
    {
        try {
            DB::transaction(function () use ($request) {
                RoomType::create($request->validated());
            });
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Property $property, RoomType $roomType)
    {
        return response()->json($roomType);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(RoomTypeRequest $request, Property $property, RoomType $roomType)
    {
        try {
            DB::transaction(function () use ($request, $roomType) {
                $roomType->update($request->validated());
            });

            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Property $property, RoomType $roomType)
    {
        try {
            $roomType->delete();

            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
