<?php

namespace App\Http\Controllers;

use App\Models\CustomerType;
use App\Models\Property;
use App\Http\Requests\CustomerTypeRequest;
use DataTables;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class CustomerTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Property $property)
    {
        $Customer_type = CustomerType::all();
        // dd($Customer_type);
        if ($request->ajax()) {
            return DataTables::of($Customer_type)->addIndexColumn()->make(true);
        }
        return view('admin.customerType.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CustomerTypeRequest $request)
    {
        try {
            DB::transaction(function () use ($request) {
                CustomerType::create($request->validated());
            });
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(CustomerType $customerType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Property $property, CustomerType $customerType)
    {
        return response()->json($customerType);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CustomerTypeRequest $request, Property $property, CustomerType $customerType)
    {
        try {

            DB::transaction(function () use ($request, $customerType) {
                $customerType->update($request->validated());
            });

            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Property $property, CustomerType $customerType)
    {
        try {
            $customerType->delete();

            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
