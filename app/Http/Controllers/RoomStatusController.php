<?php

namespace App\Http\Controllers;

use App\Models\Floor;
use App\Models\Property;
use App\Models\RoomService;
// use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoomStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Property $property)
    {

        // Method 1: using 'Lazy Eager Loading' and 'loadCount()'
        // counts the number of rooms that has atleast one 'roomConditions' relationship with 'status' = 'need_repairing'
        $floors = $property->floors
            ->load(['rooms.roomConditions.roomService'])
            ->loadCount([
                'rooms as rooms_need_repairing_count' => function ($query) {
                    $query->whereHas('roomConditions', function ($query) {
                        $query->where('status', 'need_repairing')
                        ->orWhere('status', 'dirty');
                    });
                }
            ]);
        // dd($floors);

        $room_services = RoomService::all();
        return view('admin.roomStatus.index', compact('floors', 'room_services'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
