<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\RoomTypeCollection;
use App\Models\RoomType;
use App\Models\Room;
use App\Models\Floor;

class RoomTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $roomTypes = RoomType::all();
        return new RoomTypeCollection($roomTypes);
    }
    public function roomfloor()
    {
        $rooms = Floor::join('rooms', 'floors.id', '=', 'rooms.floor_id')
            ->join('room_types', 'room_types.id', '=', 'rooms.room_type_id')
            ->join('properties', 'properties.id', '=', 'floors.property_id')
            ->join('property_types', 'property_types.id', '=', 'properties.property_type_id')
            ->select('rooms.id', 'rooms.name', 'rooms.capacity', 'rooms.price', 'rooms.cover_image', 'rooms.other_images', 'floors.name as floorname', 'properties.id as property_id', 'properties.name as propertiesName', 'properties.address as propertiesAddress')
            ->get();
        return response()->json([
            'rooms' => $rooms,
        ]);
    }
}
