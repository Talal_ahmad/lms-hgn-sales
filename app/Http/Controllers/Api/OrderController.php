<?php

namespace App\Http\Controllers\Api;

use App\Models\Order;
use App\Models\Property;
use App\Models\Customer;
use App\Models\Table;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderResource;
use App\Http\Requests\OrderRequest;
use Illuminate\Http\Request;
use App\Events\OrderPlaced;
use Carbon\Carbon;
use DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Property $property)
    {
        if ($request->dateFilter != 'Invalid Date') {
            $date = $request->dateFilter;
        } else {
            $date = Carbon::today()->toDateString();
        }

        $orders = $property->orders()->with(['reservation', 'customer'])->whereDate('orders.created_at', $date)->get();
        return new OrderCollection($orders);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(OrderRequest $request, Property $property)
    {
        $order = null;
        DB::transaction(function () use ($request, $property, &$order) {
            $customerData = (array)$request->input('customer');
            if (isset($request->customer['id']) && !empty($request->customer['id'])) {
                $customer = (object) array('id' => $request->customer['id']);
            } else {
                $customer = $this->createCustomer($property);
            }
            $order = Order::create([
                'property_id' => $property->id,
                'reservation_id' => $request->reservation_id,
                'customer_id' => !empty($customer) ? $customer->id : NULL,
                'order_number' => $this->getNextOrderNumber($property),
                'type' => $request->type,
                'price' => $request->total_price,
                'payment_method' => $request->payment_method,
                'discount' => $request->discount,
                'service_charges' => $request->service_charges,
            ]);
            foreach ($request->orderedItems as $orderedItem) {
                $order->orderDetails()->create([
                    'menu_item_id' => $orderedItem['id'],
                    'menu_serving_id' => $orderedItem['menu_serving_id'] ?? null,
                    'menu_serving_quantity_id' => $orderedItem['menu_serving_quantity_id'] ?? null,
                    'price' => $orderedItem['price'],
                    'quantity' => $orderedItem['quantity'],
                    'total_price' => $orderedItem['total_price'],
                ]);
            }
        });
        $orderDetail = $order->load(['customer', 'orderDetails.menuItem', 'orderDetails.menuServing', 'orderDetails.menuServingQuantity']);
        event(new OrderPlaced($property, $orderDetail));
        return new OrderResource($orderDetail);
    }

    /**
     * Display the specified resource.
     */
    public function show(Property $property, Order $order)
    {
        if ($order->property_id != $property->id) {
            abort(401);
        }

        return new OrderResource($order->load(['customer', 'orderDetails.menuItem', 'orderDetails.menuServing', 'orderDetails.menuServingQuantity']));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Property $property, Order $order)
    {
        if ($order->property_id != $property->id) {
            abort(401);
        }

        DB::transaction(function () use ($request, $property, &$order) {
            $customerData = (array)$request->input('customer');

            $customer = $this->createCustomer($property);
            $order->update([
                'property_id' => $property->id,
                'reservation_id' => $request->reservation_id,
                'customer_id' => !empty($customer) ? $customer->id : NULL,
                'type' => $request->type,
                'price' => $request->total_price,
                'payment_method' => $request->payment_method,
                'discount' => $request->discount,
                'service_charges' => $request->service_charges,
            ]);
            $order->orderDetails()->delete();
            foreach ($request->orderedItems as $orderedItem) {
                $order->orderDetails()->create([
                    'menu_item_id' => $orderedItem['menu_item_id'] ?? $orderedItem['id'],
                    'menu_serving_id' => $orderedItem['menu_serving_id'] ?? null,
                    'menu_serving_quantity_id' => $orderedItem['menu_serving_quantity_id'] ?? null,
                    'price' => $orderedItem['price'],
                    'quantity' => $orderedItem['quantity'],
                    'total_price' => $orderedItem['total_price'],
                ]);
            }
        });
        $orderDetail = $order->load(['customer', 'orderDetails.menuItem', 'orderDetails.menuServing', 'orderDetails.menuServingQuantity']);
        event(new OrderPlaced($property, $orderDetail));
        return new OrderResource($orderDetail);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Property $property, Order $order)
    {
        if ($order->property_id != $property->id) {
            abort(401);
        }

        $order->orderDetails->map->delete();
        $order->delete();
        return response()->json([
            'message' => 'Order Deleted Successfully!',
        ]);
    }

    private function getNextOrderNumber($property)
    {
        $lastOrder = $property->orders()->latest()->first();

        if ($lastOrder) {
            $currentOrderNumber = $lastOrder->order_number;
            $nextOrderNumber = $currentOrderNumber + 1;
        } else {
            $nextOrderNumber = 1;
        }

        return $nextOrderNumber;
    }

    private function createCustomer($property)
    {
        $customerData = [
            'property_id' => $property->id,
            'type' => 'walk-in',
            'name' => 'Walk In Customer',
            // 'email' => $customer['email'] ?? null,
            // 'phone_number' => $customer['phone_number'] ?? null,
        ];
        $customer = Customer::updateOrcreate($customerData);
        return $customer;
    }

    private function findTableCurrentReservation($table)
    {
        $table = Table::find($table);
        return $table->currentReservation;
    }

    public function getKitchenListing(Property $property)
    {
        $orders = $property->orders()->where('status', 'pending')->with(['orderDetails.menuItem', 'orderDetails.menuServing', 'orderDetails.menuServingQuantity'])->orderBy('created_at')->get();

        return response()->json([
            'data' => $orders,
        ]);
    }

    public function markReady(Property $property, Order $order)
    {
        if ($order->property_id != $property->id) {
            abort(401);
        }

        $order->update([
            'status' => 'completed',
        ]);
        return response()->json([
            'message' => 'Order Status Updated Successfully!',
        ]);
    }
}
