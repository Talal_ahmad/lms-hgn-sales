<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MenuServingCollection;
use App\Http\Resources\MenuServingResource;
use App\Models\MenuServing;
use App\Models\Property;
use Illuminate\Http\Request;
use App\Http\Requests\MenuServingRequest;



class MenuServingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Property $property)
    {
        $menuServings = $property->menuServings()->with('menuCategory')->get();
        return new MenuServingCollection($menuServings);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MenuServingRequest $request, Property $property)
    {
        $request->merge(['property_id' => $property->id]);
        $MenuServing = MenuServing::create($request->all());
        return new MenuServingResource($MenuServing);
    }

    /**
     * Display the specified resource.
     */
    public function show(Property $property, MenuServing $menuServing)
    {
        if ($menuServing->property_id != $property->id) {
            abort(401);
        }

        return new MenuServingResource($menuServing);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MenuServingRequest $request, Property $property, MenuServing $menuServing)
    {
        if ($menuServing->property_id != $property->id) {
            abort(401);
        }

        $request->merge(['property_id' => $property->id]);
        $menuServing->update($request->all());
        return new MenuServingResource($menuServing);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Property $property, MenuServing $menuServing)
    {
        if ($menuServing->property_id != $property->id) {
            abort(401);
        }
        
        $menuServing->delete();
        return response()->json([
            'message' => 'Menu Serving Deleted Successfully!',
        ]);
    }

    public function importFromProperty(Property $property, Property $fromProperty){
        $servings = $fromProperty->menuServings()->with('menuCategory')->get();
        foreach ($servings as $serving) {
            $category = $property->menuCategories()->where('name', $serving->menuCategory->name)->first();
            $category->menuServings()->updateOrCreate([
                'name' => $serving->name, 
            ],[
                'property_id' => $property->id, 
                'name' => $serving->name, 
                'is_quantity' => $serving->is_quantity, 
            ]);
        }
        return response()->json([
            'message' => 'Menu Servings Imported Successfully!',
        ]);
    }
}
