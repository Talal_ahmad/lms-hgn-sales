<?php

namespace App\Http\Controllers;

use App\Http\Resources\FloorCollection;
use App\Http\Resources\UserCollection;
use App\Models\PropertyType;
use App\Models\Room;
use App\Models\RoomType;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\FloorRequest;
use App\Http\Resources\FloorResource;
use App\Models\RoomCondition;
use App\Models\Floor;
use App\Models\Property;
use DB;
use DataTables;
use Illuminate\Validation\Rule;
use App\Rules\UniqueChildsNamesPerFloor;
use App\Rules\UniqueFloorChildsNames;
use App\Rules\UniqueFloorNames;
use Illuminate\Validation\ValidationException;
use Storage;

class FloorController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Property $property)
    {
        // $roomTypes = RoomType::all(["id", "name"]);
        // dd($roomTypes);
        // dd($property);
        // dd($property->propertyType->name);

        $floors = $property->floors;

        if ($request->ajax()) {
            return DataTables::of($floors)->addIndexColumn()->make(true);
        }
        // if propertyType is "Hotel", send roomTypes
        if ($property->propertyType->name === "Hotel") {

            $data["roomTypes"] = RoomType::all(["id", "name"]);
        }

        $data["propertyType"] = $property->propertyType->name;

        return view('admin.property.floor.index', $data);

        // return view('admin.property.floor.index', $data)
        //        ->with(["propertyType" => $property->propertyType->name]);

        // return view('admin.property.floor.index')
        //        ->with(["propertyType" => $property->propertyType->name]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(FloorRequest $request, Property $property)
    {

        DB::transaction(function () use ($request, $property) {
            foreach ($request->floors as $floor) {

                $floorObj = $property->floors()->create($floor);

                if ($property->propertyType->name === "Hotel") {

                    foreach ($floor['rooms'] as $room) {

                        $roomObj = new Room();
                        $roomObj->room_type_id = $room['room_type_id'];
                        $roomObj->name = $room['name'];
                        $roomObj->capacity = $room['capacity'];
                        $roomObj->price = $room['price'];

                        // store Cover Image
                        if (!empty($room['cover_image'])) {

                            // $coverImageName = $roomObj->id + "-cover-" . $room['cover_image']->hashName(); // Generate a unique, random name (it includes extension)
                            $coverImageName = "cover-" . $room['cover_image']->hashName(); // Generate a unique, random name (it includes extension)

                            $room['cover_image']->storeAs("images/rooms", $coverImageName, "public");

                            $roomObj->cover_image = $coverImageName;
                        }

                        // store Other Images
                        if (!empty($room['other_images'])) {

                            $imagesNameArray = [];

                            foreach ($room['other_images'] as $image) {

                                $imageNewName = $image->hashName(); // Generate a unique, random name (it includes extension)

                                // storeAs() accepts 'path relative to disk root path', 'file name', 'disk name'
                                // returns the path as sepcified in storeAs() method (i.e., 'path relative to disk root path') but not the complete absolute or relative path
                                $path = $image->storeAs("images/rooms", $imageNewName, "public");
                                $imagesNameArray[] = $imageNewName;
                            }

                            $roomObj->other_images = implode("|", $imagesNameArray);
                        }
                        $floorObj->rooms()->save($roomObj);
                        $RoomCondition = new RoomCondition();
                        $RoomCondition->room_id = $roomObj->id; // Corrected to use the roomObj ID
                        $RoomCondition->room_service_id = 7; // Assuming room_service_id is an integer
                        $RoomCondition->status = 'satisfied';
                        $RoomCondition->save();
                    }
                } elseif ($property->propertyType->name === "Restaurant") {

                    foreach ($floor['tables'] as $table) {
                        // dd($floor['tables']);
                        $floorObj->tables()->create($table);
                    }
                }
            }
        });
        return response()->json([
            'message' => 'Floors Created Successfully!',
        ]);
    }



    /**
     * Display the specified resource.
     */
    public function edit(Property $property, Floor $floor)
    {

        if ($floor->property_id != $property->id) {
            abort(401);
        }

        $data = [];
        // $data["propertyType"] = $property->propertyType->name;
        // $data["floor"] = $floor;
        // $data["floor"] = $floor->load(["rooms", "tables"]); // will send one empty string of "empty relationship model"

        // if propertyType is "Hotel", send roomTypes
        if ($property->propertyType->name === "Hotel") {

            $data["roomTypes"] = RoomType::all(["id", "name"]);
            $data["floor"] = $floor->load("rooms");
            // $data["rooms"] = $floor->rooms;  // with it, rooms are sent 2 times, one with "$floor->rooms (rooms)" and one with "$floor (floor.rooms)"

        } elseif ($property->propertyType->name === "Restaurant") {

            $data["floor"] = $floor->load("tables");
        }

        // return response()->json($data);
        return view("admin.property.floor.edit", $data);
    }

    /**
     * Display the specified resource.
     */
    public function show(Property $property, Floor $floor)
    {
        if ($floor->property_id != $property->id) {
            abort(401);
        }

        return new FloorResource($floor);
    }

    /**
     * Update the specified resource in storage.
     */
    // public function update(Request $request, Property $property, Floor $floor)
    public function update(FloorRequest $request, Property $property, Floor $floor)
    {
        // dd($request->rooms[0]['id']);
        // $roomsDbArray = $floor->rooms()->where('id', '=', $request->rooms[0]['id'])->get();
        // dd($roomsDbArray);

        if ($floor->property_id != $property->id) {
            abort(401);
        }

        DB::transaction(function () use ($request, $property, $floor) {
            $floor->update($request->validated());

            if ($property->propertyType->name === "Hotel") {

                // if ($request->has('rooms')) {
                if ($request->filled('rooms')) {
                    // remove false entries from rooms' id collection
                    $roomIds = collect($request->rooms)->pluck('id')->filter();

                    // delete those rooms that are not sent from frontend (deleted in frontend)
                    $floor->rooms()->whereNotIn('id', $roomIds)->delete();

                    $path = storage_path('app/public/images/rooms/');
                    $coverImageName = null;
                    $otherImagesName = null;
                    // get all floor's rooms from DB as an Array
                    $roomsDbArray = $floor->rooms()->get()->toArray();

                    // Note:
                    // the order of existing rooms from DB and from Frontend is Same, so we loop through the Rooms present in Request
                    // and check whether on that index of "$request->rooms" Array, there exists any Room extracted from DB (present in "$roomsDbArray").
                    // we are just checking using the index ($key) present if "$request->rooms as $key => $room".
                    foreach ($request->rooms as $key => $room) {

                        if (!empty($room['cover_image'])) {

                            // check if old Cover Image are already present in "database"
                            if ($roomsDbArray[$key]['cover_image']) {
                                // delete old Cover Image, if it is already present in "storage"
                                if (Storage::disk('public')->exists($path . $roomsDbArray[$key]['cover_image'])) {
                                    Storage::disk('public')->delete($path . $roomsDbArray[$key]['cover_image']);
                                }
                            }
                            // change image name and Store it in Storage and return "image name"
                            $coverImageName = "cover-" . $room['cover_image']->hashName(); // Generate a unique, random name (it includes extension)
                            $room['cover_image']->storeAs("images/rooms", $coverImageName, "public");
                        } else {
                            $coverImageName = $roomsDbArray[$key]['cover_image'] ?? null;
                        }

                        if (!empty($room['other_images'])) {

                            // check if old Other Images are already present in "database"
                            if ($roomsDbArray[$key]['other_images']) {
                                foreach (explode("|", $roomsDbArray[$key]['other_images']) as $imageName) {
                                    // delete old Other Images, if they are already present in "storage"
                                    if (Storage::disk('public')->exists($path . $imageName)) {
                                        Storage::disk('public')->delete($path . $imageName);
                                    }
                                }
                            }
                            // change images name and Store them in Storage and return "images name string"
                            $imagesNameArray = [];

                            foreach ($room['other_images'] as $image) {
                                $imageNewName = $image->hashName(); // Generate a unique, random name (it includes extension)
                                $image->storeAs("images/rooms", $imageNewName, "public");
                                $imagesNameArray[] = $imageNewName;
                            }
                            $otherImagesName = implode("|", $imagesNameArray);
                        } else {
                            $otherImagesName = $roomsDbArray[$key]['other_images'] ?? null;
                        }

                        // update or create, because we may send more rooms from frontend than those present in DB
                        $roomObj = $floor->rooms()->updateOrCreate(
                            ['id' => $room['id'] ?? 0],
                            [
                                'room_type_id' => $room['room_type_id'],
                                'name' => $room['name'],
                                'capacity' => $room['capacity'],
                                'price' => $room['price'],
                                'cover_image' => $coverImageName,
                                'other_images' => $otherImagesName,
                            ]
                        );
                        // Update or create RoomCondition based on room_id
                        RoomCondition::updateOrCreate(
                            ['room_id' => $roomObj->id],
                            [
                                'room_service_id' => 7,
                                'status' => 'satisfied',
                            ]
                        );
                    }
                }
                // delete all rooms (if all rooms are deleted at frontend)
                else {
                    $floor->rooms()->delete();
                }
            } elseif ($property->propertyType->name === "Restaurant") {

                if ($request->filled('tables')) {

                    $tableIds = collect($request->tables)->pluck('id')->filter();
                    $floor->tables()->whereNotIn('id', $tableIds)->delete();

                    foreach ($request->tables as $table) {
                        $floor->tables()->updateOrCreate(
                            ['id' => $table['id'] ?? 0],
                            $table
                        );
                    }
                }
                // delete all tables (if all tables are deleted at frontend)
                else {
                    $floor->tables()->delete();
                }
            }
        });

        return response()->json([
            'message' => 'Floor Updated Successfully!',
        ]);
    }



    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Property $property, Floor $floor)
    {
        if ($floor->property_id != $property->id) {
            abort(401);
        }

        if ($property->propertyType->name === "Hotel") {

            $path = storage_path('app/public/images/rooms/');

            foreach ($floor->rooms as $room) {

                if ($room->cover_image) {
                    // delete Cover Image, if it is already present in "storage"
                    if (Storage::disk('public')->exists($path . $room->cover_image)) {
                        Storage::disk('public')->delete($path . $room->cover_image);
                    }
                }

                if ($room->other_images) {
                    // delete Other Images, if it is already present in "storage"
                    foreach (explode("|", $room->other_images) as $imageName) {
                        // delete stored images
                        if (Storage::disk('public')->exists($path . $imageName)) {
                            Storage::disk('public')->delete($path . $imageName);
                        }
                    }
                }
            }

            $floor->rooms()->delete();
        } elseif ($property->propertyType->name === "Restaurant") {
            $floor->tables()->delete();
        }

        $floor->delete();

        return response()->json([
            'message' => 'Floor Deleted Successfully!',
        ]);
    }

    public function getFloorRooms(Request $request)
    {
        $rooms = Room::whereIn('floor_id',$request->floorIds)->get(['id','name']);
        return response()->json($rooms);
    }
}
