<?php

namespace App\Http\Requests;

use App\Rules\UniqueChildsNamesPerFloor;
use App\Rules\UniqueFloorChildsNames;
use App\Rules\UniqueFloorNames;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FloorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        // dd($this->all());
        
        $rules = [
            'floors' => 'required|array',
            'floors.*.name' => [
                'required',
                new UniqueFloorNames(),
                Rule::unique('floors', 'name')->where('property_id', $this->property->id),
            ],
            'floors.*.rooms' => 'nullable|array',
            'floors.*.rooms.*.room_type_id' => 'required_if:floors.*.tables,null|exists:room_types,id',
            'floors.*.rooms.*.name' => [
                'required_if:floors.*.tables,null',
                new UniqueChildsNamesPerFloor(),
            ],
            'floors.*.rooms.*.capacity' => 'required_if:floors.*.tables,null|integer|max:127',
            'floors.*.rooms.*.price' => 'required_if:floors.*.tables,null|integer',
            'floors.*.rooms.*.cover_image' => 'required_with_all:floors.*.rooms.*.other_images|image|mimes:jpg,jpeg,png,bmp,gif,svg,webp|max:2048',
            'floors.*.rooms.*.other_images' => 'nullable|array',
            'floors.*.rooms.*.other_images.*' => 'required_with_all:floors.*.rooms.*.other_images|image|mimes:jpg,jpeg,png,bmp,gif,svg,webp|max:2048',

            'floors.*.tables' => 'nullable|array',
            'floors.*.tables.*.name' => [
                'required_if:floors.*.rooms,null',
                new UniqueChildsNamesPerFloor(),
            ],
            'floors.*.tables.*.seating_capacity' => 'nullable|integer',
        ];
    
        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $floor = $this->route()->parameter('floor');
            $rules = [
                'name' => [
                    'required',
                    Rule::unique('floors', 'name')->where('property_id', $this->property->id)->ignore($this->route()->parameter('floor')),
                ],
                'rooms' => 'nullable|array',
                'rooms.*.room_type_id' => 'required_if:tables,null|exists:room_types,id',
                'rooms.*.name' => [
                    'required_if:tables,null',
                    new UniqueFloorChildsNames(),
                ],
                'rooms.*.capacity' => 'required_if:tables,null|integer|max:127',
                'rooms.*.price' => 'required_if:tables,null|integer',
                'rooms.*.cover_image' => 'required_with_all:rooms.*.other_images|image|mimes:jpg,jpeg,png,bmp,gif,svg,webp|max:2048',
                'rooms.*.other_images' => 'nullable|array',
                'rooms.*.other_images.*' => 'required_with_all:rooms.*.other_images|image|mimes:jpg,jpeg,png,bmp,gif,svg,webp|max:2048',
    
                'tables' => 'nullable|array',
                'tables.*.name' => [
                    'required_if:rooms,null',
                    new UniqueFloorChildsNames(),
                ],
                'tables.*.seating_capacity' => 'nullable|integer',
            ];
        }
    
        return $rules;
    }
    

}
