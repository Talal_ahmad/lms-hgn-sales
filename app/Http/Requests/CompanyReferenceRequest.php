<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyReferenceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'company_name' => 'required',
            'email_address' => 'required|email|unique:company_references,email_address',
            'phone_number' => 'required',
            'address' => 'required',
            'discount' => 'required',
            'company_commission' => 'required|numeric',
            // Add any other validation rules
        ];

        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $companyReference = $this->route()->parameter('company_reference');
            $rules['email_address'] = 'required|unique:company_references,email_address,' . ($companyReference ? $companyReference->id : null);
        }

        return $rules;
    }
}
