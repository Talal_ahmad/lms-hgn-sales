<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ThirdPartyVehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'company_name' => 'required',
            'email_address' => 'required|email|unique:third_party_vehicles,email_address',
            'phone_number' => 'required',
            'address' => 'required',
            'company_owner_name' => 'required',
            // Add any other validation rules
        ];

        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $ThirdPartyVehicle = $this->route()->parameter('third_party_vehicles');
            $rules['email_address'] = 'required|unique:third_party_vehicles,email_address,' . ($ThirdPartyVehicle ? $ThirdPartyVehicle->id : null);
        }

        return $rules;
    }
}
