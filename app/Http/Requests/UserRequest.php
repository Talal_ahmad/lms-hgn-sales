<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        // dd($this->all());

        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|max:12|confirmed',
            'role' => 'nullable|exists:roles,id',
            'property_id' => 'required|array',
            'property_id.*' => 'required|exists:properties,id',
            'train' => 'nullable',
        ];

        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $user = $this->route()->parameter('user');
            $rules['email'] = 'required|email|unique:users,email,' . $user->id;
            $rules['password'] = 'nullable|min:8|max:12';
        }

        return $rules;

    }
}
