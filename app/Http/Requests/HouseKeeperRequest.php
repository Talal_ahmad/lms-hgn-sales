<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HouseKeeperRequest extends FormRequest
{
     /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'user_id' => 'required|exists:users,id',
            'floor_ids' => 'required|array',
        ];

        // if (in_array($this->method(), ['PUT', 'PATCH'])) {
        //     $rules = [
        //         'user_id' => 'required|exists:users,id',
        //         'floor_id' => 'required|exists:floors,id',
        //     ];
        // }

        return $rules;
    }
}
