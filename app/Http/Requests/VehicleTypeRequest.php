<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VehicleTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'property_id' => 'required',
            'name' => 'required|unique:vehicle_types',
        ];

        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $vehicle_type = $this->route()->parameter('vehicle_types');
            if ($vehicle_type) {
                $rules['name'] = 'required|unique:menu_categories,name,' . $vehicle_type->id;
            }
        }

        return $rules;
    }
}
