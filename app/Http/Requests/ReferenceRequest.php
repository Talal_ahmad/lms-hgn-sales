<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReferenceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'reference_name' => 'required',
            'email_address' => 'required|email|unique:references,email_address',
            'phone_number' => 'required',
            'address' => 'required',
            'discount' => 'required',
            'reference_commission' => 'required|numeric',
        ];
        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $references = $this->route()->parameter('references');
            $rules['email_address'] = 'required|unique:references,email_address,' . ($references ? $references->id : null);
        }

        return $rules;
    }
}
