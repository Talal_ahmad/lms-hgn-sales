<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'property_id' => 'required',
            'vehicle_type_id' => 'required|exists:vehicle_types,id',
            'name' => 'required',
            'with_driver_payment' => 'required|numeric|min:0',
            'without_driver_payment' => 'required|numeric|min:0',
            'vehicle_number_plate' => 'required|unique:vehicles',
        ];

        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $vehicle = $this->route()->parameter('vehicle');
            if ($vehicle) {
                $rules['vehicle_number_plate'] = 'required|unique:vehicles,vehicle_number_plate,' . $vehicle->id;
            }
        }

        return $rules;
    }
}
