<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CheckTablePreReservationTimeOverlap;

class TableReservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'name' => 'required',
            'fromDateTime' => ['required', new CheckTablePreReservationTimeOverlap($this->input('table_id'), $this->input('fromDateTime'), $this->input('toDateTime'))],
        ];

        if ($this->isMethod('PUT') || $this->isMethod('PATCH')) {
            $rules['fromDateTime'] = ['required', new CheckTablePreReservationTimeOverlap($this->table_reservation->table->id, $this->input('fromDateTime'), $this->input('toDateTime'), $this->table_reservation->table->id)];
        }

        return $rules;
    }
}
