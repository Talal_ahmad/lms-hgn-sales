<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PropertyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'property_type_id' => 'required|exists:property_types,id',
            'name' => 'required|unique:properties',
            'address' => 'nullable',
        ];

        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $property = $this->route()->parameter('property');
            $rules['name'] = 'required|unique:properties,name,' . $property->id;
        }

        return $rules;
    }
}
