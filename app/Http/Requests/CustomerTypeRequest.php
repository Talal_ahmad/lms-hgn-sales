<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'name' => 'required|unique:customer_types',
            // Add any other validation rules for your CustomerType model
        ];

        // If the request method is PUT or PATCH, apply additional rules
        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $customer_type = $this->route()->parameter('customer_type');
            $rules['name'] = 'required|unique:customer_types,name,' . ($customer_type ? $customer_type->id : null);
        }

        return $rules;
    }
}
