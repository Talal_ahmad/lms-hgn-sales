<?php

use App\Models\Floor;
use Spatie\Permission\Models\Permission;
use Kutia\Larafirebase\Facades\Larafirebase;

if(!function_exists('properties')){
    function properties(){
        return \App\Models\Property::all();
    }
}

// Get Permissions According to Parent Name
if (!function_exists('permissionParentName')) {
    function permissionParentName($parent_name)
    {
        $permissions = Permission::where('parent_name', $parent_name)->select('id', 'display_name')->get();
        return $permissions;
    }
}

if (!function_exists('getProperty')) {
    function getProperty()
    {
        $routeParameters = \Route::current()->parameters;
        return array_key_exists('property', $routeParameters) ? (is_string($routeParameters['property']) ? $routeParameters['property'] : $routeParameters['property']->id) : null;
    }
}

if (!function_exists('totalRoomsNeedRepairingCount')) {
    function totalRoomsNeedRepairingCount()
    {
        $property = getProperty();
        // here '$property' is 'property id', if not, then change it with '$property->id' in 'where()' method
        // dd($property);

        $floors = Floor::where('property_id', $property)
                        ->withCount([
                            'rooms as rooms_need_repairing_count' => function ($query) {
                                $query->whereHas('roomConditions', function ($query) {
                                    $query->where('status', 'need_repairing');
                                });
                            }
                        ])
                        ->get();
        // dd($floors);
        $totalRoomsNeedRepairingCount = $floors->sum('rooms_need_repairing_count');

        return $totalRoomsNeedRepairingCount;
    }
    if (!function_exists('sendNotifications')) {
        function sendNotifications($fcmTokens, $data)
        {
            $notification = Larafirebase::withTitle($data['title'])
                ->withBody($data['body'])
                // ->withIcon($data['icon'])
                ->withClickAction($data['url'])
                ->withPriority($data['priority'])
                ->sendNotification($fcmTokens);
            return $notification->getStatusCode();
        }

    }

}
