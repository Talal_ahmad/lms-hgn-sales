<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Property extends Model
{
    use HasFactory;

    protected $fillable = [
        'property_type_id',
        'name',
        'address',
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($property) {
            $property->floors()->each(function ($floor) {
                $floor->delete();
            });
        });
    }

    public function propertyType()
    {
        return $this->belongsTo(PropertyType::class);
    }

    public function floors()
    {
        return $this->hasMany(Floor::class);
    }

    public function menuCategories()
    {
        return $this->hasMany(MenuCategory::class);
    }
    public function VehicleTypes()
    {
        return $this->hasMany(vehicleType::class);
    }
    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }

    public function menuServings()
    {
        return $this->hasMany(MenuServing::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }
}
