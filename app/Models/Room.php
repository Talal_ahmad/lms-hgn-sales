<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Room extends Model
{
    use HasFactory;

    protected $fillable = [
        'floor_id',
        'room_type_id',
        'name',
        'capacity',
        'price',
        'cover_image',
        'other_images',
        'status'
    ];

    public function floor()
    {
        return $this->belongsTo(Floor::class);
    }

    public function roomType()
    {
        return $this->belongsTo(RoomType::class);
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class);
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }
    public function images(): HasMany
    {
        return $this->hasMany(Image::class);
    }
    public function roomConditions(): HasMany
    {
        return $this->hasMany(RoomCondition::class);
    }
    public function bookings(): HasMany
    {
        return $this->hasMany(BookingRoom::class, 'room_id');
    }

    // protected static function boot()
    // {
    //     parent::boot();

    //     static::deleting(function ($room) {
    //         // Delete the associated images when the room is deleted
    //         $room->images()->delete();
    //     });
    // }
}
