<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PickAndDrop extends Model
{
    use HasFactory;
    protected $table = 'pick_and_drops';


    protected $fillable = [
        'booking_id',
        'pick',
        'drop',
        'PickDateTime',
        'fare'
    ];

    public function booking()
    {
        return $this->belongsTo(BookingRoom::class, 'booking_id');
    }
}
