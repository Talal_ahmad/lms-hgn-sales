<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    use HasFactory;

    protected $fillable = [
        'floor_id',
        'name',
        'seating_capacity',
        'status',
    ];

    public function floor()
    {
        return $this->belongsTo(Floor::class);
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    public function currentReservation()
    {
        return $this->hasOne(Reservation::class)
        ->with('customer')
        ->latest();
    }
}
