<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingRoom extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'customer_id',
        'customer_type_id',
        'reference_id',
        'description',
        'fromDateTime',
        'toDateTime',
        'arrivalDateTime',
        'departureDateTime',
        'company_reference_id',
        'room_id',
        'female_count',
        'male_count',
        'child_count',
        'infant_count',
        'stay_length',
        'shift_room_id',
        'complementary_food',

        // '_token', // Add this line to include the _token field
    ];
    // protected $casts = [
    //     'complementary_food' => 'array',
    // ];

    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class, 'room_id');
    }
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
    public function pickAndDrops()
    {
        return $this->hasMany(PickAndDrop::class, 'booking_id');
    }
}
