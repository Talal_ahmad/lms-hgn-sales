<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class RoomCondition extends Model
{
    use HasFactory;

    protected $fillable = [
        'room_id',
        'room_service_id',
        'status',
        'description'
    ];

    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class);
    }
    public function roomService(): BelongsTo
    {
        return $this->belongsTo(RoomService::class);
    }
    
}
