<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class RoomService extends Model
{
    use HasFactory;
    protected $fillable = [
        'name'
    ];

    public function roomCondition(): HasMany
    {
        return $this->hasMany(RoomCondition::class);
    }

}
