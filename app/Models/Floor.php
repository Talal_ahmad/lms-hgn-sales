<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    use HasFactory;

    protected $fillable = [
        'property_id',
        'name',
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($floor) {
            $floor->rooms()->delete();
            $floor->tables()->delete();
        });
    }

    public function property()
    {
        return $this->belongsTo(property::class);
    }

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    public function tables()
    {
        return $this->hasMany(Table::class);
    }
}
