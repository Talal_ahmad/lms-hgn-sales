<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class MenuCategory extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name',
        'property_id',
    ];

    public function property()
    {
        return $this->belongsTo(property::class);
    }

    public function menuServings()
    {
        return $this->hasMany(MenuServing::class);
    }

    public function menuItems()
    {
        return $this->hasMany(MenuItem::class);
    }
}
