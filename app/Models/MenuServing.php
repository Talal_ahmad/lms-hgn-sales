<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuServing extends Model
{
    use HasFactory;

    protected $fillable = [
        'property_id',
        'menu_category_id',
        'name',
        'is_quantity',
    ];

    public function property()
    {
        return $this->belongsTo(property::class);
    }

    public function menuCategory()
    {
        return $this->belongsTo(MenuCategory::class);
    }

    public function getIsQuantityAttribute($value){
        return $value ? true : false;
    }
}
