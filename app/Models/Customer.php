<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'property_id',
        'name',
        'email',
        'gender',
        'guest_category',
        'cnic_expired',
        'phone_number',
        'country_code',
        'dob',
        'type',
        'nationality',
        'cnic',
        'address',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }
    public function bookingRooms()
    {
        return $this->hasMany(BookingRoom::class, 'customer_id');
    }
}
