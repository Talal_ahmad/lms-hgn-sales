<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;

    protected $fillable = [
        'property_id',
        'vehicle_type_id',
        'name',
        'with_driver_payment',
        'without_driver_payment',
        'vehicle_number_plate'
    ];



    public function property()
    {
        return $this->belongsTo(Property::class, 'property_id');
    }

    /**
     * Get the menu category associated with the vehicle.
     */
    public function VehicleType()
    {
        return $this->belongsTo(VehicleType::class, 'vehicle_type_id');
    }
}
