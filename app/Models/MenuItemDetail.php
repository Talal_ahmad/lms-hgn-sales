<?php

namespace App\Models;

use App\Models\MenuCategory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuItemDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'menu_item_id',
        'menu_serving_id',
        'menu_serving_quantity_id',
        'price',
    ];

    public function menuItem()
    {
        return $this->belongsTo(MenuItem::class);
    }

    public function menuServing()
    {
        return $this->belongsTo(MenuServing::class)->where('is_quantity', 0);
    }

    public function menuServingQuantity()
    {
        return $this->belongsTo(MenuServing::class, 'menu_serving_quantity_id', 'id')->where('is_quantity', 1);
    }
}
