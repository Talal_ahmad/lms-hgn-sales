<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use OwenIt\Auditing\Contracts\Auditable;

class Reservation extends Model implements Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'type',
        'customer_id',
        'table_id',
        'room_id',
        'number_of_persons',
        'start_date_time',
        'end_date_time',
        'remarks',
    ];

    public function setStartDateTimeAttribute($value)
    {
        $this->attributes['start_date_time'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function setEndDateTimeAttribute($value)
    {
        $this->attributes['end_date_time'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function table()
    {
        return $this->belongsTo(Table::class);
    }

    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
