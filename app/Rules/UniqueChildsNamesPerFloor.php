<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class UniqueChildsNamesPerFloor implements Rule
{
    protected $attributeName;

    public function passes($attribute, $value)
    {
        $this->attributeName = $attribute;

        // Get the floor index from the attribute
        $floorIndex = $this->getFloorIndex($attribute);

        // Get the floors from the request data
        $floors = request()->input('floors', []);

        // Extract the names from the current floor based on the attribute name
        $names = collect($floors[$floorIndex][$this->getAttributeType()] ?? [])->pluck('name');

        // Check if names are unique within the floor
        $uniqueNames = $names->unique();

        return $names->count() === $uniqueNames->count();
    }

    public function message()
    {
        return __("The :attribute name is repeated more than once in a floor.", ['attribute' => $this->getAttributeType()]);
    }

    protected function getFloorIndex(string $attribute): ?int
    {
        $attributeSegments = explode('.', $attribute);
        return $attributeSegments[1] ?? null;
    }

    protected function getAttributeType(): string
    {
        // Extract the attribute type from the attribute name
        $segments = explode('.', $this->attributeName);
        return $segments[2] ?? 'unknown';
    }
}
