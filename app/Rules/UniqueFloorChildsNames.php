<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class UniqueFloorChildsNames implements Rule
{
    protected $attributeName;

    public function passes($attribute, $value)
    {
        $this->attributeName = $attribute;

        // Extract the names from the current floor based on the attribute name
        $names = collect(request($this->getAttributeType()))->pluck('name');

        // Check if names are unique within the floor
        $uniqueNames = $names->unique();

        return $names->count() === $uniqueNames->count();
    }

    public function message()
    {
        return __("The :attribute name is repeated more than once in a floor.", ['attribute' => $this->getAttributeType()]);
    }

    protected function getAttributeType(): string
    {
        // Extract the attribute type from the attribute name
        $segments = explode('.', $this->attributeName);
        return $segments[0] ?? 'unknown';
    }
}
