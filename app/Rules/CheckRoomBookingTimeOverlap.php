<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\BookingRoom;
use Carbon\Carbon;

class CheckRoomBookingTimeOverlap implements Rule
{
    protected $roomId;
    protected $fromDateTime;
    protected $toDateTime;
    protected $booking;

    public function __construct($roomId, $fromDateTime, $toDateTime, $booking = null)
    {
        $this->roomId = $roomId;
        $this->fromDateTime = Carbon::parse($fromDateTime);
        $this->toDateTime = Carbon::parse($toDateTime);
        $this->booking = $booking;
    }

    public function passes($attribute, $value)
    {
        // Check for existing bookings for the same room with overlapping time
        // $existingBookings = BookingRoom::where('room_id', $this->roomId)
        // ->where('toDateTime', '=>', $this->fromDateTime);
        // ->where(function ($query) {
        //     // $query->where(function ($q) {
        //     //     $q->where('fromDateTime', '>=', $this->fromDateTime)
        //     //         ->where('fromDateTime', '<', $this->toDateTime);
        //     // })
        //     $query->where(function ($q) {
        //         $q->where('toDateTime', '>', $this->fromDateTime)
        //             ->where('toDateTime', '<=', $this->toDateTime);
        //     });
        // });
        // if ($this->booking) {
        //     $existingBookings = $existingBookings->where('id', '!=', $this->booking->id);
        // }

        // $existingBookings = $existingBookings->first();
        // // dd($existingBookings);

        // return !$existingBookings;

        // Get existing bookings for the room
        $existingBookings = BookingRoom::where('room_id', $this->roomId)
            ->where(function ($query) {
                $query->where(function ($query) {
                    $query->where('fromDateTime', '<', $this->toDateTime)
                        ->where('toDateTime', '>', $this->fromDateTime);
                });
            })
            ->when($this->booking, function ($query) {
                // Exclude the current booking if it's an update
                $query->where('id', '!=', $this->booking->id);
            })
            ->exists();

        return !$existingBookings;
    }

    public function message()
    {
        return 'A booking already exists for this room against the selected time.';
    }
}
