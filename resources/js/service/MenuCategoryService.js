import axios from '../axios';
export default class MenuCategoryService {
    constructor(vm) {
        this.vm = vm;
    }
    getCategories(property) {
        let self = this.vm;
        return axios
            .get(`properties/${property}/menu-categories`)
            .then(function (response) {
                self.categories = response.data.data;
                if (self.menuCategories) {
                    self.menuCategories = response.data.data;
                }
            })
            .catch((error) => error);
    }
    storeCategory(formData) {
        let self = this.vm;
        let loader = self.$loading.show({
            container: self.$refs.addMenuCategoryForm
        });
        axios
        .post(`properties/${self.$store.state.property.id}/menu-categories`, formData)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: 'Category Created Successfully!', life: 1000 });
                loader.hide();
                self.$router.push({name : 'menu-categories'});
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    getCategory(id) {
        let self = this.vm;
        return this.vm.$axios
            .get(`properties/${self.$store.state.property.id}/menu-categories/${id}`)
            .then(function (response) {
                self.formData = response.data.data;
            })
            .catch((error) => error);
    }
    updateCategory(formData) {
        let self = this.vm;
        let loader = self.$loading.show({
            container: self.$refs.updateMenuCategoryForm
        });
        axios
        .put(`properties/${self.$store.state.property.id}/menu-categories/${formData.id}`, formData)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: 'Category Updated Successfully!', life: 1000 });
                loader.hide();
                self.$router.push({name : 'menu-categories'});
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    deleteCategory(id) {
        let self = this.vm;
        let loader = self.$loading.show();
        axios
        .delete(`properties/${self.$store.state.property.id}/menu-categories/${id}`)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: response.data.message, life: 1000 });
                loader.hide();
                self.menuCategoryService.getCategories();
            })
            .catch(function (error) {
                loader.hide();
                if(error.response){
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    import(id) {
        let self = this.vm;
        let loader = self.$loading.show();
        return axios
            .post(`properties/${self.$store.state.property.id}/import-menu-categories/${id}`)
            .then(function (response) {
                self.menuCategoryService.getCategories();
                self.importProperty = null;
                self.importCategoryDialog = false;
                self.$toast.add({ severity: 'success', summary: 'Success', detail: response.data.message, life: 1000 });
                loader.hide();
            })
            .catch(function (error) {
                loader.hide();
                if(error.response){
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
}
