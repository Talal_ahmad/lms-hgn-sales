import axios from '../axios';
import store from '../store';
export default class PropertyService {
    constructor(vm) {
        this.vm = vm;
    }
    getProperties(storeFlag = false) {
        let self = this.vm;
        if(!storeFlag){
            var loader = self.$loading.show();
        }
        return axios
            .get('properties')
            .then(function (response) {
                if(storeFlag){
                    localStorage.setItem('properties', JSON.stringify(response.data.data));
                    store.dispatch('setPropertyFromLocalStorage'); 
                }
                else{
                    self.properties = response.data.data;
                    loader.hide();
                }
            })
            .catch((error) => error);
    }
    storeProperty(formData) {
        let self = this.vm;
        let loader = self.$loading.show({
            container: self.$refs.propertyForm
        });
        axios
            .post('/properties', formData)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: 'Property Created Successfully!', life: 1000 });
                loader.hide();
                self.$router.push('/properties');
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    getProperty(id, storeFlag = false) {
        let self = this.vm;
        return axios
            .get('properties' + '/' + id)
            .then(function (response) {
                if(storeFlag){
                    localStorage.setItem('property', JSON.stringify(response.data.data));
                    store.dispatch('setPropertyFromLocalStorage'); 
                }
                else{
                    self.formData = response.data.data;
                }
            })
            .catch((error) => error);
    }
    updateProperty(formData) {
        let self = this.vm;
        let loader = self.$loading.show({
            container: self.$refs.propertyForm
        });
        axios
            .put('/properties' + '/' + formData.id, formData)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: 'Property Updated Successfully!', life: 1000 });
                loader.hide();
                self.$router.push('/properties');
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    deleteProperty(id) {
        let self = this.vm;
        let loader = self.$loading.show();
        axios
            .delete('/properties' + '/' + id)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: response.data.message, life: 1000 });
                loader.hide();
                self.propertyService.getProperties();
            })
            .catch(function (error) {
                loader.hide();
                if(error.response){
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
}
