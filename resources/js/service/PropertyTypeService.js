export default class PropertyTypeService {
    constructor(vm) {
        this.vm = vm;
    }
    getTypes() {
        let self = this.vm;
        let loader = self.$loading.show();
        return self.$axios
            .get('property-types')
            .then(function (response) {
                if (self.propertyTypes) {
                    self.propertyTypes = response.data.data;
                }
                loader.hide();
            })
            .catch((error) => error);
    }
}
