export default class ReportService {
    constructor(vm) {
        this.vm = vm;
    }
    getSaleReport(date) {
        let self = this.vm;
        let loader = self.$loading.show();
        return self.$axios
            .get(`properties/${self.$store.state.property.id}/sale-report?fromDate=${date.fromDate}&toDate=${date.toDate}`)
            .then(function (response) {
                self.report = response.data.Result;
                loader.hide();
            })
            .catch((error) => error);
    }
    
}
