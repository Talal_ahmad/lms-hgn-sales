export default class PermissionService {
    constructor(vm) {
        this.vm = vm;
    }
    getPermissions() {
        let self = this.vm;
        let loader = self.$loading.show();
        return self.$axios
            .get('permissions')
            .then(function (response) {
                if (self.permissions) {
                    self.permissions = response.data.data;
                }
                if (self.groupedPermissions) {
                    self.groupedPermissions = self.groupPermissionsByParentName(self.permissions);
                }
                loader.hide();
            })
            .catch((error) => error);
    }
}
