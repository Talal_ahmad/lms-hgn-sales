export default class RoomTypeService {
    constructor(vm) {
        this.vm = vm;
    }
    getTypes() {
        let self = this.vm;
        let loader = self.$loading.show();
        return self.$axios
            .get('room-types')
            .then(function (response) {
                if (self.roomTypes) {
                    self.roomTypes = response.data.data;
                }
                loader.hide();
            })
            .catch((error) => error);
    }
}
