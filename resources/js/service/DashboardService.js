export default class DashboardService {
    constructor(vm) {
        this.vm = vm;
    }
    index() {
        let self = this.vm;
        let loader = self.$loading.show();
        return self.$axios
            .get(`properties/${self.$store.state.property?.id}/dashboard`)
            .then(function (response) {
                self.cards = response.data.data.cards;
                loader.hide();
            })
            .catch((error) => error);
    }
}
