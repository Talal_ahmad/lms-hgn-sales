export default class TableReservationService {
    constructor(vm) {
        this.vm = vm;
    }
    getReservations() {
        let self = this.vm;
        let loader = self.$loading.show();
        return self.$axios
            .get(`properties/${self.$store.state.property.id}/table-reservations`)
            .then(function (response) {
                self.reservations = response.data.data;
                loader.hide();
            })
            .catch((error) => error);
    }
    getCreateData() {
        let self = this.vm;
        let loader = self.$loading.show();
        return self.$axios
            .get(`properties/${self.$store.state.property.id}/table-reservations/create`)
            .then(function (response) {
                self.tables = response.data.data;
                loader.hide();
            })
            .catch((error) => error);
    }
    storeReservation(formData) {
        let self = this.vm;
        let loader = self.$loading.show({
            container: self.$refs.AddReservationForm
        });
        self.$axios
        .post(`properties/${self.$store.state.property.id}/table-reservations`, formData)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: 'Reservations Created Successfully!', life: 1000 });
                loader.hide();
                self.$router.push({name : 'reservations'});
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    getReservation(id) {
        let self = this.vm;
        let loader = self.$loading.show();
        return this.vm.$axios
            .get(`properties/${self.$store.state.property.id}/table-reservations/${id}`)
            .then(function (response) {
                loader.hide();
                if(self.preFillForm){
                    self.preFillForm(response.data.data);
                }
            })
            .catch((error) => error);
    }
    updateReservation(formData) {
        let self = this.vm;
        let loader = self.$loading.show({
            container: self.$refs.UpdateReservationForm
        });
        self.$axios
        .put(`properties/${self.$store.state.property.id}/table-reservations/${formData.id}`, formData)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: 'Reservation Updated Successfully!', life: 1000 });
                loader.hide();
                self.$router.push({name : 'reservations'});
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    deleteReservation(id) {
        let self = this.vm;
        let loader = self.$loading.show();
        self.$axios
        .delete(`properties/${self.$store.state.property.id}/table-reservations/${id}`)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: response.data.message, life: 1000 });
                loader.hide();
                self.tableReservationService.getReservations();
            })
            .catch(function (error) {
                loader.hide();
                if(error.response){
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    freeTable(table) {
        let self = this.vm;
        let loader = self.$loading.show();
        self.$axios
        .put(`properties/${self.$store.state.property.id}/free-table/${table}`)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: response.data.message, life: 1000 });
                loader.hide();
                self.confirmFreeTableDialog = false;
                self.tableReservationService.getCreateData();
            })
            .catch(function (error) {
                loader.hide();
                if(error.response){
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
}
