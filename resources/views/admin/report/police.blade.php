@extends('admin.layouts.master')
@section('title', 'Police Report')
@section('content')
    @php
        $routeParameters = \Route::current()->parameters;
        $propertyId = getProperty();
    @endphp
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-2 mb-1"><span class="text-muted fw-light">Reports /</span> Police Report </h4>
        <section>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="form" id="search_form">
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="fromDate">From Date</label>
                                            <input type="date" name="fromDate" id="fromDate" class="form-control"
                                                value="{{ request('fromDate') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="toDate">To Date</label>
                                            <input type="date" name="toDate" id="toDate" class="form-control"
                                                value="{{ request('toDate') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-4 text-end mt-4">
                                        <a href="{{ route('property.police-report', ['property' => $propertyId]) }}"
                                            type="button" class="btn btn-danger mt-1 mx-2">Reset</a>

                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-datatable table-responsive">
                            <table class="datatables table">
                                <thead class="border-top">
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Booking ID</th>
                                        <th>Room Name</th>
                                        <th>Customer Name</th>
                                        <th>Custommer Cnic</th>
                                        <th>From Date</th>
                                        <th>To Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            var propertyId = '{!! request()->route('property')->id !!}';
            dataTable = $('.datatables').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                searching: true,
                ajax: {
                    url: "{{ url('/properties') }}" + '/' + propertyId + '/police-report',
                    data: function(filter) {
                        filter.fromDate = $('#fromDate').val();
                        filter.toDate = $('#toDate').val();
                    }
                },
                columns: [
                    // columns according to JSON
                    // {
                    //     data: 'responsive_id'
                    // },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'id'
                    },
                    {
                        data: 'roomName',
                    },
                    {
                        data: 'customerName'
                    },
                    {
                        data: 'customercnic'
                    },
                    {
                        data: 'fromDateTime'
                    },
                    {
                        data: 'toDateTime'
                    },
                ],
                "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                }],
                order: [
                    [2, 'desc']
                ],
                dom: '<"row mx-2"' + '<"col-md-2"<"me-3"l>>' +
                    '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
                    '>t' + '<"row mx-2"' + '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
                language: {
                    sLengthMenu: '_MENU_',
                    search: '',
                    searchPlaceholder: 'Search..'
                },
                // Buttons with Dropdown
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-label-primary dropdown-toggle mx-3',
                    text: '<i class="ti ti-logout rotate-n90 me-2"></i>Export',
                    buttons: [{
                        extend: 'print',
                        title: 'police Report',
                        text: '<i class="ti ti-printer me-2" ></i>Print',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList !== undefined && item
                                            .classList.contains('user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        },
                        customize: function customize(win) {
                            //customize print view for dark
                            $(win.document.body).css('color', config.colors
                                    .headingColor)
                                .css('border-color', config.colors.borderColor).css(
                                    'background-color', config.colors.body);
                            $(win.document.body).find('table').addClass('compact').css(
                                    'color', 'inherit').css('border-color', 'inherit')
                                .css(
                                    'background-color', 'inherit');
                        }
                    }, {
                        extend: 'csv',
                        title: 'police Report',
                        text: '<i class="ti ti-file-text me-2" ></i>Csv',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'excel',
                        title: 'police Report',
                        text: '<i class="ti ti-file-spreadsheet me-2"></i>Excel',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'pdf',
                        title: 'police Report',
                        text: '<i class="ti ti-file-text me-2"></i>Pdf',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'copy',
                        title: 'police Report',
                        text: '<i class="ti ti-copy me-1" ></i>Copy',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be copy
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }]
                }, ],
                // For responsive popup
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function header(row) {
                                var data = row.data();
                                return 'Details of ' + data['name'];
                            }
                        }),
                    }
                }
            });
            // Filter Function
            $('#search_form').submit(function(e) {
                dataTable.draw();
                e.preventDefault();
            });
        });
    </script>
@endsection
