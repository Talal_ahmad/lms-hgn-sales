@extends('admin.layouts.master')
@section('title', 'Reservations')
@section('styles')
    <style>
        .select2-container {
            z-index: 100000 !important;
        }
        textarea {
            resize: none !important;
        }
        table.dataTable thead th:last-of-type {
            padding-right: 20px !important;
        }
        table.dataTable tbody td:last-of-type {
            padding-right: 18px !important;
        }
        /* .cus-ps-20 {
            padding-left: 20px !important;
        }
        .cus-pe-20 {
            padding-right: 20px !important;
        } */
    </style>
@endsection
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-2 mb-1"><span class="text-muted fw-light">Reservations /</span> List</h4>
        <div class="card">
            <div class="card-datatable table-responsive">
                <table class="datatables-users table">
                    <thead class="border-top">
                        <tr>
                            <th class="not_include"></th>
                            {{-- <th>Sr.#</th> --}}
                            <th class="text-start">Sr.#</th>
                            <th>Type</th>
                            <th>Date & Time</th>
                            <th>Table</th>
                            <th>Customer Name</th>
                            <th>Customer Email</th>
                            <th>Customer Phone</th>
                            {{-- <th>Actions</th> --}}
                            <th class="text-end">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <!-- Add New Modal -->
            <div class="modal fade" id="add_modal" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered1 modal-lg modal-simple modal-add-new-cc">
                    <div class="modal-content p-3 p-md-5">
                        <div class="modal-body">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            <div class="text-center mb-4">
                                <h3 class="mb-2">Add New Reservation</h3>
                            </div>
                            <form id="add_form" class="form row g-3">
                                @csrf
                                
                                @isset($propertyType)
                                    
                                    @if ($propertyType === "Hotel")

                                        <div class="col-6">
                                            <label class="form-label" for="room_id">Room</label>
                                            <select id="room_id" name="room_id" class="form-select select2"
                                                data-placeholder="Select Room" requiredd>
                                                <option value=""></option>
                                                @foreach ($rooms as $room)
                                                    <option value="{{$room->id}}">{{$room->name}}</option>  
                                                @endforeach
                                            </select>
                                        </div>

                                    @elseif ($propertyType === "Restaurant")

                                        <div class="col-6">
                                            <label class="form-label" for="table_id">Table</label>
                                            <select id="table_id" name="table_id" class="form-select select2"
                                                data-placeholder="Select Table" requiredd>
                                                <option value=""></option>
                                                @foreach ($tables as $table)
                                                    <option value="{{$table->id}}">{{$table->name}}</option>  
                                                @endforeach
                                            </select>
                                        </div>
                                    
                                    @endif

                                @endisset

                                <div class="col-6">
                                    <label class="form-label" for="name">Customer Name</label>
                                    <input type="text" class="form-control" id="name" name="name"
                                        placeholder="Enter Customer Name" requiredd />
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="name">From Date & Time</label>
                                    <input type="datetime-local" class="form-control" id="fromDateTime" name="fromDateTime"
                                        placeholder="Enter Customer Name" requiredd />
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="name">To Date & Time</label>
                                    <input type="datetime-local" class="form-control" id="toDateTime" name="toDateTime"
                                        placeholder="Enter Customer Name" requiredd />
                                </div>

                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                                    <button type="reset" class="btn btn-label-secondary btn-reset" data-bs-dismiss="modal"
                                        aria-label="Close">
                                        Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Add New Modal -->

            <!-- edit New Modal -->
            <div class="modal fade" id="edit_modal" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered1 modal-lg modal-simple modal-add-new-cc">
                    <div class="modal-content p-3 p-md-5">
                        <div class="modal-body">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            <div class="text-center mb-4">
                                <h3 class="mb-2">Edit Reservation</h3>
                            </div>
                            <form id="update_form" class="form row g-3">
                                @csrf
                                @method('PUT')

                                <div class="col-12">
                                    <label class="form-label" for="edit_type">Reservation Type</label>
                                    <select id="edit_type" name="type" class="form-select select2 select2-dropdown"
                                        data-placeholder="Select Table" required>
                                        <option value="runtime-booking">Runtime-booking</option>
                                        <option value="pre-booking">Pre-booking</option>
                                    </select>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="edit_customer_type">Customer Type</label>
                                    <select id="edit_customer_type" name="customer_type" class="form-select select2 select2-dropdown"
                                        data-placeholder="Select Table" required>
                                        <option value="walk-in">Walk In</option>
                                        <option value="staff">Staff</option>
                                        <option value="foc">FOC</option>
                                    </select>
                                </div>
                                {{-- <div class="col-6">
                                    <label class="form-label" for="edit_table_id">Table</label>
                                    <select id="edit_table_id" name="table_id" class="form-select select2 select2-dropdown"
                                        data-placeholder="Select Table" required>
                                        <option value=""></option>
                                    </select>
                                </div> --}}
                                <div class="col-6">
                                    <label class="form-label" for="name">Customer Name</label>
                                    <input type="text" class="form-control" id="edit_name" name="name"
                                        placeholder="Enter Customer Name" required />
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="edit_email">Email</label>
                                    <input type="email" class="form-control" id="edit_email" name="email"
                                        placeholder="Enter Email"/>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="edit_phone_number">Phone</label>
                                    <input type="tel" class="form-control" id="edit_phone_number" name="phone_number"
                                        placeholder="Enter Phone Number"/>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="edit_number_of_persons">Number of Persons</label>
                                    <input type="number" class="form-control" id="edit_number_of_persons" name="number_of_persons"
                                        placeholder="Enter Number of Persons" min="" required />
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="edit_fromDateTime">From Date & Time</label>
                                    <input type="datetime-local" class="form-control" id="edit_fromDateTime" name="fromDateTime"
                                        placeholder="Enter Customer Name" required />
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="edit_toDateTime">To Date & Time</label>
                                    <input type="datetime-local" class="form-control" id="edit_toDateTime" name="toDateTime"
                                        placeholder="Enter Customer Name" required />
                                </div>
                                <div class="col-12">
                                    <label class="form-label" for="edit_remarks">Remarks</label>
                                    <textarea class="form-control" id="edit_remarks" name="remarks"
                                              placeholder="Enter Remarks"></textarea>
                                </div>

                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-primary me-sm-3 me-1">Update</button>
                                    <button type="reset" class="btn btn-label-secondary btn-reset"
                                        data-bs-dismiss="modal" aria-label="Close">
                                        Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Add New Modal -->

        </div>
        <div class="sk-wave sk-primary" id="loader"
            style="display: none; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
        </div>
    </div>
@endsection
@section('scripts')

    <script>
        var propertyId = '{!! request()->route('property')->id !!}';
        $(document).ready(function() {

        // preventing users from entering value below 1
        $('#edit_number_of_persons').on('input', function() {
            if ($(this).val() < 1) {
                $(this).val(1);
            }
        });

        // converting DateTime in AM/PM format
        function formatDateTime(datetime) {
            // var originalTime = "2024-02-15 19:39:00";
            // Convert to Date object
            var dateObj = new Date(datetime);
            // Format year, month, day, hours, and minutes
            var year = dateObj.getFullYear();
            // Format month name
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var month = monthNames[dateObj.getMonth()];

            var day = dateObj.getDate();
            var hours = dateObj.getHours();
            var minutes = dateObj.getMinutes();
            // Determine AM or PM
            var ampm = hours >= 12 ? 'PM' : 'AM';
            // Convert hours to 12-hour format
            hours = hours % 12;
            hours = hours ? hours : 12; // The hour '0' should be '12' in 12-hour time format
            // Construct the new formatted date and time
            var formattedDateTime = day + ' ' + month + ' ' + year + ' ' + hours + ':' + minutes + ' ' + ampm;
            return formattedDateTime;
        }

        // for test
        // $('#add_modal').modal('show');
        // $("#edit_modal").modal('show');
            
            var url = "{{ route('reservations.index', ['property' => ':propertyId']) }}";
            url = url.replace(':propertyId', propertyId);
            dataTable = $('.datatables-users').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [
                    // columns according to JSON
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                        width: '5%',
                        "render": function(data, type, full, meta) {
                            return `<div class="text-start">${data}</div>`;
                        }
                    },
                    {
                        data: 'type',
                        width: '17%',
                        "render": function(data, type, full, meta) {
                            return `<div class="bg-primary bg-opacity-75 text-white text-center rounded-pill py-2">${data}</div>`;
                        }
                    },
                    {
                        data: 'start_date_time',
                        "render": function(data, type, full, meta) {
                            return formatDateTime(data);
                            // return formatDateTime(full.start_date_time);
                        }
                    },
                    {
                        data: 'table.floor.name',
                        "render": function(data, type, full, meta) {
                            // Assuming "full" is the row data, access the nested property
                            return full.table.floor.name + ' / ' + full.table.name;
                        }
                    },
                    {
                        data: 'customer.name'
                    },
                    {
                        data: 'customer.email'
                    },
                    {
                        data: 'customer.phone_number'
                    },
                    {
                        data: 'action'
                    }
                ],
                columnDefs: [{
                    // For Responsive
                    className: 'control',
                    searchable: false,
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0,
                    render: function render(data, type, full, meta) {
                        return '';
                    }
                },
                {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    width: '8%',
                    render: function render(data, type, full, meta) {
                        // return '<div class="d-inline-block text-nowrap">' +
                        return '<div class="d-flex justify-content-end text-nowrap">' +
                            '<a href="javascript:;" class="text-body" onclick=typeEdit(' + full
                            .id + ')>' + '<i class=\"ti ti-edit\"></i>' +
                            '</a>' +
                            '<a href="javascript:;" class="text-body ms-3" onclick=deleteType(' +
                            full.id + ')>' + '<i class=\"ti ti-trash\"></i>' +
                            '</a>' + '</div>';
                    }
                }],
                order: [
                    [2, 'desc']
                ],
                dom: '<"row mx-2"' + '<"col-md-2"<"me-3"l>>' +
                    '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
                    '>t' + '<"row mx-2"' + '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
                language: {
                    sLengthMenu: '_MENU_',
                    search: '',
                    searchPlaceholder: 'Search..'
                },
                // Buttons with Dropdown
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-label-primary dropdown-toggle mx-3',
                    text: '<i class="ti ti-logout rotate-n90 me-2"></i>Export',
                    buttons: [{
                        extend: 'print',
                        title: 'Users',
                        text: '<i class="ti ti-printer me-2" ></i>Print',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList !== undefined && item
                                            .classList.contains('user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        },
                        customize: function customize(win) {
                            //customize print view for dark
                            $(win.document.body).css('color', config.colors
                                    .headingColor)
                                .css('border-color', config.colors.borderColor).css(
                                    'background-color', config.colors.body);
                            $(win.document.body).find('table').addClass('compact').css(
                                    'color', 'inherit').css('border-color', 'inherit')
                                .css(
                                    'background-color', 'inherit');
                        }
                    }, {
                        extend: 'csv',
                        title: 'Users',
                        text: '<i class="ti ti-file-text me-2" ></i>Csv',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'excel',
                        title: 'Users',
                        text: '<i class="ti ti-file-spreadsheet me-2"></i>Excel',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'pdf',
                        title: 'Users',
                        text: '<i class="ti ti-file-text me-2"></i>Pdf',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'copy',
                        title: 'Users',
                        text: '<i class="ti ti-copy me-1" ></i>Copy',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be copy
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }]
                }, {
                    text: '<i class="ti ti-plus me-0 me-sm-1"></i><span class="d-none d-sm-inline-block">Add New</span>',
                    className: 'add-new btn btn-primary',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#add_modal',
                        'id': 'add-new-reservation'
                    }
                }],
                // For responsive popup
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function header(row) {
                                var data = row.data();
                                return 'Details of ' + data['name'];
                            }
                        }),
                        type: 'column',
                        renderer: function renderer(api, rowIdx, columns) {
                            var data = $.map(columns, function(col, i) {
                                return col.title !==
                                    '' // ? Do not show row in modal popup if title is blank (for check box)
                                    ?
                                    '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' +
                                    col
                                    .columnIndex + '">' + '<td>' + col.title + ':' + '</td> ' +
                                    '<td>' + col.data + '</td>' + '</tr>' : '';
                            }).join('');
                            return data ? $('<table class="table"/><tbody />').append(data) : false;
                        }
                    }
                }
            });

            $('.form-repeater').repeater({
                show: function() {
                    // var fromControl = $(this).find('.form-control, .form-select');
                    // var formLabel = $(this).find('.form-label');
                    $(this).slideDown();
                    $('.form-repeater').find('select').next('.select2-container').remove();
                    $('.form-repeater').find('select').select2({
                        placeholder: "Select Type"
                    });

                },
                hide: function(e) {
                    confirm('Are you sure you want to delete this element?') && $(this).slideUp(e);
                }
            });


            
            // get latest tables
            // $("#add-new-reservation").on("click", function(e) {
            //     // $('#loader').show();
            //     // e.preventDefault();
            //     var url = "{{ route('reservations.create', ['property' => ':propertyId']) }}";
            //     url = url.replace(':propertyId', propertyId);
            //     $.ajax({
            //         url: url,
            //         type: "get",
            //         // data: new FormData(this),
            //         processData: false,
            //         contentType: false,
            //         responsive: true,
            //         success: function success(response) {
            //             // sweetalert
            //             if (response.errors) {
            //                 $.each(response.errors, function(index, value) {
            //                     Swal.fire({
            //                         title: value,
            //                         icon: 'error',
            //                         customClass: {
            //                             confirmButton: 'btn btn-success'
            //                         }
            //                     });
            //                 });
            //             } else if (response.error_message) {
            //                 swal.fire({
            //                     icon: 'error',
            //                     title: 'An error has been occured! Please Contact Administrator.'
            //                 })
            //             } else {
            //                 // $('#add_form')[0].reset();
            //                 // dataTable.ajax.reload();
            //                 // $('#loader').hide();
            //                 // $('#add_modal').modal('hide');
            //                 // Swal.fire({
            //                 //     icon: 'success',
            //                 //     title: 'Menu Category has been Added Successfully!',
            //                 //     customClass: {
            //                 //         confirmButton: 'btn btn-success'
            //                 //     }
            //                 // });

            //                 // Assuming response is an array of objects with 'id' and 'name' properties
            //                 var select = $('#table_id');

            //                 // Clear existing options
            //                 select.empty();

            //                 // Add an empty option
            //                 select.append('<option value=""></option>');

            //                 // console.log(response.data[0]);

            //                 // Add options based on the response data
            //                 $.each(response.data, function(index, item) {
            //                     select.append('<option value="' + item.id + '">' + item.name + '</option>');
            //                 });

            //                 // Initialize select2 or update options if already initialized
            //                 // $('#table_id').trigger('change.select2');
            //                 // select.trigger('change.select2');
            //                 // $('#table_id').trigger('change');
            //                 $('#table_id').select2();

            //                 // $('#add_modal').modal('show');

            //             }
            //         },
            //     });
            // });
            
            $("#add_form").submit(function(e) {
                $('#loader').show();
                e.preventDefault();

                let formData = new FormData(this);

                //  $.each with FormData may not work as expected in all browsers, as FormData is not a regular JavaScript object
                formData.forEach(function (value, key) { 
                    
                    console.log(key + " : " + value);
                 });

                // console.log($("#fromDateTime").val());
                // return;
                
                var url = "{{ route('reservations.store', ['property' => ':propertyId']) }}";
                url = url.replace(':propertyId', propertyId);
                $.ajax({
                    url: url,
                    type: "POST",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function success(response) {
                        console.log(response);
                        // sweetalert
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Swal.fire({
                                    title: value,
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            });
                        } else if (response.error_message) {
                            swal.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            dataTable.ajax.reload();
                            $('#loader').hide();
                            $('#add_modal').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'Reservation has been Added Successfully!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        }
                    },
                    error: function(error) {
                        swal.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                        })
                        console.error('Error fetching data:', error);
                    }
                });
            });

            $("#update_form").submit(function(e) {
                $('#loader').show();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append("customer_id", $("#edit_modal").find($("#edit_name")).data("customer_id"));
                $.ajax({
                    url: "{{ url('properties') }}" + "/" + propertyId + "/reservations/" + rowid,
                    type: 'POST',
                    data: formData,
                    // data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function success(response) {
                        // sweetalert
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Swal.fire({
                                    title: value,
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            });
                        } else if (response.error_message) {
                            swal.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            dataTable.ajax.reload();
                            $('#loader').hide();
                            $('#edit_modal').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'Reservation has been Updated Successfully!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        }
                    },
                    error: function(error) {
                        swal.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                        })
                        console.error('Error fetching data:', error);
                    }
                });
            });
        });

        function typeEdit(id) {
            rowid = id;

            $.ajax({
                // url: "{{ url('properties') }}" + "/" + propertyId + "/reservations/" + rowid,    // route to show() method
                url: "{{ url('properties') }}" + "/" + propertyId + "/reservations/" + rowid + "/edit",
                type: "GET",
                success: function(response) {

                    let editModal =  $("#edit_modal");
                    
                    editModal.find($('#edit_type')).val(response.reservation.type).select2();
                    editModal.find($('#edit_customer_type')).val(response.reservation.customer.type).select2();
                    
                    editModal.find($("#edit_name")).val(response.reservation.customer.name).data('customer_id', response.reservation.customer.id);
                    
                    editModal.find($("#edit_email")).val(response.reservation.customer.email);
                    editModal.find($("#edit_phone_number")).val(response.reservation.customer.phone_number);
                    editModal.find($("#edit_number_of_persons")).val(response.reservation.number_of_persons);
                    
                    // set Time Fields without Seconds, to avoid Frontend Time Fields Validation Error
                    editModal.find($("#edit_fromDateTime")).val(response.reservation.start_date_time.substring(0, 16));
                    editModal.find($("#edit_toDateTime")).val(response.reservation.end_date_time.substring(0, 16));
                    // front-end provided date time and works without errors due to lack of seconds included
                    // editModal.find($("#edit_fromDateTime")).val("2024-02-02T12:57");

                    // show Edit Modal
                    $("#edit_modal").modal('show');
                }
            });
        }

        function deleteType(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            rowid = id;
                            $.ajax({
                                url: "{{ url('properties') }}" + "/" + propertyId + "/reservations/" + rowid,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        dataTable.ajax.reload();
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'Reservation has been Deleted Successfully!',
                                            customClass: {
                                                confirmButton: 'btn btn-success'
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
