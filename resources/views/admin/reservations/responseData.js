// All Reservations
{
  "data": [
      {
          "id": 1,
          "type": "runtime-booking",
          "customer_id": 1,
          "table_id": 3,
          "room_id": null,
          "number_of_persons": 1,
          "start_date_time": "2024-01-11 10:00:03",
          "end_date_time": "2024-01-11 10:30:03",
          "remarks": null,
          "created_at": "2024-01-11T10:00:16.000000Z",
          "updated_at": "2024-01-11T10:00:16.000000Z",
          "table": {
              "id": 3,
              "floor_id": 2,
              "name": "Table 1",
              "seating_capacity": 2,
              "status": "occupied",
              "created_at": "2024-01-09T04:43:28.000000Z",
              "updated_at": "2024-01-10T02:13:58.000000Z",
              "floor": {
                  "id": 2,
                  "property_id": 1,
                  "name": "Dining Room 1",
                  "created_at": "2024-01-09T04:43:28.000000Z",
                  "updated_at": "2024-01-09T04:43:28.000000Z"
              }
          },
          "customer": {
              "id": 1,
              "property_id": 1,
              "type": "walk-in",
              "name": "tariq",
              "email": null,
              "phone_number": null,
              "created_at": "2024-01-11T10:00:16.000000Z",
              "updated_at": "2024-01-11T10:00:16.000000Z"
          }
      },
      {
          "id": 2,
          "type": "runtime-booking",
          "customer_id": 2,
          "table_id": 3,
          "room_id": null,
          "number_of_persons": 1,
          "start_date_time": "2024-01-11 05:31:03",
          "end_date_time": "2024-01-11 06:01:03",
          "remarks": null,
          "created_at": "2024-01-11T10:01:38.000000Z",
          "updated_at": "2024-01-11T10:01:38.000000Z",
          "table": {
              "id": 3,
              "floor_id": 2,
              "name": "Table 1",
              "seating_capacity": 2,
              "status": "occupied",
              "created_at": "2024-01-09T04:43:28.000000Z",
              "updated_at": "2024-01-10T02:13:58.000000Z",
              "floor": {
                  "id": 2,
                  "property_id": 1,
                  "name": "Dining Room 1",
                  "created_at": "2024-01-09T04:43:28.000000Z",
                  "updated_at": "2024-01-09T04:43:28.000000Z"
              }
          },
          "customer": {
              "id": 2,
              "property_id": 1,
              "type": "walk-in",
              "name": "tareq",
              "email": null,
              "phone_number": null,
              "created_at": "2024-01-11T10:01:38.000000Z",
              "updated_at": "2024-01-11T10:01:38.000000Z"
          }
      },
      {
          "id": 3,
          "type": "runtime-booking",
          "customer_id": 3,
          "table_id": 3,
          "room_id": null,
          "number_of_persons": 1,
          "start_date_time": "2024-01-11 01:02:03",
          "end_date_time": "2024-01-11 01:32:03",
          "remarks": null,
          "created_at": "2024-01-11T10:10:28.000000Z",
          "updated_at": "2024-01-11T10:10:28.000000Z",
          "table": {
              "id": 3,
              "floor_id": 2,
              "name": "Table 1",
              "seating_capacity": 2,
              "status": "occupied",
              "created_at": "2024-01-09T04:43:28.000000Z",
              "updated_at": "2024-01-10T02:13:58.000000Z",
              "floor": {
                  "id": 2,
                  "property_id": 1,
                  "name": "Dining Room 1",
                  "created_at": "2024-01-09T04:43:28.000000Z",
                  "updated_at": "2024-01-09T04:43:28.000000Z"
              }
          },
          "customer": {
              "id": 3,
              "property_id": 1,
              "type": "walk-in",
              "name": "gtggt",
              "email": null,
              "phone_number": null,
              "created_at": "2024-01-11T10:10:28.000000Z",
              "updated_at": "2024-01-11T10:10:28.000000Z"
          }
      },
      {
          "id": 4,
          "type": "runtime-booking",
          "customer_id": 4,
          "table_id": 3,
          "room_id": null,
          "number_of_persons": 1,
          "start_date_time": "2024-01-10 20:33:03",
          "end_date_time": "2024-01-10 21:03:03",
          "remarks": null,
          "created_at": "2024-01-11T10:11:06.000000Z",
          "updated_at": "2024-01-11T10:11:06.000000Z",
          "table": {
              "id": 3,
              "floor_id": 2,
              "name": "Table 1",
              "seating_capacity": 2,
              "status": "occupied",
              "created_at": "2024-01-09T04:43:28.000000Z",
              "updated_at": "2024-01-10T02:13:58.000000Z",
              "floor": {
                  "id": 2,
                  "property_id": 1,
                  "name": "Dining Room 1",
                  "created_at": "2024-01-09T04:43:28.000000Z",
                  "updated_at": "2024-01-09T04:43:28.000000Z"
              }
          },
          "customer": {
              "id": 4,
              "property_id": 1,
              "type": "walk-in",
              "name": "vvvvvvv",
              "email": null,
              "phone_number": null,
              "created_at": "2024-01-11T10:11:06.000000Z",
              "updated_at": "2024-01-11T10:11:06.000000Z"
          }
      },
      {
          "id": 5,
          "type": "runtime-booking",
          "customer_id": 5,
          "table_id": 4,
          "room_id": null,
          "number_of_persons": 1,
          "start_date_time": "2024-01-11 11:02:08",
          "end_date_time": "2024-01-11 11:32:08",
          "remarks": null,
          "created_at": "2024-01-11T11:02:15.000000Z",
          "updated_at": "2024-01-11T11:02:15.000000Z",
          "table": {
              "id": 4,
              "floor_id": 2,
              "name": "Table 2",
              "seating_capacity": 3,
              "status": "available",
              "created_at": "2024-01-11T11:01:59.000000Z",
              "updated_at": "2024-01-11T11:01:59.000000Z",
              "floor": {
                  "id": 2,
                  "property_id": 1,
                  "name": "Dining Room 1",
                  "created_at": "2024-01-09T04:43:28.000000Z",
                  "updated_at": "2024-01-09T04:43:28.000000Z"
              }
          },
          "customer": {
              "id": 5,
              "property_id": 1,
              "type": "walk-in",
              "name": "gtgtg",
              "email": null,
              "phone_number": null,
              "created_at": "2024-01-11T11:02:15.000000Z",
              "updated_at": "2024-01-11T11:02:15.000000Z"
          }
      },
      {
          "id": 6,
          "type": "runtime-booking",
          "customer_id": 6,
          "table_id": 4,
          "room_id": null,
          "number_of_persons": 1,
          "start_date_time": "2024-01-11 06:33:08",
          "end_date_time": "2024-01-11 07:03:08",
          "remarks": null,
          "created_at": "2024-01-11T11:02:42.000000Z",
          "updated_at": "2024-01-11T11:02:42.000000Z",
          "table": {
              "id": 4,
              "floor_id": 2,
              "name": "Table 2",
              "seating_capacity": 3,
              "status": "available",
              "created_at": "2024-01-11T11:01:59.000000Z",
              "updated_at": "2024-01-11T11:01:59.000000Z",
              "floor": {
                  "id": 2,
                  "property_id": 1,
                  "name": "Dining Room 1",
                  "created_at": "2024-01-09T04:43:28.000000Z",
                  "updated_at": "2024-01-09T04:43:28.000000Z"
              }
          },
          "customer": {
              "id": 6,
              "property_id": 1,
              "type": "walk-in",
              "name": "bbbb",
              "email": null,
              "phone_number": null,
              "created_at": "2024-01-11T11:02:42.000000Z",
              "updated_at": "2024-01-11T11:02:42.000000Z"
          }
      },
      {
          "id": 7,
          "type": "runtime-booking",
          "customer_id": 7,
          "table_id": 4,
          "room_id": null,
          "number_of_persons": 1,
          "start_date_time": "2024-01-11 07:04:08",
          "end_date_time": "2024-01-11 07:34:08",
          "remarks": null,
          "created_at": "2024-01-11T11:56:12.000000Z",
          "updated_at": "2024-01-11T11:56:12.000000Z",
          "table": {
              "id": 4,
              "floor_id": 2,
              "name": "Table 2",
              "seating_capacity": 3,
              "status": "available",
              "created_at": "2024-01-11T11:01:59.000000Z",
              "updated_at": "2024-01-11T11:01:59.000000Z",
              "floor": {
                  "id": 2,
                  "property_id": 1,
                  "name": "Dining Room 1",
                  "created_at": "2024-01-09T04:43:28.000000Z",
                  "updated_at": "2024-01-09T04:43:28.000000Z"
              }
          },
          "customer": {
              "id": 7,
              "property_id": 1,
              "type": "walk-in",
              "name": "dcvva",
              "email": null,
              "phone_number": null,
              "created_at": "2024-01-11T11:56:12.000000Z",
              "updated_at": "2024-01-11T11:56:12.000000Z"
          }
      }
  ]
}
// Edit Reservation and All Tables
{
    "tables": [
        {
            "id": 1,
            "floor_id": 1,
            "name": "f wef  F  ",
            "seating_capacity": 2,
            "status": "available",
            "created_at": null,
            "updated_at": null,
            "reservations": [
                {
                    "id": 1,
                    "type": "runtime-booking",
                    "customer_id": 2,
                    "table_id": 1,
                    "room_id": 1,
                    "number_of_persons": 1,
                    "start_date_time": "2024-02-14 12:21:09",
                    "end_date_time": "2024-02-14 17:21:10",
                    "remarks": null,
                    "created_at": null,
                    "updated_at": null,
                    "customer": {
                        "id": 2,
                        "property_id": 1,
                        "type": "walk-in",
                        "name": "aaa",
                        "email": null,
                        "phone_number": null,
                        "created_at": null,
                        "updated_at": null
                    }
                },
                {
                    "id": 2,
                    "type": "pre-booking",
                    "customer_id": 1,
                    "table_id": 1,
                    "room_id": 1,
                    "number_of_persons": 10,
                    "start_date_time": "2024-02-14 12:22:22",
                    "end_date_time": "2024-02-14 21:22:22",
                    "remarks": null,
                    "created_at": null,
                    "updated_at": null,
                    "customer": {
                        "id": 1,
                        "property_id": 1,
                        "type": "walk-in",
                        "name": "sfaf ewf  ",
                        "email": null,
                        "phone_number": null,
                        "created_at": null,
                        "updated_at": null
                    }
                },
                {
                    "id": 5,
                    "type": "runtime-booking",
                    "customer_id": 3,
                    "table_id": 1,
                    "room_id": null,
                    "number_of_persons": 1,
                    "start_date_time": "2024-02-16 20:37:00",
                    "end_date_time": "2024-02-16 21:37:00",
                    "remarks": null,
                    "created_at": "2024-02-14T14:37:53.000000Z",
                    "updated_at": "2024-02-14T14:37:53.000000Z",
                    "customer": {
                        "id": 3,
                        "property_id": 1,
                        "type": "walk-in",
                        "name": "Lloyd Daugherty V",
                        "email": null,
                        "phone_number": null,
                        "created_at": "2024-02-14T14:37:53.000000Z",
                        "updated_at": "2024-02-14T14:37:53.000000Z"
                    }
                },
                {
                    "id": 6,
                    "type": "runtime-booking",
                    "customer_id": 4,
                    "table_id": 1,
                    "room_id": null,
                    "number_of_persons": 1,
                    "start_date_time": "2024-02-15 19:39:00",
                    "end_date_time": "2024-02-15 19:39:00",
                    "remarks": null,
                    "created_at": "2024-02-14T14:39:58.000000Z",
                    "updated_at": "2024-02-14T14:39:58.000000Z",
                    "customer": {
                        "id": 4,
                        "property_id": 1,
                        "type": "walk-in",
                        "name": "chevronAll.woff",
                        "email": null,
                        "phone_number": null,
                        "created_at": "2024-02-14T14:39:58.000000Z",
                        "updated_at": "2024-02-14T14:39:58.000000Z"
                    }
                },
                {
                    "id": 7,
                    "type": "runtime-booking",
                    "customer_id": 5,
                    "table_id": 1,
                    "room_id": null,
                    "number_of_persons": 1,
                    "start_date_time": "2024-02-01 10:37:00",
                    "end_date_time": "2024-02-03 10:37:00",
                    "remarks": null,
                    "created_at": "2024-02-15T05:37:21.000000Z",
                    "updated_at": "2024-02-15T05:37:21.000000Z",
                    "customer": {
                        "id": 5,
                        "property_id": 1,
                        "type": "walk-in",
                        "name": "chevronAll.woff",
                        "email": null,
                        "phone_number": null,
                        "created_at": "2024-02-15T05:37:21.000000Z",
                        "updated_at": "2024-02-15T05:37:21.000000Z"
                    }
                },
                {
                    "id": 8,
                    "type": "runtime-booking",
                    "customer_id": 6,
                    "table_id": 1,
                    "room_id": null,
                    "number_of_persons": 1,
                    "start_date_time": "2024-02-03 10:41:00",
                    "end_date_time": "2024-02-10 10:41:00",
                    "remarks": null,
                    "created_at": "2024-02-15T05:42:03.000000Z",
                    "updated_at": "2024-02-15T05:42:03.000000Z",
                    "customer": {
                        "id": 6,
                        "property_id": 1,
                        "type": "walk-in",
                        "name": "Lloyd Daugherty V",
                        "email": null,
                        "phone_number": null,
                        "created_at": "2024-02-15T05:42:03.000000Z",
                        "updated_at": "2024-02-15T05:42:03.000000Z"
                    }
                }
            ]
        }
    ],
    "reservation": {
        "id": 1,
        "type": "runtime-booking",
        "customer_id": 2,
        "table_id": 1,
        "room_id": 1,
        "number_of_persons": 1,
        "start_date_time": "2024-02-14 12:21:09",
        "end_date_time": "2024-02-14 17:21:10",
        "remarks": null,
        "created_at": null,
        "updated_at": null,
        "customer": {
            "id": 2,
            "property_id": 1,
            "type": "walk-in",
            "name": "aaa",
            "email": null,
            "phone_number": null,
            "created_at": null,
            "updated_at": null
        }
    }
}