// function updateSelectOptions(selectElement, responseData, currentValue) {
  //     selectElement.empty();
  //     selectElement.append('<option value=""></option>');

  //     // Add options based on the response data
  //     $.each(responseData, function(index, item) {
  //         selectElement.append('<option value="' + item.id + '">' + item.name + '</option>');
  //     });

  //     // set current values
  //     selectElement.val(currentValue).select2();
  // }

  {{-- code for looping through multiple select2s for fill their option elements
       using server provided data --}}
  // Assuming you have an array of select element IDs
  // const selectElementIds = ['edit_type', 'edit_customer_type', 'edit_table_id'];
  // const selectElementMappings = {
  //     'edit_type': 'type',
  //     'edit_customer_type': 'customer.type',
  //     'edit_table_id': 'table_id'
  // };

  // // Loop through the select elements
  // $.each(selectElementMappings, function(elementId, responseDataKey) {
  //     let selectElement = $('#' + elementId);
  //     let responseData = responseDataKey.split('.').reduce(function(obj, key) {
  //         return obj[key];
  //     }, response);
  //     let currentValue = response.reservation[responseDataKey];

  //     updateSelectOptions(selectElement, responseData, currentValue);
  // });
  // Loop through the select elements
  // selectElementIds.forEach(function(elementId) {
  //     let selectElement = $('#' + elementId);
  //     let responseData = response[elementId.replace('edit_', '')]; // Assuming your response data is structured accordingly
  //     let currentValue = response.reservation[elementId.replace('edit_', '')]; // Assuming your response data is structured accordingly

  //     updateSelectOptions(selectElement, responseData, currentValue);
  // });

  // edit_type = reservation.type
  // edit_customer_type = reservation.customer.type
  // edit_customer_type = reservation.table_id
  