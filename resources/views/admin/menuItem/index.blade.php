@extends('admin.layouts.master')
@section('title', 'Menu Items')
@section('styles')
    <style>
        .select2-container {
            z-index: 999999;
        }
    </style>
@endsection
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-2 mb-1"><span class="text-muted fw-light">Menu Items /</span> List</h4>
        <div class="card">
            <div class="card-datatable table-responsive">
                <table class="datatables-users table">
                    <thead class="border-top">
                        <tr>
                            <th class="not_include"></th>
                            <th>Sr.#</th>
                            <th>Name</th>
                            <th>Categorie Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <!-- Add New Modal -->
            <div class="modal fade" id="add_modal" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered1 modal-lg modal-simple modal-add-new-cc">
                    <div class="modal-content p-3 p-md-5">
                        <div class="modal-body">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            <div class="text-center mb-4">
                                <h3 class="mb-2">Add New Menu Item</h3>
                            </div>
                            <form id="add_form" class="form row g-3" enctype="multipart/form-data">
                                @csrf
                                <div class="col-6">
                                    <label class="form-label" for="name"> Name</label>
                                    <input type="text" class="form-control" id="name" name="name"
                                        placeholder="Enter Name" required />
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="property-type">Menu Category</label>
                                    <select id="property-type" name="menu_category_id" class="form-select select2"
                                        data-placeholder="Select Menu Category">
                                        <option value=""></option>
                                        @foreach ($Menu_Category as $Category)
                                            <option value="{{ $Category->id }}">{{ $Category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-12 mb-3">
                                    <label for="pic" class="form-label">Image</label>
                                    <input class="form-control" name="pic" type="file" accept="image/*" id="pic">
                                </div>
                                <div class="col-12">
                                    <div class="form-repeater">
                                        <div data-repeater-list="servings">
                                            <div data-repeater-item>
                                                <div class="row">
                                                    <div class="col-4 mb-0">
                                                        <label class="form-label" for="menu_serving_id">Menu Serving</label>
                                                        <select name="menu_serving_id" class="form-select select2"
                                                            data-placeholder="Menu Serving">
                                                            <option value=""></option>
                                                            @foreach ($MenuServing as $Serving)
                                                                <option value="{{ $Serving->id }}">{{ $Serving->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-4 mb-0">
                                                        <label class="form-label" for="menu_serving_quantity_id">Menu
                                                            Serving
                                                            Quality</label>
                                                        <select name="menu_serving_quantity_id" class="form-select select2"
                                                            data-placeholder="Menu Serving Quality">
                                                            <option value=""></option>
                                                            @foreach ($MenuServing_quality as $Serving_quality)
                                                                <option value="{{ $Serving_quality->id }}">
                                                                    {{ $Serving_quality->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-2 mb-0">
                                                        <label class="form-label" for="form-repeater-1-1">Price</label>
                                                        <input type="number" name="price" id="edit_form-repeater-1-1"
                                                            class="form-control" placeholder="Price" />
                                                    </div>
                                                    <div
                                                        class="col-lg-12 col-xl-2 col-2 d-flex align-items-center mb-0">
                                                        <button type="button" class="btn btn-label-danger mt-4"
                                                            data-repeater-delete>
                                                            <i class="ti ti-trash ti-xs me-1"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-0">
                                            <button type="button" class="btn btn-primary" data-repeater-create>
                                                <i class="ti ti-plus me-1"></i>
                                                <span class="align-middle">Add</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                                    <button type="reset" class="btn btn-label-secondary btn-reset" data-bs-dismiss="modal"
                                        aria-label="Close">
                                        Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Add New Modal -->

            <!-- edit New Modal -->
            <div class="modal fade" id="edit_modal" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered1 modal-lg modal-simple modal-add-new-cc">
                    <div class="modal-content p-3 p-md-5">
                        <div class="modal-body">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            <div class="text-center mb-4">
                                <h3 class="mb-2">Edit Menu Item</h3>
                            </div>
                            <form id="update_form" class="form row g-3" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="col-6">
                                    <label class="form-label" for="name"> Name</label>
                                    <input type="text" class="form-control" id="edit_name" name="name"
                                        placeholder="Enter Name" required />
                                </div>
                                <div class="col-6 property-type-container">
                                    <label class="form-label" for="edit_menu_category_id">MenuCategory</label>
                                    <select id="edit_menu_category_id" name="menu_category_id"
                                        class="form-select select2" data-placeholder="Select Menu Category">
                                        @foreach ($Menu_Category as $Category)
                                            <option value="{{ $Category->id }}">{{ $Category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-12 mb-3">
                                    <label for="edit_pic" class="form-label">Image</label>
                                    <input class="form-control" name="pic" type="file" accept="image/*" id="edit_File">
                                </div>
                                <div class="col-12">
                                    <div class="edit-form-repeater">
                                        <div data-repeater-list="servings" id="repeater-container">
                                            <div data-repeater-item>
                                                <div class="row">
                                                    <div class="col-4 mb-0">
                                                        <label class="form-label" for="edit_menu_Serving_id">Menu
                                                            Serving</label>
                                                        <select id="edit_menu_Serving_id" name="menu_serving_id"
                                                            class="form-select select2" data-placeholder="Menu Serving">
                                                            <option value=""></option>
                                                            @foreach ($MenuServing as $Serving)
                                                                <option value="{{ $Serving->id }}">{{ $Serving->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-4 mb-0">
                                                        <label class="form-label" for="edit_menu_serving_quantity_id">Menu
                                                            Serving
                                                            Quality</label>
                                                        <select id="edit_menu_serving_quantity_id"
                                                            name="menu_serving_quantity_id"
                                                            class="form-select select2"
                                                            data-placeholder="Menu Serving Quality">
                                                            <option value=""></option>
                                                            @foreach ($MenuServing_quality as $Serving_quality)
                                                                <option value="{{ $Serving_quality->id }}">
                                                                    {{ $Serving_quality->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-2 mb-0">
                                                        <label class="form-label" for="form-repeater-1-1">Price</label>
                                                        <input type="number" name="price"
                                                            id="edit_form-repeater-1-1" class="form-control"
                                                            placeholder="Price" />
                                                    </div>
                                                    <div
                                                        class="col-lg-12 col-xl-2 col-2 d-flex align-items-center mb-0">
                                                        <button type="button" class="btn btn-label-danger mt-4"
                                                            data-repeater-delete>
                                                            <i class="ti ti-trash ti-xs me-1"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-0">
                                            <button type="button" class="btn btn-primary" data-repeater-create>
                                                <i class="ti ti-plus me-1"></i>
                                                <span class="align-middle">Add</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-primary me-sm-3 me-1">Update</button>
                                    <button type="reset" class="btn btn-label-secondary btn-reset"
                                        data-bs-dismiss="modal" aria-label="Close">
                                        Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Add New Modal -->

        </div>
        <div class="sk-wave sk-primary" id="loader"
            style="display: none; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
        </div>
    </div>
@endsection
@section('scripts')

    <script>
        var propertyId = '{!! request()->route('property')->id !!}';
        $(document).ready(function() {
            var url = "{{ route('menu-items.index', ['property' => ':propertyId']) }}";
            url = url.replace(':propertyId', propertyId);
            dataTable = $('.datatables-users').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [
                    // columns according to JSON
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'name',
                        name: 'menu_items.name'
                    },
                    {
                        data: 'categorie_name',
                        name : 'menu_categories.name'
                    },
                    {
                        data: 'action'
                    }
                ],
                columnDefs: [{
                    // For Responsive
                    className: 'control',
                    searchable: false,
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0,
                    render: function render(data, type, full, meta) {
                        return '';
                    }
                }, {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function render(data, type, full, meta) {
                        return '<div class="d-inline-block text-nowrap">' +
                            '<a href="javascript:;" class="text-body" onclick=itemEdit(' + full
                            .id + ')>' + '<i class=\"ti ti-edit\"></i>' +
                            '</a>' +
                            '<a href="javascript:;" class="text-body" onclick=deleteItem(' +
                            full.id + ')>' + '<i class=\"ti ti-trash\"></i>' +
                            '</a>' + '</div>';
                    }
                }],
                order: [
                    [2, 'desc']
                ],
                dom: '<"row mx-2"' + '<"col-md-2"<"me-3"l>>' +
                    '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
                    '>t' + '<"row mx-2"' + '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
                language: {
                    sLengthMenu: '_MENU_',
                    search: '',
                    searchPlaceholder: 'Search..'
                },
                // Buttons with Dropdown
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-label-primary dropdown-toggle mx-3',
                    text: '<i class="ti ti-logout rotate-n90 me-2"></i>Export',
                    buttons: [{
                        extend: 'print',
                        title: 'Users',
                        text: '<i class="ti ti-printer me-2" ></i>Print',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList !== undefined && item
                                            .classList.contains('user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        },
                        customize: function customize(win) {
                            //customize print view for dark
                            $(win.document.body).css('color', config.colors
                                    .headingColor)
                                .css('border-color', config.colors.borderColor).css(
                                    'background-color', config.colors.body);
                            $(win.document.body).find('table').addClass('compact').css(
                                    'color', 'inherit').css('border-color', 'inherit')
                                .css(
                                    'background-color', 'inherit');
                        }
                    }, {
                        extend: 'csv',
                        title: 'Users',
                        text: '<i class="ti ti-file-text me-2" ></i>Csv',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'excel',
                        title: 'Users',
                        text: '<i class="ti ti-file-spreadsheet me-2"></i>Excel',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'pdf',
                        title: 'Users',
                        text: '<i class="ti ti-file-text me-2"></i>Pdf',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'copy',
                        title: 'Users',
                        text: '<i class="ti ti-copy me-1" ></i>Copy',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be copy
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }]
                }, {
                    text: '<i class="ti ti-plus me-0 me-sm-1"></i><span class="d-none d-sm-inline-block">Add New</span>',
                    className: 'add-new btn btn-primary',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#add_modal'
                    }
                }],
                // For responsive popup
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function header(row) {
                                var data = row.data();
                                return 'Details of ' + data['name'];
                            }
                        }),
                        type: 'column',
                        renderer: function renderer(api, rowIdx, columns) {
                            var data = $.map(columns, function(col, i) {
                                return col.title !==
                                    '' // ? Do not show row in modal popup if title is blank (for check box)
                                    ?
                                    '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' +
                                    col
                                    .columnIndex + '">' + '<td>' + col.title + ':' + '</td> ' +
                                    '<td>' + col.data + '</td>' + '</tr>' : '';
                            }).join('');
                            return data ? $('<table class="table"/><tbody />').append(data) : false;
                        }
                    }
                }
            });
            $('.form-repeater').repeater({
                show: function() {
                    // var fromControl = $(this).find('.form-control, .form-select');
                    // var formLabel = $(this).find('.form-label');
                    $(this).slideDown();
                    $('.form-repeater').find('select').next('.select2-container').remove();
                    $('.form-repeater').find('select').select2({
                        placeholder: "Select Type"
                    });

                },
                hide: function(e) {
                    confirm('Are you sure you want to delete this element?') && $(this).slideUp(e);
                }
            });
            $('.edit-form-repeater').repeater({
                show: function() {
                    $(this).slideDown();
                    $('.edit-form-repeater').find('select').next('.select2-container').remove();
                    $('.edit-form-repeater').find('select').select2({
                        placeholder: "Select Type"
                    });

                },
                hide: function(e) {
                    confirm('Are you sure you want to delete this element?') && $(this).slideUp(e);
                }
            });

            $("#add_form").submit(function(e) {
                $('#loader').show();
                e.preventDefault();
                $.ajax({
                    url: "{{ route('menu-items.store', ['property' => 'property_id']) }}",
                    type: "POST",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function success(response) {
                        // sweetalert
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Swal.fire({
                                    title: value,
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            });
                        } else if (response.error_message) {
                            swal.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            dataTable.ajax.reload();
                            $('#loader').hide();
                            $('#add_modal').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'Menu Item has been Added Successfully!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        }
                    },
                });
            });

            $("#update_form").submit(function(e) {
                $('#loader').show();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('property_id', propertyId);
                $.ajax({
                    url: "{{ url('properties') }}" + "/" + propertyId + "/menu-items/" +
                        rowid,
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function success(response) {
                        // sweetalert
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Swal.fire({
                                    title: value,
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            });
                        } else if (response.error_message) {
                            Swal.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            dataTable.ajax.reload();
                            $('#loader').hide();
                            $('#edit_modal').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'Menu Item has been Updated Successfully!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        }
                    },
                });
            });
        });

        function itemEdit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('properties') }}" + "/" + propertyId + "/menu-items/" + rowid + "/edit",
                type: "GET",
                success: function(response) {
                    // Set values for non-repeater fields
                    $('#edit_name').val(response.menuItems.name);
                    $('#edit_menu_category_id').val(response.menuItems.menu_category_id).select2();

                    // Clear existing repeater items
                    $('#repeater-container').empty();

                    var newItemHtml = '';
                    // Append new repeater items based on the data
                    response.menuItemDetails.forEach(function(detail) {
                        newItemHtml += `
                            <div data-repeater-item>
                                <div class="row">
                                    <div class="col-4 mb-0">
                                        <label class="form-label" for="edit_menu_Serving_id">Menu Serving</label>
                                        <select id="edit_menu_Serving_id" name="menu_serving_id" class="form-select select2" data-placeholder="Menu Serving">
                                            <option value=""></option>
                                            @foreach ($MenuServing as $Serving)
                                                <option value="{{ $Serving->id }}">{{ $Serving->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-4 mb-0">
                                        <label class="form-label" for="edit_menu_serving_quantity_id">Menu Serving Quality</label>
                                        <select id="edit_menu_serving_quantity_id" name="menu_serving_quantity_id" class="form-select select2" data-placeholder="Menu Serving Quality">
                                            <option value=""></option>
                                            @foreach ($MenuServing_quality as $Serving_quality)
                                                <option value="{{ $Serving_quality->id }}">{{ $Serving_quality->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-2 mb-0">
                                        <label class="form-label" for="edit_form-repeater-1-1">Price</label>
                                        <input type="number" name="price" id="edit_form-repeater-1-1" class="form-control" placeholder="Price" />
                                    </div>
                                    <div class="mb-3 col-lg-12 col-xl-2 col-2 d-flex align-items-center mb-0">
                                        <button type="button" class="btn btn-label-danger mt-4" data-repeater-delete>
                                            <i class="ti ti-trash ti-xs me-1"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        `;
                    

                        // Append the new item to the repeater container
                        $('#repeater-container').append(newItemHtml);

                        // Set values for the new item
                        var newItem = $('#repeater-container').children().last();
                        newItem.find('#edit_menu_Serving_id').val(detail.menu_serving_id).select2();
                        newItem.find('#edit_menu_serving_quantity_id').val(detail
                            .menu_serving_quantity_id).select2();
                        newItem.find('#edit_form-repeater-1-1').val(detail.price);
                    });

                    // Show the modal
                    $('#edit_modal').modal('show');
                }
            });
        }

        function deleteItem(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "menu-items/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        dataTable.ajax.reload();
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'Menu Item has been Deleted Successfully!',
                                            customClass: {
                                                confirmButton: 'btn btn-success'
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
