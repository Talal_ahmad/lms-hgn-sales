@extends('admin.layouts.master')
@section('title', 'Floors')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-2 mb-1"><span class="text-muted fw-light">Floors /</span> List</h4>
        <div class="card">
            <div class="card-datatable table-responsive">
                <table class="datatables-users table">
                    <thead class="border-top">
                        <tr>
                            <th class="not_include"></th>
                            <th>Sr.#</th>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            {{-- Add Modal start --}}
            <div class="modal fade" id="add_modal" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered1 modal-simple modal-lg">
                    <div class="modal-content p-3 p-md-5">
                        <div class="modal-body">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            <div class="text-center mb-4">
                                <h3 class="mb-2">Add Floors</h3>
                            </div>
                            <form id="add_form" class="form row g-3" enctype="multipart/form-data">
                                @csrf
                                
                                <div class="col-12">
                                    
                                    {{-- "outter (floors)" repeater container --}}
                                    <div class="floors-form-repeater">
                                        
                                        <div class="mb-0 mt-2 pb-3 row justify-content-end">
                                            <button type="button" class="btn btn-primary w-auto" data-repeater-create>
                                                <i class="ti ti-plus me-1"></i>
                                                <span class="align-middle">Add Floor</span>
                                            </button>
                                        </div>
                                        
                                        {{-- "floors" repeater (list) --}}
                                        <div data-repeater-list="floors">

                                            {{-- "floors" repeater (item) --}}
                                            <div data-repeater-item>
                                                <div class="row justify-content-between">
                                                    <div class="mb-3 col-6 p-0">
                                                        <label class="form-label" for="floor-name">Floor Name</label>
                                                        <input type="text" id="floor-name" class="form-control"
                                                                name="name" value=""
                                                            placeholder="Enter Floor Name" />
                                                    </div>

                                                    <div class="mb-3 col-lg-2 col-xl-2 col-12 p-0">
                                                        <button type="button" class="btn btn-label-danger mt-4 col-12" title="Remove Floor" data-repeater-delete>
                                                            <i class="ti ti-trash ti-xs me-1"></i>
                                                        </button>
                                                    </div>
                                                </div>

                                                @isset($propertyType)
                                                    
                                                    {{-- "inner (rooms / tables)" repeater container --}}
                                                    <div class="inner-form-repeater">

                                                        @if ($propertyType === "Hotel")
                                                            {{-- "rooms" repeater (list) --}}
                                                            <div data-repeater-list="rooms">
                                                                
                                                                {{-- "rooms" repeater (item) --}}
                                                                <div data-repeater-item>
                                                                    <div class="row justify-content-between border rounded-3 mb-2 px-1 pt-2">
                                                                        <div class="mb-3 col-6">
                                                                            <label class="form-label">Room Type</label>
                                                                            <select name="room_type_id" class="form-select select2"
                                                                                    data-placeholder="Select Room Type" required>

                                                                                <option value=""></option>
                                                                                @foreach ($roomTypes as $roomType)
                                                                                    <option value="{{$roomType->id}}">{{$roomType->name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                        <div class="mb-3 col-6">
                                                                            <label class="form-label">Room Name</label>
                                                                            <input type="text" name="name" class="form-control" value=""
                                                                                placeholder="Enter Room Name" />
                                                                        </div>
                                                                        <div class="mb-3 col-6">
                                                                            <label class="form-label">Room Capacity</label>
                                                                            <input type="number" name="capacity" class="form-control" value=""
                                                                                placeholder="Enter Room Capacity" min="1" max="127" maxlength="3"/>
                                                                        </div>
                                                                        <div class="mb-3 col-6">
                                                                            <label class="form-label">Room Price</label>
                                                                            <input type="number" name="price" class="form-control" value=""
                                                                                placeholder="Enter Room Price" />
                                                                        </div>

                                                                        <div class="mb-3 col-10">
                                                                            <label class="form-label">Room Cover Image</label>
                                                                            <input type="file" name="cover_image" class="form-control"
                                                                                placeholder="Select Room Cover Image" accept=".jpg, .jpeg, .png, .bmp, .gif, .svg, .webp" />
                                                                        </div>

                                                                        <div class="mb-3 col-10">
                                                                            <label class="form-label">Room Other Images</label>
                                                                            <input type="file" name="other_images[]" class="form-control"
                                                                                placeholder="Select Room Other Images" accept=".jpg, .jpeg, .png, .bmp, .gif, .svg, .webp" multiple />
                                                                        </div>

                                                                        <div class="mb-3 col-lg-2 col-xl-2 col-12 ps-0">
                                                                            <button type="button" class="btn btn-label-danger mt-4 col-12" title="Remove Room" data-repeater-delete>
                                                                                <i class="ti ti-trash ti-xs me-1"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>

                                                            <div class="mb-0">
                                                                <button type="button" class="btn btn-primary" data-repeater-create>
                                                                    <i class="ti ti-plus me-1"></i>
                                                                    <span class="align-middle">Add Room</span>
                                                                </button>
                                                            </div>

                                                        @elseif ($propertyType === "Restaurant")
                                                            {{-- "tables" repeater (list) --}}
                                                            <div data-repeater-list="tables">
                                                            
                                                                {{-- "tables" repeater (item) --}}
                                                                <div data-repeater-item>
                                                                    <div class="row justify-content-between">
                                                                        <div class="mb-3 col-6">
                                                                            <label class="form-label" for="table-name">Table Name</label>
                                                                            <input type="text" id="table-name" class="form-control"
                                                                                    name="name"
                                                                                placeholder="Enter Table Name" />
                                                                        </div>
                                                                        <div class="mb-3 col-4">
                                                                            <label class="form-label" for="seating-capacity">Seating Capacity</label>
                                                                            <input type="number" id="seating-capacity" class="form-control"
                                                                                    name="seating_capacity"
                                                                                placeholder="Enter Table Capacity" />
                                                                        </div>
                                                                        <div class="mb-3 col-lg-2 col-xl-2 col-12">
                                                                            <button type="button" class="btn btn-label-danger mt-4 col-12" title="Remove Table" data-repeater-delete>
                                                                                <i class="ti ti-trash ti-xs me-1"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            
                                                            <div class="mb-0">
                                                                <button type="button" class="btn btn-primary" data-repeater-create>
                                                                    <i class="ti ti-plus me-1"></i>
                                                                    <span class="align-middle">Add Table</span>
                                                                </button>
                                                            </div>

                                                        @endif

                                                    </div>

                                                @else
                                                    <div>Server Error</div>
                                                @endisset
                                                
                                                <hr class="border-4 rounded-pill border-primary opacity-75" />

                                            </div>
                                            
                                        </div>

                                    </div>
                                </div>
                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                                    <button type="reset" class="btn btn-label-secondary btn-reset" data-bs-dismiss="modal"
                                        aria-label="Close">
                                        Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Add Modal end --}}

        </div>
        <div class="sk-wave sk-primary" id="loader"
            style="display: none; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var propertyId = '{!! request()->route('property')->id !!}';
        $(document).ready(function() {

            // for test
            // $('#add_modal').modal('show');

            // preventing users from entering value below 1
            $('input[type="number"]').on('input', function() {
                if ($(this).val() < 1) {
                    $(this).val(1);
                }
            });
            // when "room_type_id" is 1 (i.e., "Single") then make the Room Capacity = 1 and readonly
            $('select[name="room_type_id"]').on("change", function() {
                if ($(this).val() == 1) {
                    
                    $(this).closest('[data-repeater-item]').find('input[name="capacity"]').val(1).prop('readonly', true);
                }
                else {
                    $(this).closest('[data-repeater-item]').find('input[name="capacity"]').prop('readonly', false);
                } 
            });


            
            // Nested Repeaters Configuration for "add_form"
            $('.floors-form-repeater').repeater({

                repeaters: [{
                    // (Required)
                    // Specify the jQuery selector for this nested repeater
                    // for "rooms / tables" inner-form-repeater
                    selector: '.inner-form-repeater',
                    show: function() {
                        var fromControl = $(this).find('.form-control, .form-select');
                        var formLabel = $(this).find('.form-label');

                        $(this).slideDown();
                        $(this).find('select').next('.select2-container').remove();
                        // $(this).find('select').select2({
                        //     placeholder: "Select Type"
                        // });

                        $('[data-repeater-list="rooms"]').find('.select2').select2();
                        $('[data-repeater-list="tables"]').find('.select2').select2();

                        // preventing users from entering value below 1 (in newly added repeater-item)
                        $('input[type="number"]').on('input', function() {
                            if ($(this).val() < 1) {
                                $(this).val(1);
                            }
                        });
                    },
                    hide: function(e) {
                        confirm('Are you sure you want to delete this element?') && $(this).slideUp(e);
                    }
                }],
                show: function() {
                    var fromControl = $(this).find('.form-control, .form-select');
                    var formLabel = $(this).find('.form-label');

                    $(this).slideDown();
                    $(this).find('select').next('.select2-container').remove();
                    // $(this).find('select').select2({
                    //     placeholder: "Select Type"
                    // });

                    $('[data-repeater-list="rooms"]').find('.select2').select2();
                    $('[data-repeater-list="tables"]').find('.select2').select2();

                    // $($this).find('.select2').select2();

                    // $('.inner-form-repeater').find('select').next('.select2-container').remove();
                    // $('.inner-form-repeater').find('select').select2({
                    //     placeholder: "Select Type"
                    // });
                    // $('inner-form-repeater').find('select').next('.select2-container').remove();
                    // $('.inner-form-repeater').find('select').select2();
                },
                hide: function(e) {
                    confirm('Are you sure you want to delete this element?') && $(this).slideUp(e);
                }
            });
            // Repeater Configuration for "update_form"
            $('.edit-form-repeater').repeater({

                // repeaters: [{
                //     // (Required)
                //     // Specify the jQuery selector for this nested repeater
                //     selector: '.edit-inner-form-repeater',
                //     show: function() {
                //         var fromControl = $(this).find('.form-control, .form-select');
                //         var formLabel = $(this).find('.form-label');

                //         $(this).slideDown();
                //         $(this).find('select').next('.select2-container').remove();
                //         $(this).find('select').select2({
                //             placeholder: "Select Type"
                //         });

                //         // $('[data-repeater-list="rooms"]').find('.select2').select2();
                //     },
                //     hide: function(e) {
                //         confirm('Are you sure you want to delete this element?') && $(this).slideUp(e);
                //     }
                // }],

                show: function() {
                    var fromControl = $(this).find('.form-control, .form-select');
                    var formLabel = $(this).find('.form-label');

                    $(this).slideDown();
                    $(this).find('select').next('.select2-container').remove();
                    // $(this).find('select').select2({
                    //     placeholder: "Select Type"
                    // });

                    $('[data-repeater-list="rooms"]').find('.select2').select2();
                    $('[data-repeater-list="tables"]').find('.select2').select2();
                },
                hide: function(e) {
                    // confirm('Are you sure you want to delete this element?') && $(this).slideUp(e);
                }
            });

            // var propertyId = '{!! request()->route('property')->id !!}';
            var url = "{{ route('floors.index', ['property' => ':propertyId']) }}";
            url = url.replace(':propertyId', propertyId);
            dataTable = $('.datatables-users').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [
                    // columns according to JSON
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'action'
                    }
                ],
                columnDefs: [{
                    // For Responsive
                    className: 'control',
                    searchable: false,
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0,
                    render: function render(data, type, full, meta) {
                        return '';
                    }
                }, {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function render(data, type, full, meta) {
                        return '<div class="d-inline-block text-nowrap">' +
                            `<a href="{{ url('properties') }}/${propertyId}/floors/${full.id}/edit" class="text-body">`
                             + '<i class=\"ti ti-edit\"></i>' +
                            '</a>' +
                            '<a href="javascript:;" class="text-body" onclick=deleteProperty(' +
                            full.id + ')>' + '<i class=\"ti ti-trash\"></i>' +
                            '</a>' + '</div>';

                        // return '<div class="d-inline-block text-nowrap">' +
                        //     '<a href="javascript:;" class="text-body" onclick=propertyEdit(' +
                        //     full
                        //     .id + ')>' + '<i class=\"ti ti-edit\"></i>' +
                        //     '</a>' +
                        //     '<a href="javascript:;" class="text-body" onclick=deleteProperty(' +
                        //     full.id + ')>' + '<i class=\"ti ti-trash\"></i>' +
                        //     '</a>' + '</div>';
                    }
                }],
                order: [
                    [2, 'desc']
                ],
                dom: '<"row mx-2"' + '<"col-md-2"<"me-3"l>>' +
                    '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
                    '>t' + '<"row mx-2"' + '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
                language: {
                    sLengthMenu: '_MENU_',
                    search: '',
                    searchPlaceholder: 'Search..'
                },
                // Buttons with Dropdown
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-label-primary dropdown-toggle mx-3',
                    text: '<i class="ti ti-logout rotate-n90 me-2"></i>Export',
                    buttons: [{
                        extend: 'print',
                        title: 'Users',
                        text: '<i class="ti ti-printer me-2" ></i>Print',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList !== undefined && item
                                            .classList.contains('user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        },
                        customize: function customize(win) {
                            //customize print view for dark
                            $(win.document.body).css('color', config.colors
                                    .headingColor)
                                .css('border-color', config.colors.borderColor).css(
                                    'background-color', config.colors.body);
                            $(win.document.body).find('table').addClass('compact').css(
                                    'color', 'inherit').css('border-color', 'inherit')
                                .css(
                                    'background-color', 'inherit');
                        }
                    }, {
                        extend: 'csv',
                        title: 'Users',
                        text: '<i class="ti ti-file-text me-2" ></i>Csv',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'excel',
                        title: 'Users',
                        text: '<i class="ti ti-file-spreadsheet me-2"></i>Excel',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'pdf',
                        title: 'Users',
                        text: '<i class="ti ti-file-text me-2"></i>Pdf',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'copy',
                        title: 'Users',
                        text: '<i class="ti ti-copy me-1" ></i>Copy',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be copy
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }]
                }, {
                    text: '<i class="ti ti-plus me-0 me-sm-1"></i><span class="d-none d-sm-inline-block">Add New</span>',
                    className: 'add-new btn btn-primary',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#add_modal'
                    }
                }],
                // For responsive popup
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function header(row) {
                                var data = row.data();
                                return 'Details of ' + data['name'];
                            }
                        }),
                        type: 'column',
                        renderer: function renderer(api, rowIdx, columns) {
                            var data = $.map(columns, function(col, i) {
                                return col.title !==
                                    '' // ? Do not show row in modal popup if title is blank (for check box)
                                    ?
                                    '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' +
                                    col
                                    .columnIndex + '">' + '<td>' + col.title + ':' + '</td> ' +
                                    '<td>' + col.data + '</td>' + '</tr>' : '';
                            }).join('');
                            return data ? $('<table class="table"/><tbody />').append(data) : false;
                        }
                    }
                }
            });

            $("#add_form").submit(function(e) {
                $('#loader').show();
                e.preventDefault();
                var url = "{{ route('floors.store', ['property' => ':propertyId']) }}";
                url = url.replace(':propertyId', propertyId);

                let formData = new FormData(this);
                // this way, "null" string is recieved in laravel
                // formData.append("floors[0][tables][0][name]", null);
                // formData.append("floors[0][tables][0][seating_capacity]", null);
                
                // this way, null is recieved in laravel
                // formData.append("floors[0][tables][0][name]", "");
                // formData.append("floors[0][tables][0][seating_capacity]", "");
                
                // formData.append("floors[0][tables][]", " ");
                // formData.append("floors[0][tables][]", " ");
                // formData.append("floors[0][tables][0]", [{"name": ""}]);    // Error: create(): Argument #1 ($attributes) must be of type array, string given
                
                // formData.append("floors[0][tables][]", { name: '' });

                // formData.append("floors[0][tables]", "[]");

                // test: make name = null -> (laravel adds the record with name = "null")
                // test: make seating_capacity = "" (empty string) -> ( Integrity constraint violation: 1048 Column 'seating_capacity' cannot be null)
                // formData.append("floors[0][tables][0][name]", null);
                // formData.append("floors[0][tables][0][seating_capacity]", "");
                
                // formData.append("floors[0][tables][0]", null);

                // console.log([...formData]);  // Log the FormData to the console for debugging

                // this.formData.floors.push({ name: '', rooms: [], tables: [] });
                // formData[3].floors.push({ name: '', rooms: [], tables: [] });

                $.ajax({
                    url: url,
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function success(response) {
                        // sweetalert
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Swal.fire({
                                    title: value,
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            });
                        } else if (response.error_message) {
                            swal.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            dataTable.ajax.reload();
                            $('#loader').hide();
                            $('#add_modal').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'Floor has been Added Successfully!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        }
                    },
                    error:  function(jqXHR, textStatus, errorThrown) {
                        // console.log(jqXHR.responseJSON);
                        // console.log(jqXHR.responseJSON.message);
                        $.each(jqXHR.responseJSON.errors, function(index, value) {
                            Swal.fire({
                                title: value,
                                icon: 'error',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        });
                    } 
                });
            });

            $("#update_form").submit(function(e) {
                $('#loader').show();
                e.preventDefault();

                // problem:
                // during update, if you do not add or delete a repeater-item, array will not be sent, instead direct values will be sent
                
                // to do:
                // when update button is clicked, 
                // 1) click the 'add room / table' button
                // 2) click the 'delete' button of last added (empty) repeater item

                // $('.edit-form-repeater').find('button[data-repeater-create]').click();
                // $('.edit-form-repeater').find('div[data-repeater-list]').children().last().find("button[data-repeater-delete]").click();

                // console.log($('.edit-form-repeater').find('div[data-repeater-list]').children().last().find("button[data-repeater-delete]").text());
                // console.log($('.edit-form-repeater').find('button[data-repeater-create]').parent().prev().children().last().text());

                // disable alert when delete button is triggered
                window.alert = function ( text ) { console.log( 'tried to alert: ' + text ); return true; };

                   // Repeater Configuration for "update_form"
            // $('.edit-form-repeater').repeater({

            //     show: function() {
            //         var fromControl = $(this).find('.form-control, .form-select');
            //         var formLabel = $(this).find('.form-label');

            //         $(this).slideDown();
            //         $(this).find('select').next('.select2-container').remove();
            //         // $(this).find('select').select2({
            //         //     placeholder: "Select Type"
            //         // });

            //         $('[data-repeater-list="rooms"]').find('.select2').select2();
            //         $('[data-repeater-list="tables"]').find('.select2').select2();

            //     },
            //     hide: function(e) {
            //     }
            // });

                // return;



                // var formData = new FormData(this);  

                // console.log(formData.has("rooms[0][room_type_id]"));                
                // // return;
                
                // if (!formData.has("rooms[0][room_type_id]")) {
                    
                //     console.log(!formData.has("rooms[0][room_type_id]"));                
                //     console.log("no new item is added");
                    
                //     console.log("formdata does not contain rooms array, single repeater-item");
                //     // return;

                //     // for one repeater-item, for multiple, either loop through 
                //     // formData.append("rooms[0][room_type_id]", formData.get("room_type_id"));
                //     // formData.append("rooms[0][name]", formData.get("name"));
                    
                //     console.log(...formData);
                    
                //     console.log(formData.getAll("name"));
                
                //     // get the last "name" value, because the get() method returns the 1st one, which is "name" value of "floor" but we need for "room / table"
                //     var namesArray = formData.getAll("name");
                //     console.log(namesArray[namesArray.length - 1]);                
                //     // console.log(formData.get("rooms[0][room_type_id]"));                
                //     console.log(!formData.has("rooms[0][room_type_id]"));                
                    

                    
                //     // var obj = Object.fromEntries(formData);
                //     // var formDataArray = Array.from(formData);
                //     // console.log(obj);
                //     // console.log(obj.name);
                // }

                // return;
                // console.log(formData.get("name"));                
                // console.log(formData.get("room_type_id"));                
                // formData.rooms = [
                //     {
                //         room_type_id: formData.room_type_id,
                //         name: formData.name
                //     }
                // ];

                // formData.rooms = []; // Ensure rooms is initialized as an array

                // var newRoom = {
                //     room_type_id: formData.room_type_id,
                //     name: formData.name
                // };

                // formData.rooms.push(newRoom);
                
                $.ajax({
                    url: "{{ url('properties') }}" + "/" + propertyId + "/floors/" + rowid,
                    type: 'POST',
                    data: new FormData(this),
                    // data: formData,
                    contentType: false,
                    processData: false,
                    success: function success(response) {
                        // sweetalert
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Swal.fire({
                                    title: value,
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            });
                        } else if (response.error_message) {
                            swal.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            dataTable.ajax.reload();
                            $('#loader').hide();
                            $('#edit_modal').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'Floor has been Updated Successfully!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        }
                    },
                    error:  function(jqXHR, textStatus, errorThrown) {
                        $.each(jqXHR.responseJSON.errors, function(index, value) {
                            Swal.fire({
                                title: value,
                                icon: 'error',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        });
                    } 
                });
            });
        });

        function propertyEdit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('properties') }}" + "/" + propertyId + "/floors/" + rowid + "/edit",
                // url: "{{ url('properties') }}" + "/" + propertyId + "/floors/" + rowid,
                type: "GET",
                success: function(response) {

                    // console.log(response);
                    // console.log(response.floor.rooms);
                    // console.log(response.floor.name);

                    // Set values for non-repeater fields
                    $("#edit-floor-name").val(response.floor.name);
                    
                    // // Clear existing inner repeater items
                    // $('.edit-inner-form-repeater').find('div[data-repeater-list="rooms"]','div[data-repeater-list="tables"]').empty();
                    
                    // Set values for repeater fields
                    var newItemHtml = '';

                    if(response.floor.rooms) {

                        // Clear existing repeater items
                        $('.edit-form-repeater').find('div[data-repeater-list="rooms"]').empty();

                        $.each(response.floor.rooms, function (index, room) { 

                            // console.log(index + " : " + room);
                            newItemHtml = `
                                {{-- "rooms" repeater (item) --}}
                                                    <div data-repeater-item>
                                                        <div class="row justify-content-between">

                                                            <div class="w-0 h-0 invisible">
                                                                <input type="hidden" name="id" class="w-0 h-0 edit-room-id"/>
                                                            </div>
                                                            
                                                            <div class="mb-3 col-6">
                                                                <label class="form-label">Room Type</label>
                                                                <select name="room_type_id" class="form-select select2 edit-room-type"
                                                                        data-placeholder="Select Room Type" required>
                                                                {{-- <label class="form-label">Room Type</label>
                                                                <select name="room_type_id" class="form-select select2"
                                                                        data-placeholder="Select Room Type" required> --}}

                                                                    <option value=""></option>
                                                                    @isset($roomTypes)
                                                                        @foreach ($roomTypes as $roomType)
                                                                            <option value="{{$roomType->id}}">{{$roomType->name}}</option>
                                                                        @endforeach
                                                                    @endisset
                                                                </select>
                                                            </div>
                                                            <div class="mb-3 col-4">
                                                                {{-- <label class="form-label" for="room-name">Room Name</label>
                                                                <input type="text" id="room-name" name="name" class="form-control"
                                                                    placeholder="Enter Room Name" /> --}}
                                                                <label class="form-label">Room Name</label>
                                                                <input type="text" name="name" class="form-control edit-room-name"
                                                                    placeholder="Enter Room Name" />
                                                            </div>

                                                            <div class="mb-3 col-lg-2 col-xl-2 col-12">
                                                                <button type="button" class="btn btn-label-danger mt-4 col-12" title="Remove Room" data-repeater-delete>
                                                                    <i class="ti ti-trash ti-xs me-1"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                            `;

                            // append new repeater-item
                            $('.edit-form-repeater').find('div[data-repeater-list="rooms"]').append(newItemHtml);

                            // Set values for the new item (last appended repeater-item)
                            let newItem = $('.edit-form-repeater').find('div[data-repeater-list="rooms"]').children().last();
                            // console.log(newItem.text());
                            // console.log(newItem.find(".edit-room-type"));

                            newItem.find(".edit-room-id").val(room.id);
                            newItem.find(".edit-room-type").val(room.room_type_id).select2();
                            newItem.find(".edit-room-name").val(room.name);

                        });

                    }
                    else if(response.floor.tables) {

                        // Clear existing repeater items
                        $('.edit-form-repeater').find('div[data-repeater-list="tables"]').empty();
                    
                        
                        $.each(response.floor.tables, function (index, table) { 

                            // console.log(index + " : " + room);
                            newItemHtml = `
                            {{-- "tables" repeater (item) --}}
                                                    <div data-repeater-item>
                                                        <div class="row justify-content-between">
                                                            <div class="w-0 h-0 invisible">
                                                                <input type="hidden" name="id" class="w-0 h-0 edit-table-id"/>
                                                            </div>
                                                            <div class="mb-3 col-6">
                                                                <label class="form-label">Table Name</label>
                                                                <input type="text" class="form-control edit-table-name"
                                                                        name="name"
                                                                    placeholder="Enter Table Name" />
                                                            </div>
                                                            <div class="mb-3 col-4">
                                                                <label class="form-label">Seating Capacity</label>
                                                                <input type="number" class="form-control edit-seating-capacity"
                                                                        name="seating_capacity"
                                                                    placeholder="Enter Table Capacity" />
                                                            </div>
                                                            <div class="mb-3 col-lg-2 col-xl-2 col-12">
                                                                <button type="button" class="btn btn-label-danger mt-4 col-12" title="Remove Table" data-repeater-delete>
                                                                    <i class="ti ti-trash ti-xs me-1"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>                    
                            `;

                            // append new repeater-item
                            $('.edit-form-repeater').find('div[data-repeater-list="tables"]').append(newItemHtml);

                            // Set values for the new item (last appended repeater-item)
                            let newItem = $('.edit-form-repeater').find('div[data-repeater-list="tables"]').children().last();

                            newItem.find(".edit-table-id").val(table.id);
                            newItem.find(".edit-table-name").val(table.name);
                            newItem.find(".edit-seating-capacity").val(table.seating_capacity);

                        });

                    }


                    $('#edit_modal').modal('show');

                }
            });
        }

        function deleteProperty(id) {
            rowid = id;
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "{{ url('properties') }}" + "/" + propertyId + "/floors/" + rowid,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        dataTable.ajax.reload();
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'Property has been Deleted Successfully!',
                                            customClass: {
                                                confirmButton: 'btn btn-success'
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        // when Modal is Closed without Submitting the Form (the form fields may contain Values)
        $('#add_modal, #edit_modal').on("hidden.bs.modal", function (e) {

            $('#add_form').reset();
            // $('#update_form').reset();
            $('#loader').hide();
        });
    </script>
@endsection
