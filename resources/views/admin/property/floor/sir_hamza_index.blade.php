@extends('admin.layouts.master')
@section('title', 'Floors')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-2 mb-1"><span class="text-muted fw-light">Floors /</span> List</h4>
        <div class="card">
            <div class="card-datatable table-responsive">
                <table class="datatables-users table">
                    <thead class="border-top">
                        <tr>
                            <th class="not_include"></th>
                            <th>Sr.#</th>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal fade" id="add_modal" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered1 modal-simple modal-lg">
                    <div class="modal-content p-3 p-md-5">
                        <div class="modal-body">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            <div class="text-center mb-4">
                                <h3 class="mb-2">Add Floor</h3>
                            </div>
                            <form class="repeater">
                                <div data-repeater-list="outer-list">
                                    <div data-repeater-item>
                                        <div class="row">
                                            <div class="col-8">
                                                <label class="form-label" for="name"> Name</label>
                                                <input type="text" class="form-control" id="name" name="name"
                                                    placeholder="Enter Name" required />
                                            </div>
                                            <div class="mb-3 col-lg-12 col-xl-2 col-12 d-flex align-items-center">
                                                <button class="btn btn-label-danger mt-4" title="Remove Floor"
                                                    data-repeater-delete>
                                                    <i class="ti ti-trash ti-xs me-1"></i>
                                                </button>
                                            </div>
                                        </div>

                                        <!-- innner repeater -->
                                        @isset($floors)
                                        @else
                                            <div>Server Error</div>
                                        @endisset
                                        <div class="inner-repeater">
                                            <div data-repeater-list="inner-list">
                                                <div data-repeater-item>
                                                    <div class="row justify-content-between">
                                                        <div class="mb-3 col-6">
                                                            <label class="form-label" for="table-name">Table Name</label>
                                                            <input type="text" id="table-name" class="form-control"
                                                                name="name" placeholder="Enter Table Name" />
                                                        </div>
                                                        <div class="mb-3 col-4">
                                                            <label class="form-label" for="seating-capacity">Seating
                                                                Capacity</label>
                                                            <input type="number" id="seating-capacity" class="form-control"
                                                                name="seating_capacity"
                                                                placeholder="Enter Table Capacity" />
                                                        </div>
                                                        <div class="mb-3 col-lg-2 col-xl-2 col-12">
                                                            <button class="btn btn-label-danger mt-4 col-12"
                                                                title="Remove Table" data-repeater-delete>
                                                                <i class="ti ti-trash ti-xs me-1"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                </div>
                                            </div>
                                            <div class="mb-2">
                                                <button type="button" class="btn btn-primary" data-repeater-create>
                                                    <i class="ti ti-plus me-1"></i>
                                                    <span class="align-middle">Add Table</span>
                                                </button>
                                            </div>
                                        </div>
                                        @isset($floors)
                                            {{-- "inner (rooms / tables)" repeater container --}}
                                            <div class="inner-repeater">

                                                @if ($floors[0]->property_id === '2')
                                                    {{-- "rooms" repeater (list) --}}
                                                    <div data-repeater-list="rooms">

                                                        {{-- "rooms" repeater (item) --}}
                                                        <div data-repeater-item>
                                                            <div class="row justify-content-between">
                                                                <div class="mb-3 col-6">
                                                                    {{-- <label class="form-label" for="room-type">Room Type</label>
                                                                <select id="room-type" name="room_type_id" class="form-select select2"
                                                                        data-placeholder="Select Room Type" required> --}}
                                                                    <label class="form-label">Room Type</label>
                                                                    <select name="room_type_id" class="form-select select2"
                                                                        data-placeholder="Select Room Type" required>

                                                                        <option value=""></option>
                                                                        @foreach ($roomTypes as $roomType)
                                                                            <option value="{{ $roomType->id }}">
                                                                                {{ $roomType->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="mb-3 col-4">
                                                                    {{-- <label class="form-label" for="room-name">Room Name</label>
                                                                <input type="text" id="room-name" name="name" class="form-control"
                                                                    placeholder="Enter Room Name" /> --}}
                                                                    <label class="form-label">Room Name</label>
                                                                    <input type="text" name="name" class="form-control"
                                                                        placeholder="Enter Room Name" />
                                                                </div>

                                                                <div class="mb-3 col-lg-2 col-xl-2 col-12">
                                                                    <button class="btn btn-label-danger mt-4 col-12"
                                                                        title="Remove Room" data-repeater-delete>
                                                                        <i class="ti ti-trash ti-xs me-1"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            {{-- <hr /> --}}
                                                        </div>

                                                    </div>

                                                    <div class="mb-0">
                                                        <button type="button" class="btn btn-primary" data-repeater-create>
                                                            <i class="ti ti-plus me-1"></i>
                                                            <span class="align-middle">Add Room</span>
                                                        </button>
                                                    </div>
                                                @elseif ($floors[0]->property_id === '3')
                                                    {{-- "tables" repeater (list) --}}
                                                    <div data-repeater-list="tables">

                                                        {{-- "tables" repeater (item) --}}
                                                        <div data-repeater-item>
                                                            <div class="row justify-content-between">
                                                                <div class="mb-3 col-6">
                                                                    <label class="form-label" for="table-name">Table
                                                                        Name</label>
                                                                    <input type="text" id="table-name" class="form-control"
                                                                        name="name" placeholder="Enter Table Name" />
                                                                </div>
                                                                <div class="mb-3 col-4">
                                                                    <label class="form-label" for="seating-capacity">Seating
                                                                        Capacity</label>
                                                                    <input type="number" id="seating-capacity"
                                                                        class="form-control" name="seating_capacity"
                                                                        placeholder="Enter Table Capacity" />
                                                                </div>
                                                                <div class="mb-3 col-lg-2 col-xl-2 col-12">
                                                                    <button class="btn btn-label-danger mt-4 col-12"
                                                                        title="Remove Table" data-repeater-delete>
                                                                        <i class="ti ti-trash ti-xs me-1"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <hr />
                                                        </div>

                                                        <div class="mb-0">
                                                            <button type="button" class="btn btn-primary"
                                                                data-repeater-create>
                                                                <i class="ti ti-plus me-1"></i>
                                                                <span class="align-middle">Add Table</span>
                                                            </button>
                                                        </div>

                                                    </div>
                                                @endif

                                            </div>
                                        @else
                                            <div>Server Error</div>
                                        @endisset

                                    </div>
                                </div>
                                <div class="mb-0">
                                    <button type="button" class="btn btn-primary" data-repeater-create>
                                        <i class="ti ti-plus me-1"></i>
                                        <span class="align-middle">Add Floor</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="edit_modal" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered1 modal-simple modal-add-new-cc">
                    <div class="modal-content p-3 p-md-5">
                        <div class="modal-body">
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                            <div class="text-center mb-4">
                                <h3 class="mb-2">Edit Floor</h3>
                            </div>
                            <form id="update_form" class="form row g-3">
                                @csrf
                                @method('PUT')
                                <div class="col-12">
                                    <label class="form-label" for="edit_name"> Name</label>
                                    <input type="text" class="form-control" id="edit_name" name="name"
                                        placeholder="Enter Name" required />
                                </div>
                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-primary me-sm-3 me-1">Update</button>
                                    <button type="reset" class="btn btn-label-secondary btn-reset"
                                        data-bs-dismiss="modal" aria-label="Close">
                                        Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sk-wave sk-primary" id="loader"
            style="display: none; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {

            $('.repeater').repeater({
                show: function() {
                    var fromControl = $(this).find('.form-control, .form-select');
                    var formLabel = $(this).find('.form-label');

                    $(this).slideDown();
                },
                hide: function(deleteElement) {
                    if (confirm('Are you sure you want to delete Floor element?')) {
                        $(this).slideUp(deleteElement);
                    }
                },
                repeaters: [{
                    // (Required)
                    // Specify the jQuery selector for this nested repeater
                    selector: '.inner-repeater',
                    show: function() {
                        // Add any specific functionality for showing the inner repeater
                        $(this).slideDown();
                    },
                    hide: function(deleteElement) {
                        if (confirm('Are you sure you want to delete this Table element?')) {
                            $(this).slideUp(deleteElement);
                        }
                    }
                }]
            });


            var propertyId = '{!! request()->route('property')->id !!}';
            var url = "{{ route('floors.index', ['property' => ':propertyId']) }}";
            url = url.replace(':propertyId', propertyId);
            dataTable = $('.datatables-users').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [
                    // columns according to JSON
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'action'
                    }
                ],
                columnDefs: [{
                    // For Responsive
                    className: 'control',
                    searchable: false,
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0,
                    render: function render(data, type, full, meta) {
                        return '';
                    }
                }, {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function render(data, type, full, meta) {
                        return '<div class="d-inline-block text-nowrap">' +
                            '<a href="javascript:;" class="text-body" onclick=propertyEdit(' +
                            full
                            .id + ')>' + '<i class=\"ti ti-edit\"></i>' +
                            '</a>' +
                            '<a href="javascript:;" class="text-body" onclick=deleteProperty(' +
                            full.id + ')>' + '<i class=\"ti ti-trash\"></i>' +
                            '</a>' + '</div>';
                    }
                }],
                order: [
                    [2, 'desc']
                ],
                dom: '<"row mx-2"' + '<"col-md-2"<"me-3"l>>' +
                    '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
                    '>t' + '<"row mx-2"' + '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
                language: {
                    sLengthMenu: '_MENU_',
                    search: '',
                    searchPlaceholder: 'Search..'
                },
                // Buttons with Dropdown
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-label-primary dropdown-toggle mx-3',
                    text: '<i class="ti ti-logout rotate-n90 me-2"></i>Export',
                    buttons: [{
                        extend: 'print',
                        title: 'Users',
                        text: '<i class="ti ti-printer me-2" ></i>Print',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList !== undefined && item
                                            .classList.contains('user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        },
                        customize: function customize(win) {
                            //customize print view for dark
                            $(win.document.body).css('color', config.colors
                                    .headingColor)
                                .css('border-color', config.colors.borderColor).css(
                                    'background-color', config.colors.body);
                            $(win.document.body).find('table').addClass('compact').css(
                                    'color', 'inherit').css('border-color', 'inherit')
                                .css(
                                    'background-color', 'inherit');
                        }
                    }, {
                        extend: 'csv',
                        title: 'Users',
                        text: '<i class="ti ti-file-text me-2" ></i>Csv',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'excel',
                        title: 'Users',
                        text: '<i class="ti ti-file-spreadsheet me-2"></i>Excel',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'pdf',
                        title: 'Users',
                        text: '<i class="ti ti-file-text me-2"></i>Pdf',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'copy',
                        title: 'Users',
                        text: '<i class="ti ti-copy me-1" ></i>Copy',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be copy
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }]
                }, {
                    text: '<i class="ti ti-plus me-0 me-sm-1"></i><span class="d-none d-sm-inline-block">Add New</span>',
                    className: 'add-new btn btn-primary',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#add_modal'
                    }
                }],
                // For responsive popup
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function header(row) {
                                var data = row.data();
                                return 'Details of ' + data['name'];
                            }
                        }),
                        type: 'column',
                        renderer: function renderer(api, rowIdx, columns) {
                            var data = $.map(columns, function(col, i) {
                                return col.title !==
                                    '' // ? Do not show row in modal popup if title is blank (for check box)
                                    ?
                                    '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' +
                                    col
                                    .columnIndex + '">' + '<td>' + col.title + ':' + '</td> ' +
                                    '<td>' + col.data + '</td>' + '</tr>' : '';
                            }).join('');
                            return data ? $('<table class="table"/><tbody />').append(data) : false;
                        }
                    }
                }
            });

            $("#add_form").submit(function(e) {
                $('#loader').show();
                e.preventDefault();
                $.ajax({
                    url: "{{ route('properties.store') }}",
                    type: "POST",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function success(response) {
                        // sweetalert
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Swal.fire({
                                    title: value,
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            });
                        } else if (response.error_message) {
                            swal.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            dataTable.ajax.reload();
                            $('#loader').hide();
                            $('#add_modal').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'Floor has been Added Successfully!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        }
                    },
                });
            });

            $("#update_form").submit(function(e) {
                $('#loader').show();
                e.preventDefault();
                $.ajax({
                    url: "{{ url('properties') }}" + "/" + rowid,
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function success(response) {
                        // sweetalert
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Swal.fire({
                                    title: value,
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            });
                        } else if (response.error_message) {
                            swal.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            dataTable.ajax.reload();
                            $('#loader').hide();
                            $('#edit_modal').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'Floor has been Updated Successfully!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        }
                    },
                });
            });
        });

        function propertyEdit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('properties') }}" + "/" + rowid + "/edit",
                type: "GET",
                success: function(response) {
                    $('#edit_property_type').val(response.property_type_id);
                    $('#edit_name').val(response.name);
                    $('#edit_address').val(response.address);
                    $('#edit_modal').modal('show');

                }
            });
        }

        function deleteProperty(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "properties/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        dataTable.ajax.reload();
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'Property has been Deleted Successfully!',
                                            customClass: {
                                                confirmButton: 'btn btn-success'
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
