<footer class="content-footer footer bg-footer-theme">
    <div class="container-xxl">
        <div class="footer-container d-flex align-items-center justify-content-between py-2 flex-md-row flex-column">
            <div>
                ©
                <script>
                    document.write(new Date().getFullYear());
                </script>
            </div>
            <div class="d-none d-lg-inline-block">
                Powered By: Bright Lines Solution (PVT) LTD. (www.bls.com.pk, Waqar Safdar)
            </div>
        </div>
    </div>
</footer>
