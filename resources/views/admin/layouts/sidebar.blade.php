@php
    $routeParameters = \Route::current()->parameters;
    $propertyId = getProperty();
@endphp
<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
        <a href="{{ route('dashboard') }}" class="app-brand-link">
            <span class="app-brand-text demo menu-text fw-bold">PMS</span>
        </a>

        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto">
            <i class="ti menu-toggle-icon d-none d-xl-block ti-sm align-middle"></i>
            <i class="ti ti-x d-block d-xl-none ti-sm align-middle"></i>
        </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
        {{-- <li class="menu-header small text-uppercase">
            <span class="menu-header-text" data-i18n="Home">Home</span>
        </li> --}}
        {{-- @role('Super Admin') --}}
        @if (!array_key_exists('property', $routeParameters))
            @can('view-dashboard')
                <li class="menu-item {{ \Route::getFacadeRoot()->current()->uri() == '/' ? 'active' : '' }}">
                    <a href="{{ route('dashboard') }}" class="menu-link">
                        <i class="menu-icon tf-icons ti ti-smart-home"></i>
                        <span class="menu-title text-truncate" data-i18n="Dashboard">Dashboard</span>
                    </a>
                </li>
            @endcan
        @else
            @can('view-main-dashboard')
                <li
                    class="menu-item {{ \Route::getFacadeRoot()->current()->uri() == 'properties/{property}' ? 'active' : '' }}">
                    <a href="{{ route('property.dashboard', request('property')) }}" class="menu-link">
                        <i class="menu-icon tf-icons ti ti-smart-home"></i>
                        <span class="menu-title text-truncate" data-i18n="Dashboard">Dashboard</span>
                    </a>
                </li>
            @endcan
            {{-- @endrole --}}
        @endif

        @if (array_key_exists('property', $routeParameters))
            @can(['pos-list', 'sales-list', 'kitchens-list', 'reservations-list', 'room-booking-list',
                'room-service-list', 'room-condition-list', 'house-keeper-list'])
                <li
                    class="menu-item {{ \Route::getFacadeRoot()->current()->uri() == 'properties/{property}/sales/create' || \Route::getFacadeRoot()->current()->uri() == 'properties/{property}/sales' || request()->segment(3) == 'kitchen-orders' || request()->segment(3) == 'booking' || request()->segment(3) == 'bookingsummary' ? 'active open' : '' }}">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons ti ti-report-analytics"></i>
                        <div data-i18n="Front Office">Front Office</div>
                        {{-- <div class="badge bg-primary rounded-pill ms-auto">3</div> --}}
                    </a>
                    <ul class="menu-sub">
                        @can('pos-list')
                            <li
                                class="menu-item {{ \Route::getFacadeRoot()->current()->uri() == 'properties/{property}/sales/create' ? 'active' : '' }}">
                                <a class="menu-link" href='{{ url("properties/$propertyId/sales/create") }}'>
                                    <div data-i18n="POS">POS</div>
                                </a>
                            </li>
                        @endcan

                        @can('sales-list')
                            <li
                                class="menu-item {{ \Route::getFacadeRoot()->current()->uri() == 'properties/{property}/sales' ? 'active' : '' }}">
                                <a class="menu-link" href='{{ url("properties/$propertyId/sales") }}'>
                                    <div data-i18n="Sales">Sales</div>
                                </a>
                            </li>
                        @endcan
                        @can('kitchens-list')
                            <li class="menu-item {{ request()->segment(3) == 'kitchen-orders' ? 'active' : '' }}">
                                <a class="menu-link" href='{{ url("properties/$propertyId/kitchen-orders") }}'>
                                    <div data-i18n="kitchen">kitchen</div>
                                </a>
                            </li>
                        @endcan
                        {{-- <li class="menu-item {{ \Route::getFacadeRoot()->current()->uri() == 'tables-view' ? 'active' : '' }}">
                            <a  class="menu-link" href='{{ url("properties/$propertyId/tables-view") }}'>
                                <div data-i18n="Tables">Tables</div>
                            </a>
                        </li> --}}
                        {{-- @can('reservations-list')
                        <li class="menu-item {{ \Route::getFacadeRoot()->current()->uri() == 'reservations' ? 'active' : '' }}">
                            <a class="menu-link" href='{{ url("properties/$propertyId/reservations") }}'>
                                <div data-i18n="Reservation">Reservation</div>
                            </a>
                        </li>
                    @endcan --}}

                        @can('room-booking-list')
                            <li class="menu-item {{ request()->segment(3) == 'booking' ? 'active' : '' }}">
                                <a class="menu-link" href='{{ url("properties/$propertyId/booking") }}'>
                                    <div data-i18n="Booking Room">Booking Room</div>
                                </a>
                            </li>
                            <li class="menu-item {{ request()->segment(3) == 'calender' ? 'active' : '' }}">
                                <a class="menu-link" href='{{ url("properties/$propertyId/calender") }}'>
                                    <div data-i18n="Calender Booking">Calender Booking</div>
                                </a>
                            </li>
                            <li class="menu-item {{ request()->segment(3) == 'bookingsummary' ? 'active' : '' }}">
                                <a class="menu-link" href='{{ url("properties/$propertyId/bookingsummary") }}'>
                                    <div data-i18n="Booking Summary">Booking Summary</div>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan
        @endif

        @if (array_key_exists('property', $routeParameters))
            @can(['room-service-list', 'room-condition-list', 'house-keeper-list', 'room-status-list',
                'customer-type-list', 'company-reference-list', 'reference-list'])
                <li
                    class="menu-item {{ request()->segment(3) == 'room-services' || request()->segment(3) == 'housekeeper' || request()->segment(3) == 'room-condition' || request()->segment(3) == 'room-status' ? 'active open' : '' }}">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons ti ti-home"></i>
                        <div data-i18n="House Keeping">House Keeping</div>
                        {{-- <div class="badge bg-primary rounded-pill ms-auto">3</div> --}}
                    </a>
                    <ul class="menu-sub">
                        @can('room-service-list')
                            <li class="menu-item {{ request()->segment(3) == 'room-services' ? 'active' : '' }}">
                                <a class="menu-link" href='{{ url("properties/$propertyId/room-services") }}'>
                                    <span data-i18n="Room Services">Room Services</span>
                                </a>
                            </li>
                        @endcan
                        @can('house-keeper-list')
                            <li class="menu-item {{ request()->segment(3) == 'housekeeper' ? 'active' : '' }}">
                                <a class="menu-link" href='{{ url("properties/$propertyId/housekeeper") }}'>
                                    <span data-i18n="House Keeper">House Keeper</span>
                                </a>
                            </li>
                        @endcan
                        @can('room-condition-list')
                            <li class="menu-item {{ request()->segment(3) == 'room-condition' ? 'active' : '' }}">
                                <a href="{{ url("properties/$propertyId/room-condition") }}" class="menu-link">
                                    <span data-i18n="Room Condition">Room Condition</span>
                                </a>
                            </li>
                        @endcan
                        @can('room-status-list')
                            <li class="menu-item {{ request()->segment(3) == 'room-status' ? 'active' : '' }}">
                                <a href="{{ url("properties/$propertyId/room-status") }}" class="menu-link">
                                    <span data-i18n="Room Status">
                                        Room Status
                                    </span>
                                    @if ($count = totalRoomsNeedRepairingCount())
                                        <span class="ms-3 py-1 px-2 badge rounded-pill text-bg-danger text-truncate">
                                            {{ $count }}
                                        </span>
                                    @endif
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan
        @endif

        @can(['properties-list', 'property-type-list', 'floors-list', 'room-type-list', 'customer-type-list',
            'company-reference-list', 'reference-list'])
            <li
                class="menu-item {{ \Route::getFacadeRoot()->current()->uri() == 'properties' || request()->segment(3) == 'floors' || request()->segment(3) == 'room-type' || request()->segment(3) == 'customer-type' || request()->segment(3) == 'company-reference' || request()->segment(3) == 'reference' ? 'active open' : '' }}">
                <a href="javascript:void(0);" class="menu-link menu-toggle">
                    <i class="menu-icon tf-icons ti ti-building"></i>
                    <div data-i18n="Property Management">Property Management</div>
                    {{-- <div class="badge bg-primary rounded-pill ms-auto">3</div> --}}
                </a>
                <ul class="menu-sub">
                    @if (!array_key_exists('property', $routeParameters))
                        @can('properties-list')
                            <li
                                class="menu-item {{ \Route::getFacadeRoot()->current()->uri() == 'properties' ? 'active' : '' }}">
                                <a href="{{ url('properties') }}" class="menu-link">
                                    <span data-i18n="Properties">Properties</span>
                                </a>
                            </li>
                        @endcan
                        {{-- @can('property-type-list')
                            <li class="menu-item {{ \Route::getFacadeRoot()->current()->uri() == 'property_types' ? 'active' : '' }}">
                                <a class="menu-link" href="{{ url('property_types') }}">
                                    <span data-i18n="Property Types">Property Types</span>
                                </a>
                            </li>
                        @endcan --}}
                    @endif
                    @if (array_key_exists('property', $routeParameters))
                        @can('floors-list')
                            <li class="menu-item {{ request()->segment(3) == 'floors' ? 'active' : '' }}">
                                <a href='{{ url("properties/$propertyId/floors") }}' class="menu-link">
                                    <span data-i18n="Floors">Floors</span>
                                </a>
                            </li>
                        @endcan
                        @can('room-type-list')
                            <li class="menu-item {{ request()->segment(3) == 'room-type' ? 'active' : '' }}">
                                <a href='{{ url("properties/$propertyId/room-type") }}' class="menu-link">
                                    <span data-i18n="Room Type">Room Type</span>
                                </a>
                            </li>
                        @endcan
                        @can('customer-type-list')
                            <li class="menu-item {{ request()->segment(3) == 'customer-type' ? 'active' : '' }}">
                                <a href='{{ url("properties/$propertyId/customer-type") }}' class="menu-link">
                                    <span data-i18n="Customer Type">Customer Type</span>
                                </a>
                            </li>
                        @endcan

                        @can('company-reference-list')
                            <li class="menu-item {{ request()->segment(3) == 'company-reference' ? 'active' : '' }}">
                                <a href='{{ url("properties/$propertyId/company-reference") }}' class="menu-link">
                                    <span data-i18n="Company Reference">Company Reference</span>
                                </a>
                            </li>
                        @endcan
                        @can('reference-list')
                            <li class="menu-item {{ request()->segment(3) == 'reference' ? 'active' : '' }}">
                                <a href='{{ url("properties/$propertyId/reference") }}' class="menu-link">
                                    <span data-i18n="Reference">Reference</span>
                                </a>
                            </li>
                        @endcan
                    @endif
                </ul>
            </li>
        @endcan

        @if (array_key_exists('property', $routeParameters))
            {{-- @can(['properties-list', 'property-type-list', 'floors-list', 'room-type-list', 'customer-type-list', 'company-reference-list', 'reference-list']) --}}
            <li
                class="menu-item {{ request()->segment(3) == 'vehicle-type' || request()->segment(3) == 'third_party_vehicle' || request()->segment(3) == 'vehicle' ? 'active open' : '' }}">
                <a href="javascript:void(0);" class="menu-link menu-toggle">
                    <i class="menu-icon tf-icons ti ti-car"></i>
                    <div data-i18n="Vehicle">Vehicle</div>
                    {{-- <div class="badge bg-primary rounded-pill ms-auto">3</div> --}}
                </a>
                <ul class="menu-sub">
                    {{-- @can('menu-categories-list') --}}
                    <li class="menu-item {{ request()->segment(3) == 'vehicle-type' ? 'active' : '' }}">
                        <a class="menu-link" href='{{ url("properties/$propertyId/vehicle-type") }}'>
                            <span data-i18n="Vehicle Type">Vehicle Type</span>
                        </a>
                    </li>
                    {{-- @endcan --}}
                    {{-- @can('menu-categories-list') --}}
                    <li class="menu-item {{ request()->segment(3) == 'vehicle' ? 'active' : '' }}">
                        <a class="menu-link" href='{{ url("properties/$propertyId/vehicle") }}'>
                            <span data-i18n="Vehicle">Vehicle</span>
                        </a>
                    </li>
                    {{-- @endcan --}}
                    {{-- @can('menu-categories-list') --}}
                    <li class="menu-item {{ request()->segment(3) == 'third_party_vehicle' ? 'active' : '' }}">
                        <a class="menu-link" href='{{ url("properties/$propertyId/third_party_vehicle") }}'>
                            <span data-i18n="Third Party Vehicle">Third Party Vehicle</span>
                        </a>
                    </li>
                    {{-- @endcan --}}
                </ul>
            </li>
            {{-- @endcan --}}
        @endif

        @if (array_key_exists('property', $routeParameters))
            @can(['menu-categories-list', 'menu-servings-list', 'menu-items-list'])
                <li
                    class="menu-item {{ request()->segment(3) == 'menu-categories' || request()->segment(3) == 'menu-servings' || request()->segment(3) == 'menu-items' ? 'active open' : '' }}">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons ti ti-menu"></i>
                        <div data-i18n="Menu">Menu</div>
                        {{-- <div class="badge bg-primary rounded-pill ms-auto">3</div> --}}
                    </a>
                    <ul class="menu-sub">
                        @can('menu-categories-list')
                            <li class="menu-item {{ request()->segment(3) == 'menu-categories' ? 'active' : '' }}">
                                <a class="menu-link" href='{{ url("properties/$propertyId/menu-categories") }}'>
                                    <span data-i18n="Categories">Categories</span>
                                </a>
                            </li>
                        @endcan
                        @can('menu-servings-list')
                            <li class="menu-item {{ request()->segment(3) == 'menu-servings' ? 'active' : '' }}">
                                <a href='{{ url("properties/$propertyId/menu-servings") }}' class="menu-link">
                                    <span data-i18n="Servings">Servings</span>
                                </a>
                            </li>
                        @endcan
                        @can('menu-items-list')
                            <li class="menu-item {{ request()->segment(3) == 'menu-items' ? 'active' : '' }}">
                                <a href='{{ url("properties/$propertyId/menu-items") }}' class="menu-link">
                                    <span data-i18n="Items">Items</span>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan
        @endif

        @if (!array_key_exists('property', $routeParameters))
            @canany(['users-list', 'roles-list'])
                <li class="menu-header small text-uppercase">
                    <span class="menu-header-text" data-i18n="Settings">Settings</span>
                </li>
            @endcanany
            @can('users-list')
                <li class="menu-item {{ \Route::getFacadeRoot()->current()->uri() == 'users' ? 'active' : '' }}">
                    <a href="{{ url('users') }}" class="menu-link">
                        <i class="menu-icon fa fa-users text-success"></i><span class="menu-title text-truncate"
                            data-i18n="Users">Users</span>
                    </a>
                </li>
            @endcan
            @can('roles-list')
                <li class="menu-item {{ \Route::getFacadeRoot()->current()->uri() == 'roles' ? 'active' : '' }}">
                    <a href="{{ url('roles') }}" class="menu-link">
                        <i class="menu-icon fa fa-tasks text-danger"></i>
                        <span class="menu-title text-truncate" data-i18n="Roles">Roles</span>
                    </a>
                </li>
            @endcan
        @endif
        @if (array_key_exists('property', $routeParameters))
            {{-- @can('Report') --}}
            @canany(['sale-report'])
                <li class="menu-header small text-uppercase">
                    <span class="menu-header-text" data-i18n="Reports">Reports</span>
                </li>
            @endcanany
            @can('sale-report')
                <li class="menu-item {{ request()->segment(3) == 'sale-report' ? 'active' : '' }}">
                    <a class="menu-link" href='{{ url("properties/$propertyId/sale-report") }}'>
                        <i class="menu-icon tf-icons ti ti-report-money"></i>
                        <span class="menu-title text-truncate" data-i18n="Sale">Sale</span>
                    </a>
                </li>
                <li class="menu-item {{ request()->segment(3) == 'police-report' ? 'active' : '' }}">
                    <a class="menu-link" href='{{ url("properties/$propertyId/police-report") }}'>
                        <i class="menu-icon tf-icons ti ti-report-money"></i>
                        <span class="menu-title text-truncate" data-i18n="Police">Police</span>
                    </a>
                </li>
            @endcan
        @endif
        {{-- @endrole --}}

    </ul>
</aside>
