@extends('admin.layouts.master')
@section('title', 'Users')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-2 mb-1"><span class="text-muted fw-light">Users /</span> List</h4>
        <div class="card">
            <div class="card-datatable table-responsive">
                <table class="datatables-users table">
                    <thead class="border-top">
                        <tr>
                            <th class="not_include"></th>
                            <th>Sr.#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <!-- Add New Modal -->
            <div class="modal fade" id="add_modal" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered1 modal-lg modal-simple modal-add-new-cc">
                    <div class="modal-content p-3 p-md-5">
                        <div class="modal-body">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            <div class="text-center mb-4">
                                <h3 class="mb-2">Add New User</h3>
                            </div>
                            <form class="form row g-3" id="add_form">
                                @csrf
                                <div class="col-6 mb-2">
                                    <label class="form-label" for="add-user-fullname"> Name</label>
                                    <input type="text" class="form-control" name="name" placeholder="Enter Name"
                                        required />
                                </div>
                                <div class="col-6 mb-2">
                                    <label class="form-label" for="email">Email</label>
                                    <input type="text" class="form-control" name="email"
                                        placeholder="john.doe@example.com" required />
                                </div>
                                {{-- <div class="col-6 mb-2">
                                    <label class="form-label" for="mobile">Mobile</label>
                                    <input type="text" id="mobile" class="form-control" name="mobile" placeholder="Enter Mobile Number" />
                                </div>
                                <div class="col-6 mb-2">
                                    <label class="form-label" for="cnic">CNIC</label>
                                    <input type="text" id="cnic" class="form-control" name="cnic" placeholder="Enter CNIC Number" />
                                </div>
                                <div class="col-6 mb-2">
                                    <label class="form-label" for="address">Address</label>
                                    <input type="text" id="address" class="form-control" name="address" placeholder="Enter Address Number" />
                                </div> --}}
                                {{-- <div class="col-6 mb-2">
                                    <label class="form-label" for="train">Train</label>
                                    <input type="text" id="train" class="form-control" name="train"
                                        placeholder="Enter Train" />
                                </div> --}}

                                <div class="col-6 mb-2">
                                    <div class="form-password-toggle">
                                        <label class="form-label" for="password">Password</label>
                                        <div class="input-group input-group-merge">
                                            <input type="password" id="multicol-password" name="password"
                                                class="form-control"
                                                placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                                autocomplete="new-password" required />
                                            <span class="input-group-text cursor-pointer" id="multicol-password2"><i
                                                    class="ti ti-eye-off"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 mb-2">
                                    <div class="form-password-toggle">
                                        <label class="form-label" for="password_confirmation">Confirm Password</label>
                                        <div class="input-group input-group-merge">
                                            <input type="password" id="password_confirmation" name="password_confirmation"
                                                class="form-control"
                                                placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                                autocomplete="new-password" required />
                                            <span class="input-group-text cursor-pointer" id="passwordConfirmation"><i
                                                    class="ti ti-eye-off"></i></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="mb-3">
                                    <label class="form-label" for="user-role">User Role</label>
                                    <select id="user-role" name="role" class="form-select select2"
                                        data-placeholder="Select Role" required>
                                        @foreach ($roles as $role)
                                            <option value=""></option>
                                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label" for="property_id">User Property</label>
                                    <select id="property_id" name="property_id[]" class="form-select select2"
                                        data-placeholder="Select Property" multiple required>
                                        @foreach ($properties as $property)
                                            <option value=""></option>
                                            <option value="{{ $property->id }}">{{ $property->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                                <button type="reset" class="btn btn-label-secondary"
                                    data-bs-dismiss="offcanvas">Cancel</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!--/ Add New Modal -->

            <!-- edit Modal -->
            <div class="modal fade" id="edit_modal" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered1 modal-lg modal-simple modal-add-new-cc">
                    <div class="modal-content p-3 p-md-5">
                        <div class="modal-body">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            <div class="text-center mb-4">
                                <h3 class="mb-2">Edit User</h3>
                            </div>

                            <form class="form row g-3" id="update_form">
                                @csrf
                                @method('PUT')
                                <div class="col-6 mb-2">
                                    <label class="form-label" for="add-user-fullname"> Name</label>
                                    <input type="text" class="form-control" id="edit_name" name="name"
                                        placeholder="Enter Name" />
                                </div>
                                <div class="col-6 mb-2">
                                    <label class="form-label" for="add-user-email">Email</label>
                                    <input type="text" class="form-control" id="edit_email" name="email"
                                        placeholder="john.doe@example.com" autocomplete="username" />
                                </div>

                                {{-- <div class="col-6 mb-2">
                                    <label class="form-label" for="edit_mobile">Mobile</label>
                                    <input type="text" id="edit_mobile" class="form-control" name="mobile" placeholder="Enter Mobile Number" />
                                </div>
                                <div class="col-6 mb-2">
                                    <label class="form-label" for="edit_cnic">CNIC</label>
                                    <input type="text" id="edit_cnic" class="form-control" name="cnic" placeholder="Enter CNIC Number" />
                                </div>
                                <div class="col-6 mb-2">
                                    <label class="form-label" for="edit_address">Address</label>
                                    <input type="text" id="edit_address" class="form-control" name="address" placeholder="Enter Address Number" />
                                </div> --}}
                                {{-- <div class="col-6 mb-2">
                                    <label class="form-label" for="edit_train">Train</label>
                                    <input type="text" id="edit_train" class="form-control" name="train"
                                        placeholder="Enter Train" />
                                </div> --}}

                                <div class="col-6 mb-2">
                                    <div class="form-password-toggle">
                                        <label class="form-label" for="edit_multicol-password">Password</label>
                                        <div class="input-group input-group-merge">
                                            <input type="password" id="edit_multicol-password" name="password"
                                                class="form-control"
                                                placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                                aria-describedby="edit_pass" autocomplete="current-password" />
                                            <span class="input-group-text cursor-pointer" id="edit_pass"><i
                                                    class="ti ti-eye-off"></i></span>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="col-6 mb-2">
                                    <div class="form-password-toggle">
                                        <label class="form-label" for="edit_password_confirmation">Confirm
                                            Password</label>
                                        <div class="input-group input-group-merge">
                                            <input type="password" id="edit_password_confirmation"
                                                name="password_confirmation" class="form-control"
                                                placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                                aria-describedby="passwordConfirmation" autocomplete="current-password" />
                                            <span class="input-group-text cursor-pointer" id="passwordConfirmation"><i
                                                    class="ti ti-eye-off"></i></span>
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="col-6 mb-3">
                                    <label class="form-label" for="edit_role">User Role</label>
                                    <select id="edit_role" name="role" class="select2 form-select"
                                        data-placeholder="Select Role" required>
                                        @foreach ($roles as $role)
                                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label" for="date">User Property</label>
                                    <select id="edit_property_id" name="property_id[]" class="select2 form-select"
                                        data-placeholder="Select Property" multiple required>
                                        @foreach ($properties as $property)
                                            <option value="{{ $property->id }}">{{ $property->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary me-sm-3 me-1">Update</button>
                                <button type="reset" class="btn btn-label-secondary"
                                    data-bs-dismiss="offcanvas">Cancel</button>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
            <!--/ edit Modal -->


            {{-- </div>
            </div> --}}

        </div>
        <div class="sk-wave sk-primary" id="loader"
            style="display: none; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
        </div>
    </div>

@section('scripts')
    <script>
        $(document).ready(function() {

            $('.select2').select2();

            // $('#add_modal').modal('show');

            dataTable = $('.datatables-users').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('users.index') }}",
                columns: [
                    // columns according to JSON
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'name'
                    }, {
                        data: 'email'
                    }, {
                        name: 'roles.name',
                        render: function(data, type, full, meta) {
                            let role = full['roles'];
                            let roles = '';
                            if (role.length < 1) {
                                return '-';
                            }

                            $.each(role, function(index, value) {
                                roles +=
                                    '<span class="badge rounded-pill bg-label-success ms-1 mb-1">' +
                                    value.name + '</span>';
                            });

                            return roles
                        }
                    }, {
                        data: 'status'
                    }, {
                        data: 'action'
                    }
                ],
                columnDefs: [{
                    // For Responsive
                    className: 'control',
                    searchable: false,
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0,
                    render: function render(data, type, full, meta) {
                        return '';
                    }
                }, {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function render(data, type, full, meta) {
                        let role = full['roles'];
                        var actions = '<div class="d-inline-block text-nowrap">' +
                            '<a href="javascript:;" class="text-body" onclick=userEdit(' + full
                            .id + ')>' + '<i class=\"ti ti-edit\"></i>' +
                            '</a>';

                        // Check if the user is not a superadmin before rendering the delete button
                        // if (role.length > 0) {
                        //     if (role[0].name !== 'Super Admin') {
                        //         actions +=
                        //             '<a href="javascript:;" class="text-body" onclick=deleteUser(' +
                        //             full.id + ')>' + '<i class=\"ti ti-trash\"></i>' +
                        //             '</a>';
                        //     }
                        // }

                        actions += '</div>';

                        return actions;
                    }
                }, {
                    // Status
                    targets: -2,
                    title: 'Status',
                    render: function(data, type, full, meta) {
                        // console.log(data);
                        if (data == 1) {
                            return (
                                '<input  type="checkbox" data-id="' + full.id +
                                '" class="toggle-class" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-size="small" data-on="Active" data-off="Disable" onclick=status(' +
                                full.id + ') checked>'
                            );
                        } else {
                            return (
                                '<input  type="checkbox" data-id="' + full.id +
                                '" class="toggle-class " data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-size="small" data-on="Active" onclick=status(' +
                                full.id + ') data-off="Disable">'
                            );
                        }
                    }
                }, ],
                order: [
                    [2, 'desc']
                ],
                dom: '<"row mx-2"' + '<"col-md-2"<"me-3"l>>' +
                    '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
                    '>t' + '<"row mx-2"' + '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
                language: {
                    sLengthMenu: '_MENU_',
                    search: '',
                    searchPlaceholder: 'Search..'
                },
                // Buttons with Dropdown
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-label-primary dropdown-toggle mx-3',
                    text: '<i class="ti ti-logout rotate-n90 me-2"></i>Export',
                    buttons: [{
                        extend: 'print',
                        title: 'Users',
                        text: '<i class="ti ti-printer me-2" ></i>Print',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList !== undefined && item
                                            .classList.contains('user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        },
                        customize: function customize(win) {
                            //customize print view for dark
                            $(win.document.body).css('color', config.colors
                                    .headingColor)
                                .css('border-color', config.colors.borderColor).css(
                                    'background-color', config.colors.body);
                            $(win.document.body).find('table').addClass('compact').css(
                                    'color', 'inherit').css('border-color', 'inherit')
                                .css(
                                    'background-color', 'inherit');
                        }
                    }, {
                        extend: 'csv',
                        title: 'Users',
                        text: '<i class="ti ti-file-text me-2" ></i>Csv',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'excel',
                        title: 'Users',
                        text: '<i class="ti ti-file-spreadsheet me-2"></i>Excel',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'pdf',
                        title: 'Users',
                        text: '<i class="ti ti-file-text me-2"></i>Pdf',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'copy',
                        title: 'Users',
                        text: '<i class="ti ti-copy me-1" ></i>Copy',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be copy
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }]
                }, {
                    text: '<i class="ti ti-plus me-0 me-sm-1"></i><span class="d-none d-sm-inline-block">Add New</span>',
                    className: 'add-new btn btn-primary',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#add_modal'
                    }
                    // attr: {
                    //     'data-bs-toggle': 'offcanvas',
                    //     'data-bs-target': '#offcanvasAddUser'
                    // }
                }],
                // For responsive popup
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function header(row) {
                                var data = row.data();
                                return 'Details of ' + data['name'];
                            }
                        }),
                        type: 'column',
                        renderer: function renderer(api, rowIdx, columns) {
                            var data = $.map(columns, function(col, i) {
                                return col.title !==
                                    '' // ? Do not show row in modal popup if title is blank (for check box)
                                    ?
                                    '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' +
                                    col
                                    .columnIndex + '">' + '<td>' + col.title + ':' + '</td> ' +
                                    '<td>' + col.data + '</td>' + '</tr>' : '';
                            }).join('');
                            return data ? $('<table class="table"/><tbody />').append(data) : false;
                        }
                    }
                }
            });

            // $("#add_form").submit(function(e) {
            //     $('#loader').show();
            //     e.preventDefault();
            //     $.ajax({
            //         url: "{{ route('users.store') }}",
            //         type: "POST",
            //         data: new FormData(this),
            //         processData: false,
            //         contentType: false,
            //         responsive: true,
            //         success: function success(response) {
            //             console.log(response);
            //             // sweetalert
            //             if (response.errors) {
            //                 $.each(response.errors, function(index, value) {
            //                     Swal.fire({
            //                         title: value,
            //                         icon: 'error',
            //                         customClass: {
            //                             confirmButton: 'btn btn-success'
            //                         }
            //                     });
            //                 });
            //             } else if (response.error_message) {
            //                 swal.fire({
            //                     icon: 'error',
            //                     title: 'An error has been occured! Please Contact Administrator.'
            //                 })
            //             } else {
            //                 $('#add_form')[0].reset();
            //                 dataTable.ajax.reload();
            //                 $('#loader').hide();
            //                 // $('#offcanvasAddUser').offcanvas('hide');
            //                 $('#add_modal').modal('hide');
            //                 Swal.fire({
            //                     icon: 'success',
            //                     title: 'Users has been Added Successfully!',
            //                     customClass: {
            //                         confirmButton: 'btn btn-success'
            //                     }
            //                 });
            //             }
            //         },
            //     });
            // });
            $("#add_form").submit(function(e) {
                e.preventDefault(); // Prevent form submission
                $('#loader').show(); // Show the loader

                $.ajax({
                    url: "{{ route('users.store') }}",
                    type: "POST",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        // Hide the loader
                        $('#loader').hide();

                        // Handle server response
                        if (response.errors) {
                            // If there are validation errors, display them using Swal.fire
                            $.each(response.errors, function(field, errors) {
                                $.each(errors, function(index, error) {
                                    Swal.fire({
                                        title: error,
                                        icon: 'error',
                                        customClass: {
                                            confirmButton: 'btn btn-success'
                                        }
                                    });
                                });
                            });
                        } else if (response.error_message) {
                            // Display a generic error message
                            Swal.fire({
                                icon: 'error',
                                title: 'An error occurred! Please contact the administrator.',
                                customClass: {
                                    confirmButton: 'btn btn-danger'
                                }
                            });
                        } else {
                            $('#add_form')[0].reset();
                            dataTable.ajax.reload();
                            $('#add_modal').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'User has been added successfully!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // Hide the loader
                        $('#loader').hide();

                        // Handle HTTP errors
                        if (jqXHR.status === 422) {
                            // This is a validation error, so parse the response JSON
                            const errors = jqXHR.responseJSON.errors;
                            $.each(errors, function(field, errorList) {
                                $.each(errorList, function(index, error) {
                                    Swal.fire({
                                        title: error,
                                        icon: 'error',
                                        customClass: {
                                            confirmButton: 'btn btn-success'
                                        }
                                    });
                                });
                            });
                        } else {
                            // Handle other HTTP error statuses (e.g., 500, 404)
                            Swal.fire({
                                icon: 'error',
                                title: 'An unexpected error occurred. Please try again later.',
                                customClass: {
                                    confirmButton: 'btn btn-danger'
                                }
                            });
                        }
                    }
                });
            });


            // $("#update_form").submit(function(e) {
            //     $('#loader').show();
            //     e.preventDefault();
            //     $.ajax({
            //         url: "{{ url('users') }}" + "/" + rowid,
            //         type: 'POST',
            //         data: new FormData(this),
            //         contentType: false,
            //         processData: false,
            //         success: function success(response) {
            //             // sweetalert
            //             if (response.errors) {
            //                 $.each(response.errors, function(index, value) {
            //                     Swal.fire({
            //                         title: value,
            //                         icon: 'error',
            //                         customClass: {
            //                             confirmButton: 'btn btn-success'
            //                         }
            //                     });
            //                 });
            //             } else if (response.error_message) {
            //                 swal.fire({
            //                     icon: 'error',
            //                     title: 'An error has been occured! Please Contact Administrator.'
            //                 })
            //             } else {
            //                 dataTable.ajax.reload();
            //                 $('#loader').hide();
            //                 // $('#offcanvasEditUser').offcanvas('hide');
            //                 $('#edit_modal').modal('hide');
            //                 Swal.fire({
            //                     icon: 'success',
            //                     title: 'Users has been Updated Successfully!',
            //                     customClass: {
            //                         confirmButton: 'btn btn-success'
            //                     }
            //                 });
            //             }
            //         },
            //     });
            // });

            $("#update_form").submit(function(e) {
                e.preventDefault(); // Prevent form submission
                $('#loader').show(); // Show the loader

                $.ajax({
                    url: "{{ url('users') }}" + "/" + rowid,
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        // Hide the loader
                        $('#loader').hide();

                        // Handle server response
                        if (response.errors) {
                            // If there are validation errors, display them using Swal.fire
                            $.each(response.errors, function(field, errors) {
                                $.each(errors, function(index, error) {
                                    Swal.fire({
                                        title: error,
                                        icon: 'error',
                                        customClass: {
                                            confirmButton: 'btn btn-danger'
                                        }
                                    });
                                });
                            });
                        } else if (response.error_message) {
                            // Display a generic error message
                            Swal.fire({
                                icon: 'error',
                                title: 'An error occurred! Please contact the administrator.',
                                customClass: {
                                    confirmButton: 'btn btn-danger'
                                }
                            });
                        } else {
                            // Success case: reload the data table, hide the modal, and display a success message
                            dataTable.ajax.reload();
                            $('#edit_modal').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'User has been updated successfully!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // Hide the loader
                        $('#loader').hide();

                        // Handle HTTP errors
                        if (jqXHR.status === 422) {
                            // This is a validation error, so parse the response JSON
                            const errors = jqXHR.responseJSON.errors;
                            $.each(errors, function(field, errorList) {
                                $.each(errorList, function(index, error) {
                                    Swal.fire({
                                        title: error,
                                        icon: 'error',
                                        customClass: {
                                            confirmButton: 'btn btn-danger'
                                        }
                                    });
                                });
                            });
                        } else {
                            // Handle other HTTP error statuses (e.g., 500, 404)
                            Swal.fire({
                                icon: 'error',
                                title: 'An unexpected error occurred. Please try again later.',
                                customClass: {
                                    confirmButton: 'btn btn-danger'
                                }
                            });
                        }
                    }
                });
            });

        });

        function userEdit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('users') }}" + "/" + rowid + "/edit",
                type: "GET",
                success: function(response) {
                    var role = response.role ? response.role.role_id : '';
                    $('#edit_name').val(response.user.name);
                    $('#edit_email').val(response.user.email);
                    // $('#edit_mobile').val(response.user.mobile);
                    // $('#edit_cnic').val(response.user.cnic);
                    // $('#edit_address').val(response.user.address);
                    $('#edit_train').val(response.user.train);
                    $('#edit_modal').modal('show');
                    $('#edit_role').val(response.role).trigger('change');
                    $('#edit_property_id').val(JSON.parse(response.user.property_id)).select2();
                }
            });
        }

        function status(id) {
            $(".toggle-class").change(function(event) {
                event.preventDefault();

                // Determine the new status
                var status = $(this).prop('checked') ? 1 : 0;

                // Send an AJAX request to update the status
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{ url('users_status') }}/" + id,
                    data: {
                        status: status,
                        id: id,
                        _token: $('meta[name="csrf-token"]').attr('content'),
                    },
                    beforeSend: function() {
                        // Show the loader before sending the request
                        $('#loader').show();
                    },
                    success: function(response) {
                        // Hide the loader on success
                        $('#loader').hide();

                        // Check the response and handle it accordingly
                        if (response.status_update) {
                            // Show a success toast
                            Swal.fire({
                                icon: 'success',
                                title: 'Status Updated Successfully!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        } else {
                            // Handle unexpected or unsuccessful status updates
                            Swal.fire({
                                icon: 'error',
                                title: 'Failed to update status.',
                                text: 'Please try again later.',
                                customClass: {
                                    confirmButton: 'btn btn-danger'
                                }
                            });
                        }

                        // Reload the data table
                        $('#dataTable').DataTable().ajax.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // Hide the loader on error
                        $('#loader').hide();

                        // Display an error message
                        Swal.fire({
                            icon: 'error',
                            title: 'An unexpected error occurred: ' + textStatus,
                            text: 'Please try again later.',
                            customClass: {
                                confirmButton: 'btn btn-danger'
                            }
                        });
                    }
                });
            });
        }

        // function deleteUser(id) {
        //     $.confirm({
        //         icon: 'far fa-question-circle',
        //         title: 'Confirm!',
        //         content: 'Are you sure you want to delete!',
        //         type: 'orange',
        //         typeAnimated: true,
        //         buttons: {
        //             Confirm: {
        //                 text: 'Confirm',
        //                 btnClass: 'btn-orange',
        //                 action: function() {
        //                     $.ajax({
        //                         url: "users/" + id,
        //                         type: "DELETE",
        //                         data: {
        //                             _token: "{{ csrf_token() }}"
        //                         },
        //                         success: function(response) {
        //                             if (response.error_message) {
        //                                 Toast.fire({
        //                                     icon: 'error',
        //                                     title: 'An error has been occured! Please Contact Administrator.'
        //                                 })
        //                             } else if (response.code == 300) {
        //                                 $.alert({
        //                                     icon: 'far fa-times-circle',
        //                                     title: 'Oops!',
        //                                     content: response.message,
        //                                     type: 'red',
        //                                     buttons: {
        //                                         Okay: {
        //                                             text: 'Okay',
        //                                             btnClass: 'btn-red',
        //                                         }
        //                                     }
        //                                 });
        //                             } else {
        //                                 dataTable.ajax.reload();
        //                                 Swal.fire({
        //                                     icon: 'success',
        //                                     title: 'User has been Deleted Successfully!',
        //                                     customClass: {
        //                                         confirmButton: 'btn btn-success'
        //                                     }
        //                                 });
        //                             }
        //                         }
        //                     });
        //                 }
        //             },
        //             cancel: function() {
        //                 $.alert('Canceled!');
        //             },
        //         }
        //     });
        // }
    </script>
@endsection

@endsection
