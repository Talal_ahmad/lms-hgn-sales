@extends('admin.layouts.master')
@section('title', 'Sales')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-2 mb-1"><span class="text-muted fw-light">Sales /</span> List</h4>
        <div class="card">
            <div class="card-datatable table-responsive">
                <table class="datatable table">
                    <thead class="border-top">
                        <tr>
                            <th class="not_include"></th>
                            <th>Order No.</th>
                            <th>Type</th>
                            <th>Date & Time</th>
                            <th>Update Date & Time</th>
                            <th>Customer Name</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
@section('scripts')

    <script>
        var propertyId = '{!! request()->route('property')->id !!}';
        $(document).ready(function() {
            var url = "{{ route('sales.index', ['property' => ':propertyId']) }}";
            url = url.replace(':propertyId', propertyId);
            dataTable = $('.datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'order_number'
                    },
                    {
                        data: 'type'
                    },
                    {
                        data: 'created_at',
                        render: function(data, type, full, meta) {
                            if (type === 'display' || type === 'filter') {
                                // Format the timestamp using the Date object
                                var date = new Date(data);
                                return date
                                    .toLocaleString(); // Adjust this based on your desired format
                            } else {
                                return data; // For other types (sorting, etc.), return the raw data
                            }
                        }
                    },
                    {
                        data: 'updated_at',
                        render: function(data, type, full, meta) {
                            if (type === 'display' || type === 'filter') {
                                var createdDate = new Date(full.created_at);
                                var updatedDate = new Date(full.updated_at);

                                // Compare created_at and updated_at (date and time)
                                if (createdDate.getTime() !== updatedDate.getTime()) {
                                    // If they are not equal, display updated_at with a badge
                                    return updatedDate.toLocaleString();
                                } else {
                                    // If they are equal, display an empty string
                                    return '-';
                                }
                            } else {
                                return data; // For other types (sorting, etc.), return the raw data
                            }
                        }
                    },
                    {
                        data: 'customer.name'
                    },
                    {
                        data: 'price'
                    },
                ],
                columnDefs: [{
                    // For Responsive
                    className: 'control',
                    searchable: false,
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0,
                    render: function render(data, type, full, meta) {
                        return '';
                    }
                }, {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function render(data, type, full, meta) {
                        if ('{{ auth()->user()->hasRole('Super Admin') }}') {
                            if (full.status === 'completed' || full.status === 'pending' || full
                                .status === 'process') {
                                return '<div class="d-inline-block text-nowrap">' +
                                    '<a href="javascript:;" class="text-body" onclick=edit(' +
                                    full
                                    .id + ')>' +
                                    '<i class="ti ti-edit"></i>' +
                                    '</a>';
                            }
                        } else {
                            if (full.status === 'pending' || full.status === 'process') {
                                return '<div class="d-inline-block text-nowrap">' +
                                    '<a href="javascript:;" class="text-body" onclick=edit(' +
                                    full
                                    .id + ')>' +
                                    '<i class="ti ti-edit"></i>' +
                                    '</a>';
                            } else {
                                return ''; // Return an empty string if not a super admin or order status is not 'pending'
                            }
                        }
                    }
                }],
                order: [
                    [2, 'desc']
                ],
                dom: '<"row mx-2"' + '<"col-md-2"<"me-3"l>>' +
                    '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
                    '>t' + '<"row mx-2"' + '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
                language: {
                    sLengthMenu: '_MENU_',
                    search: '',
                    searchPlaceholder: 'Search..'
                },
                // Buttons with Dropdown
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-label-primary dropdown-toggle mx-3',
                    text: '<i class="ti ti-logout rotate-n90 me-2"></i>Export',
                    buttons: [{
                        extend: 'print',
                        title: 'Users',
                        text: '<i class="ti ti-printer me-2" ></i>Print',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList !== undefined && item
                                            .classList.contains('user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        },
                        customize: function customize(win) {
                            //customize print view for dark
                            $(win.document.body).css('color', config.colors
                                    .headingColor)
                                .css('border-color', config.colors.borderColor).css(
                                    'background-color', config.colors.body);
                            $(win.document.body).find('table').addClass('compact').css(
                                    'color', 'inherit').css('border-color', 'inherit')
                                .css(
                                    'background-color', 'inherit');
                        }
                    }, {
                        extend: 'csv',
                        title: 'Users',
                        text: '<i class="ti ti-file-text me-2" ></i>Csv',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'excel',
                        title: 'Users',
                        text: '<i class="ti ti-file-spreadsheet me-2"></i>Excel',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'pdf',
                        title: 'Users',
                        text: '<i class="ti ti-file-text me-2"></i>Pdf',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'copy',
                        title: 'Users',
                        text: '<i class="ti ti-copy me-1" ></i>Copy',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be copy
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }]
                }, {
                    text: '<i class="ti ti-plus me-0 me-sm-1"></i><span class="d-none d-sm-inline-block" id="add-new-reservation">Add New</span>',
                    className: 'add-new btn btn-primary',
                }],
                // For responsive popup
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function header(row) {
                                var data = row.data();
                                return 'Details of ' + data['name'];
                            }
                        }),
                        type: 'column',
                        renderer: function renderer(api, rowIdx, columns) {
                            var data = $.map(columns, function(col, i) {
                                return col.title !==
                                    '' // ? Do not show row in modal popup if title is blank (for check box)
                                    ?
                                    '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' +
                                    col
                                    .columnIndex + '">' + '<td>' + col.title + ':' + '</td> ' +
                                    '<td>' + col.data + '</td>' + '</tr>' : '';
                            }).join('');
                            return data ? $('<table class="table"/><tbody />').append(data) : false;
                        }
                    }
                }
            });
        });

        function edit(id) {
            var url = "{{ route('sales.edit', ['property' => ':propertyId', 'sale' => ':saleId']) }}";
            url = url.replace(':propertyId', propertyId);
            url = url.replace(':saleId', id);
            window.location.replace(url);
        }
    </script>
@endsection
