@extends('admin.layouts.master')
@section('title', 'Room Condition')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-2 mb-1"><span class="text-muted fw-light">Room Status /</span> List</h4>

        <div class="card mt-1">
            <div class="m-3 rounded">
                <div class="container">
                    <div class="row">
                        <div>
                            <h3 class="ms-2 ps-1 mb-0 " style="font-weight: 700; font-size: 24px;">
                                Floors:
                            </h3>
                        </div>
                        <ul class="nav nav-pills mt-1 d-flex row-gap-2" id="pills-tab" role="tablist">
                            @foreach ($floors as $floor)
                                <li class="nav-item ms-4 mx-2 mb-0" role="presentation">
                                    <a class="nav-link mb-0 btn rounded-pill fw-bold position-relative {{ $loop->first ? ' active border-dark' : '' }}"
                                        id="floor-{{ $floor->id }}-tab" data-bs-toggle="pill"
                                        data-bs-target="#floor-{{ $floor->id }}" type="button" role="tab"
                                        aria-controls="floor-{{ $floor->id }}" aria-selected="{{ $loop->first }}">
                                        {{ $floor->name }}

                                        @if ($floor->rooms_need_repairing_count)
                                            <span
                                                class="position-absolute top-0 start-100 translate-middle badge rounded-pill text-bg-danger text-truncate py-1 px-2"
                                                style="max-width:40%;">
                                                {{ $floor->rooms_need_repairing_count }}
                                            </span>
                                        @endif
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div id="myDIV" class="row">
                        <div class="tab-content" id="pills-tabContent">
                            @foreach ($floors as $floor)
                                <div class="tab-pane fade show{{ $loop->first ? ' active' : '' }}"
                                    id="floor-{{ $floor->id }}" role="tabpanel">
                                    <div class="row mt-1">
                                        @foreach ($floor->rooms as $key => $room)
                                            @php
                                                $roomConditionsQuery = $room->roomConditions();
                                                $needRepairingOrDirty = $roomConditionsQuery
                                                    ->whereIn('status', ['need_repairing', 'dirty'])
                                                    ->exists();
                                            @endphp
                                            <div class="col-12 col-sm-6 col-md-3 col-lg-3 rounded"
                                                style="border-radius: 10px;">
                                                <div class="card p-0 rounded-4 mt-4 text-dark {{ $needRepairingOrDirty ? 'bg-danger text-white cursor-pointer' : '' }}"
                                                    data-bs-toggle="modal" data-bs-target="#add_modal_{{ $room->id }}"
                                                    style="border-radius: 10px;">
                                                    {{-- <div>
                                                        <div class="row">
                                                            <div class="col-7"></div>
                                                            <div class="col-4 d-flex justify-content-center rounded-bottom text-white mb-0 pb-0"
                                                                style="background-color: #56C456;">
                                                                <p class="mb-0 pb-1">Available</p>
                                                            </div>
                                                        </div>
                                                    </div> --}}
                                                    <div class="mt-3 mb-0 pb-0 d-flex justify-content-center text-"
                                                        style=" border-top-right-radius: 10px;border-top-left-radius:10px ">
                                                        <div class="d-flex justify-content-center text-"
                                                            style="font-family: Poppins; font-weight: 500; font-size: 16px;">
                                                            {{ $room->name }}
                                                        </div>
                                                    </div>
                                                    <div class="card-body m-0 p-0">
                                                        <div>
                                                            <div class="d-flex justify-content-center"
                                                                style="height: 190px;"><img
                                                                    src="{{ asset('images/roomBlue.png') }}"
                                                                    alt="img">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            @if ($needRepairingOrDirty)
                                                <!-- Add New Modal -->
                                                <div class="modal fade" id="add_modal_{{ $room->id }}" tabindex="-1"
                                                    aria-hidden="true">
                                                    <div
                                                        class="modal-dialog modal-dialog-centered1 modal-lg modal-simple modal-add-new-cc">
                                                        <div class="modal-content p-3 p-md-4 px-m-5 py-m-2">
                                                            <div class="modal-body">
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal"
                                                                    style="transform: translate(0px, 0px)"
                                                                    aria-label="Close"></button>
                                                                <div class="text-center mb-4">
                                                                    <h3 class="mb-2">Room Condition</h3>
                                                                </div>
                                                                @php
                                                                    $roomConditions = $roomConditionsQuery->get();
                                                                @endphp
                                                                @foreach ($roomConditions as $key => $roomCondition)
                                                                    @php
                                                                        $room_service_id =
                                                                            $roomCondition->room_service_id;
                                                                        $room_service_name =
                                                                            $room_services[$room_service_id - 1]->name;
                                                                    @endphp
                                                                    <div class="col-12 d-flex flex-wrap">
                                                                        <h4 class="col-6 mt-3 text-capitalize">
                                                                            {{ $room_service_name }}:
                                                                        </h4>
                                                                        <div class="col-12 border rounded px-3 py-2">
                                                                            {{ $roomCondition->description }}
                                                                        </div>

                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/ Add New Modal -->
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="sk-wave sk-primary" id="loader"
            style="display: none; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // var propertyId = '{!! request()->route('property')->id !!}';
        $(document).ready(function() {

        });
    </script>
@endsection
