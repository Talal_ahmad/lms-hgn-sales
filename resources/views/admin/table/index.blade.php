@extends('admin.layouts.master')
@section('title', 'Menu Items')
@section('styles')
    <style>
        .select2-container {
            z-index: 999999;
        }
    </style>
@endsection
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="d-flex justify-content-between align-content-center">
            <div>
                <h3 class="d-flex justify-content-center align-content-center mt-2"
                    style="font-family: Montserrat; font-weight: 600; font-size: 24px;">
                    Table Booking
                </h3>
            </div>
            <div>
                <button type="button" class="btn text-white " style="background-color: #7367f0; font-family: Inter;">
                    + Add New
                </button>
            </div>
        </div>
        <section class="m-3 rounded">
            <div class="container d-block  align-content-start">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-4 col-lg-4 p-0 rounded m-3" style="border-radius: 10px">
                        <div class="card p-0 rounded-4 mt-4 shadow-lg "
                            style="border-radius: 10px; width:329px; height: 300px;">
                            <div class=" p-2 d-flex justify-content-center "
                                style="background-color: #6366F1; border-top-right-radius: 10px;border-top-left-radius:10px">
                                <div class="d-flex justify-content-center text-white"
                                    style="font-family: Montserrat; font-weight: 500; font-size: 16px;">Table:1 &nbsp;
                                    &nbsp; <img src="{{ asset('/Images/Vector.png') }}" alt=""> </div>
                            </div>
                            <div class="card-body p-3">
                                <div style="height: 195px">
                                    <div class="card-text d-flex justify-content-between  mb-2">
                                        <div><span class="ps-1 text-muted font-family-poppins d-block text-truncate"
                                                style="font-family: Montserrat; font-weight: 500; font-size: 13px;">Shoaib
                                            </span></div>
                                        <div style="font-family: Montserrat; font-weight: 400; font-size: 12px;"> From 10:00
                                            - To 10:30 PM</div>
                                    </div>
                                    <div class="card-text d-flex justify-content-between  mb-2">
                                        <div><span class="ps-1 text-muted font-family-poppins d-block text-truncate"
                                                style="font-family: Montserrat; font-weight: 500; font-size: 13px;">Shoaib
                                            </span></div>
                                        <div style="font-family: Montserrat; font-weight: 400; font-size: 12px;"> From 10:00
                                            - To 10:30 PM</div>
                                    </div>
                                    <div class="card-text d-flex justify-content-between  mb-2">
                                        <div><span class="ps-1 text-muted font-family-poppins d-block text-truncate"
                                                style="font-family: Montserrat; font-weight: 500; font-size: 13px;">Shoaib
                                            </span></div>
                                        <div style="font-family: Montserrat; font-weight: 400; font-size: 12px;"> From 10:00
                                            - To 10:30 PM</div>
                                    </div>
                                    <div class="card-text d-flex justify-content-between  mb-4">
                                        <div><span class="ps-1 text-muted font-family-poppins d-block text-truncate"
                                                style="font-family: Montserrat; font-weight: 500; font-size: 13px;">Shoaib
                                            </span></div>
                                        <div style="font-family: Montserrat; font-weight: 400; font-size: 12px;"> From 10:00
                                            - To 10:30 PM</div>
                                    </div>
                                </div>

                                <div class="d-flex justify-content-between ">
                                    <div><button type="button" class="btn btn-success px-5 "
                                            style="background-color: #6366F1; font-family: Montserrat; font-weight: 400; font-size: 13px;">
                                            <img src="bxs_edit.png" alt=""> Edit
                                            Detail</button></div>
                                    <div><button type="button" class="btn btn-success"
                                            style="background-color: #6366F1; font-family: Montserrat; font-weight: 400; font-size: 13px;">Booking</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 p-0 rounded-4 m-3" style="border-radius: 10px">
                        <div class="card p-0 rounded-4 mt-4 shadow-lg" style="border-radius: 10px; width:329px">
                            <div class=" p-2 d-flex justify-content-center text-white"
                                style="background-color: #6366F1; border-top-right-radius: 10px;border-top-left-radius:10px ">
                                <div class="d-flex justify-content-center text-white"
                                    style="font-family: Montserrat; font-weight: 500; font-size: 16px;">Table:2</div> &nbsp;
                                &nbsp; <img src="{{ asset('/Images/Vector.png') }}" alt="">
                            </div>
                            <div class="card-body  p-3">
                                <div style="height: 190px;">
                                    <div class="d-flex justify-content-center"><img
                                            src="{{ asset('/Images/Table & Chair.png') }}" alt="img">
                                    </div>
                                </div>


                                <div class="d-flex justify-content-center ">
                                    <div><button type="button" class="btn btn-success px-5 "
                                            style="background-color: #6366F1; font-family: Montserrat; font-weight: 400; font-size: 15px;">Book
                                            Table</button></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

@endsection
@section('scripts')
@endsection
