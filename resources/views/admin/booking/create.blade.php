@extends('admin.layouts.master')
@section('title', 'Booking')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-2 mb-1"><span class="text-muted fw-light">Booking /</span> Add</h4>

        <div class="col-12 mb-3 pe-4 text-end">
            <button type="button" onclick="redirect()" class="btn btn-primary ms-auto">Back</button>
        </div>

        <div class="card">
            <div class="p-3 p-md-5 z-3">
                <div class="row">
                    <div class="col-8" id="customerDropdown">
                        <label class="form-label" for="customer_id">Customer:</label>
                        <select id="customer_id" name="customer_id" class="form-select select2"
                            data-placeholder="Select Company Reference">
                            <option value=""></option>
                            <!-- Populate dropdown options dynamically from your data -->
                            @foreach ($customers as $customer)
                                <option value="{{ $customer->id }}">
                                    {{ $customer->name . ' - ' . $customer->cnic . ' -  ' . $customer->phone_number }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-4 pt-4">
                        <button class="btn btn-info" style="display: none !important" id="seeHistoryBtn">See
                            History</button>
                    </div>
                </div>
                <div id="formContainer">
                    <form id="add_form" class="form row g-3"
                        action="{{ route('booking.store', ['property' => $property_id]) }}" method="POST">
                        @csrf
                        <h4 class="py-2 mb-1">Personal Information </h4>
                        <div class="col-6">
                            <label class="form-label" for="name"> Name</label>
                            <input type="text" class="form-control" id="name" name="name"
                                placeholder="Enter Name" required />
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="name"> Email</label>
                            <input type="Email" class="form-control" id="email" name="email"
                                placeholder="Enter Email" />
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label class="form-label" for="gender">Gender:</label>
                                <select id="gender" name="gender" class="form-select select2"
                                    data-placeholder="Select Gender" required>
                                    <option value=""></option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label class="form-label" for="guest_category">Guest Category:</label>
                                <select id="guest_category" name="guest_category" class="form-select select2"
                                    data-placeholder="Select Category" required>
                                    <option value=""></option>
                                    <option value="vip">VIP</option>
                                    <option value="regular">Regular</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-4">
                            <label class="form-label" for="customer_type_id">Customer Type</label>
                            <select id="customer_type_id" name="customer_type_id" class="form-select select2"
                                data-placeholder="Select Customer Type" required>
                                <option value=""></option>
                                @foreach ($CustomerTypes as $CustomerType)
                                    <option value="{{ $CustomerType->id }}">{{ $CustomerType->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-4">
                            <label class="form-label" for="nationality"> Nationality</label>
                            <input type="text" class="form-control" id="nationality" name="nationality"
                                placeholder="Enter Nationality" />
                        </div>
                        <div class="col-4">
                            <label class="form-label" for="cnic"> CNIC / passport</label>
                            <input type="text" class="form-control" id="cnic" name="cnic"
                                placeholder="Enter CNIC / passport" required />
                        </div>
                        <div class="col-4">
                            <label class="form-label" for="cnic_expired">CNIC / passport Expired Date</label>
                            <input type="date" class="form-control" id="cnic_expired" name="cnic_expired"
                                placeholder="Enter CNIC / passport Expired" />
                        </div>
                        <div class="col-4">
                            <div class="form-group mt-4">
                                <label class="form-label" for="phone_number">Mobile </label>
                                <input type="tel" class="form-control" id="phone" name="phone_number" placeholder="Enter Phone Number" required>
                                <input type="hidden" id="country_code" name="country_code">
                            </div>
                        </div>
                        <div class="col-4">
                            <label class="form-label" for="dob">Date of Birth:</label>
                            <input type="date" class="form-control" id="dob" name="dob">
                        </div>
                        <div class="col-4">
                            <label class="form-label" for="cnic"> Address</label>
                            <input type="text" class="form-control" id="address" name="address"
                                placeholder="Enter Address" />
                        </div>
                        <div class="col-12">
                            <label class="form-label" for="description"> Description</label>
                            <textarea class="form-control" id="description" name="description" placeholder="Enter Description"></textarea>
                        </div>
                        <hr>
                        <div class="col-12">
                            <h4 class="py-2 mb-1">Complementary Food</h4>
                            <div class="row row-bordered g-0">
                                <div class="col-xl-12 p-4">
                                    {{-- <div class="text-light small fw-medium">Default</div> --}}
                                    {{-- <div class="demo-inline-spacing"> --}}

                                    <label class="switch switch-success">
                                        <input type="checkbox" class="switch-input"
                                            name="complementary_food[breakfast]" />
                                        <span class="switch-toggle-slider">
                                            <span class="switch-on">
                                                <i class="ti ti-check"></i>
                                            </span>
                                            <span class="switch-off">
                                                <i class="ti ti-x"></i>
                                            </span>
                                        </span>
                                        <span class="switch-label">Breakfast
                                        </span>
                                    </label>

                                    <label class="switch switch-warning">
                                        <input type="checkbox" class="switch-input" name="complementary_food[lunch]" />
                                        <span class="switch-toggle-slider">
                                            <span class="switch-on">
                                                <i class="ti ti-check"></i>
                                            </span>
                                            <span class="switch-off">
                                                <i class="ti ti-x"></i>
                                            </span>
                                        </span>
                                        <span class="switch-label">Lunch</span>
                                    </label>

                                    <label class="switch switch-info">
                                        <input type="checkbox" class="switch-input" name="complementary_food[dinner]" />
                                        <span class="switch-toggle-slider">
                                            <span class="switch-on">
                                                <i class="ti ti-check"></i>
                                            </span>
                                            <span class="switch-off">
                                                <i class="ti ti-x"></i>
                                            </span>
                                        </span>
                                        <span class="switch-label">Dinner</span>
                                    </label>

                                    {{-- </div> --}}
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h4 class="py-2 mb-1">Pick & Drop</h4>
                        <div class="col-12">
                            <div class="form-check">
                                <input class="form-check-input" name="DropCheckbox" type="checkbox" id="DropCheckbox">
                                <label class="form-check-label" for="pick&DropCheckbox">Pick & Drop</label>
                            </div>
                        </div>
                        <div class="row mt-2" id="pick_DropFields" style="display: none;">
                            <div class="col-6">
                                <label class="form-label" for="pick">Pick Destination</label>
                                <input type="text" class="form-control" id="pick" name="pick"
                                    placeholder="Enter To Pick Destination" />
                            </div>
                            <div class="col-6">
                                <label class="form-label" for="drop">Drop Destination</label>
                                <input type="text" class="form-control" id="drop" name="drop"
                                    placeholder="Enter To Drop Destination" />
                            </div>
                            <div class="col-6 mt-2">
                                <label class="form-label" for="pickDateTime">Pick Date & Time</label>
                                <input type="datetime-local" class="form-control" id="pickDateTime" name="PickDateTime"
                                    placeholder="Enter Pick Date & Time" />
                            </div>
                            <div class="col-6 mt-2">
                                <label class="form-label" for="fare">Estimated Fare</label>
                                <input type="number" class="form-control" id="fare" name="fare"
                                    placeholder="Enter To Estimated Fare" />
                            </div>
                        </div>
                        <hr>
                        <h4 class="py-2 mb-1">Check Information </h4>
                        <div class="col-6">
                            <label class="form-label" for="fromDateTime">From Date & Time</label>
                            <input type="date" class="form-control" id="fromDateTime" name="fromDateTime"
                                placeholder="Enter date" required />
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="toDateTime">To Date & Time</label>
                            <input type="date" class="form-control" id="toDateTime" name="toDateTime"
                                placeholder="Enter Customer Name" required />
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label class="form-label" for="guest_counts">Guest Counts:</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" id="male_count" name="male_count" placeholder="Male" min="0" required>
                                    <input type="number" class="form-control" id="female_count" name="female_count"
                                        placeholder="Female" min="0" required>
                                    <input type="number" class="form-control" id="child_count" name="child_count"
                                        placeholder="Children" min="0">
                                    <input type="number" class="form-control" id="infant_count" name="infant_count"
                                        placeholder="Infants" min="0">
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label class="form-label" for="stay_length">Length of Stay (Nights):</label>
                                <input type="number" class="form-control" id="stay_length" name="stay_length"
                                    placeholder="Number of Nights" min="1">
                            </div>
                        </div>
                        <hr>
                        <h4 class="py-2 mb-1">Reference Information </h4>
                        <div class="col-6">
                            <label class="form-label" for="company_reference_id">Company Reference</label>
                            <select id="company_reference_id" name="company_reference_id" class="form-select select2"
                                data-placeholder="Select Company Reference">
                                <option value=""></option>
                                @foreach ($CompanyReferences as $CompanyReference)
                                    <option value="{{ $CompanyReference->id }}">{{ $CompanyReference->company_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="reference_id">Personal Reference</label>
                            <select id="reference_id" name="reference_id" class="form-select select2"
                                data-placeholder="Select Personal Referenc">
                                <option value=""></option>
                                @foreach ($References as $Reference)
                                    <option value="{{ $Reference->id }}">{{ $Reference->reference_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="hidden" name="room_id" value="{{ $id }}">
                        <input type="hidden" name="property_id" value="{{ $property_id }}">
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                            <button type="reset" class="btn btn-label-secondary btn-reset" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Modal for displaying customer history -->
        <div class="modal fade" id="customerHistoryModal" tabindex="-1" aria-labelledby="customerHistoryModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="customerHistoryModalLabel">Customer History</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body" id="customerHistoryContent">
                        <table id="customerHistoryTable" class="table">
                            <thead>
                                <tr>
                                    <th>Booking ID</th>
                                    <th>Name</th>
                                    <th>Room</th>
                                    <th>From DateTime</th>
                                    <th>To DateTime</th>
                                    <!-- Add more columns as needed -->
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Data will be populated here -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="sk-wave sk-primary" id="loader"
            style="display: none; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            var phoneInput = document.querySelector("#phone");
            var countryCodeInput = document.querySelector("#country_code");

            var iti = window.intlTelInput(phoneInput, {
                initialCountry: "auto",
                geoIpLookup: function(callback) {
                    fetch('https://ipinfo.io/json?token=YOUR_TOKEN_HERE') // replace with your token from ipinfo.io
                        .then(response => response.json())
                        .then(data => callback(data.country))
                        .catch(() => callback('us'));
                },
                utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js"
            });
                 
            phoneInput.addEventListener('countrychange', function () {
                countryCodeInput.value = iti.getSelectedCountryData().dialCode;
            });

            var propertyId = '{!! request()->route('property')->id !!}';
            // redirect back
            function redirect() {
                window.location.replace(`{{ url('properties') }}/${propertyId}/booking`);
            }
            $(document).ready(function() {
                // Hide the "See History" button by default
                $('#seeHistoryBtn').hide();

                $('#customer_id').on('change', function() {
                    const customerId = $(this).val();
                    var propertyId = '{!! request()->route('property')->id !!}';

                    // Make an AJAX request to fetch customer data based on the selected ID
                    $.ajax({
                        url: "{{ url('properties') }}" + "/" + propertyId + "/get-customer/" +
                            customerId,
                        type: "GET", // Change to GET method since you're retrieving data
                        data: {
                            id: customerId
                        }, // Pass the customer ID as data
                        success: function(response) {
                            console.log(response);
                            if (response.error) {
                                // Hide the form if no customer is found
                                $('#formContainer').hide();
                                // Hide the "See History" button if no customer is selected
                                $('#seeHistoryBtn').hide();
                            } else {
                                // Populate form fields with data from the response
                                const form = $('#add_form');
                                document.getElementById('name').value = response.name;
                                document.getElementById('email').value = response.email;
                                $('#gender').val(response.gender).trigger('change');
                                $('#guest_category').val(response.guest_category).trigger('change');
                                document.getElementById('nationality').value = response.nationality;
                                document.getElementById('cnic').value = response.cnic;
                                document.getElementById('cnic_expired').value = response
                                    .cnic_expired;
                                document.getElementById('phone').value = response
                                    .phone_number;
                                document.getElementById('dob').value = response.dob;
                                document.getElementById('address').value = response.address;

                                // Show the form
                                $('#formContainer').show();
                                $('#seeHistoryBtn').show();
                                // Show the "See History" button if a customer is selected
                            }
                        },
                        error: function(xhr, status, error) {
                            console.error('Error fetching customer data:', error);
                        }
                    });
                });

                $('#seeHistoryBtn').on('click', function() {
                    // Get the selected customer ID
                    const customerId = $('#customer_id').val();
                    var propertyId = '{!! request()->route('property')->id !!}';

                    // Make an AJAX request to fetch the customer's history
                    $.ajax({
                        url: "{{ url('properties') }}" + "/" + propertyId + "/customer-history/" +
                            customerId,
                        type: "GET",
                        data: {
                            id: customerId
                        },
                        success: function(response) {
                            $('#customerHistoryTable').DataTable({
                                destroy: true, // Destroy existing DataTable instance
                                data: response, // Use the response data
                                columns: [{
                                        data: 'id'
                                    }, // Adjust the column names as per your response data
                                    {
                                        data: 'name'
                                    },
                                    {
                                        data: 'room_name'
                                    },
                                    {
                                        data: 'fromDateTime'
                                    },
                                    {
                                        data: 'toDateTime'
                                    }
                                    // Add more columns as needed
                                ]
                            });
                            $('#customerHistoryModal').modal('show');
                        },
                        error: function(xhr, status, error) {
                            console.error('Error fetching customer history:', error);
                        }
                    });
                });

                var DropCheckbox = $('#DropCheckbox');
                var pick_DropFields = $('#pick_DropFields');

                // Initially hide shift booking fields
                pick_DropFields.hide();

                DropCheckbox.change(function() {
                    if (DropCheckbox.is(':checked')) {
                        // If checkbox is checked, show shift booking fields and hide regular booking fields
                        pick_DropFields.show();
                    } else {
                        // If checkbox is unchecked, hide shift booking fields and show regular booking fields
                        pick_DropFields.hide();
                    }
                });

                function calculateNights() {
                    const fromDateTime = $('#fromDateTime').val();
                    const toDateTime = $('#toDateTime').val();

                    if (fromDateTime && toDateTime) {
                        const fromDate = new Date(fromDateTime);
                        const toDate = new Date(toDateTime);
                        const timeDiff = toDate.getTime() - fromDate.getTime();
                        const daysDiff = timeDiff / (1000 * 3600 * 24);

                        if (daysDiff >= 0) {
                            $('#stay_length').val(Math.ceil(daysDiff));
                        } else {
                            $('#stay_length').val(0);
                        }
                    }
                }

                $('#fromDateTime, #toDateTime').on('change', calculateNights);
            });
        });

    </script>
@endsection
