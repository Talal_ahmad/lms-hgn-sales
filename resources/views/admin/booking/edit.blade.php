@extends('admin.layouts.master')
@section('title', 'Booking')
@section('styles')
    @vite(['resources/js/app.js'])
@endsection
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-2 mb-1"><span class="text-muted fw-light">Booking /</span> Edit</h4>

        <div class="col-12 mb-3 pe-4 text-end">
            <button type="button" onclick="redirect()" class="btn btn-primary ms-auto">Back</button>
        </div>
        {{-- <div class="content-body"> --}}
        <section class="basic-tabs-components">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="add-booking-tab" data-bs-toggle="tab" href="#add-booking"
                        aria-controls="add-booking" role="tab" aria-selected="true">Edit Booking</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="POS-tab" data-bs-toggle="tab" href="#POS" aria-controls="POS"
                        role="tab" aria-selected="false">POS</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="add-booking" aria-labelledby="add-booking-tab" role="tabpanel">
                    <div class="card">
                        <div class="p-3 p-md-5 z-3">
                            <form id="update_form" class="form row g-3"
                                action="{{ route('booking.update', ['property' => $property_id, 'booking' => $booking->booking_id]) }}"
                                method="POST">
                                @csrf
                                @method('PUT')
                                <h4 class="py-2 mb-1">Personal Information </h4>
                                <div class="col-6">
                                    <label class="form-label" for="name"> Name</label>
                                    <input type="text" class="form-control" value="{{ $booking->name }}" id="name"
                                        name="name" placeholder="Enter Name" required />
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="name"> Email</label>
                                    <input type="Email" class="form-control" value="{{ $booking->email }}" id="email"
                                        name="email" placeholder="Enter Email" />
                                </div>
                                <div class="col-4">
                                    <label class="form-label" for="gender">Gender:</label>
                                    <select id="gender" name="gender" class="form-select select2"
                                        data-placeholder="Select Gender" required>
                                        <option value=""></option>
                                        <option value="male" {{ $booking->gender == 'male' ? 'selected' : '' }}>Male
                                        </option>
                                        <option value="female" {{ $booking->gender == 'female' ? 'selected' : '' }}>
                                            Female</option>
                                        <option value="other" {{ $booking->gender == 'other' ? 'selected' : '' }}>Other
                                        </option>
                                    </select>
                                </div>
                                <div class="col-4">
                                    <label class="form-label" for="guest_category">Guest Category:</label>
                                    <select id="guest_category" name="guest_category" class="form-select select2"
                                        data-placeholder="Select Category" required>
                                        <option value=""></option>
                                        <option value="vip" {{ $booking->guest_category == 'vip' ? 'selected' : '' }}>VIP
                                        </option>
                                        <option value="regular"
                                            {{ $booking->guest_category == 'regular' ? 'selected' : '' }}>Regular
                                        </option>
                                        <option value="other" {{ $booking->guest_category == 'other' ? 'selected' : '' }}>
                                            Other
                                        </option>
                                    </select>
                                </div>
                                <div class="col-4">
                                    <label class="form-label" for="customer_type_id">Customer Type</label>
                                    <select id="customer_type_id" name="customer_type_id" class="form-select select2"
                                        data-placeholder="Select Customer Type" required>
                                        <option value=""></option>
                                        @foreach ($CustomerTypes as $CustomerType)
                                            <option value="{{ $CustomerType->id }}"
                                                {{ $booking->customer_type_id == $CustomerType->id ? 'selected' : '' }}>
                                                {{ $CustomerType->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-4">
                                    <label class="form-label" for="nationality">Nationality</label>
                                    <input type="text" class="form-control" id="nationality" name="nationality"
                                        placeholder="Enter Nationality" value="{{ $booking->nationality }}" />
                                </div>
                                <div class="col-4">
                                    <label class="form-label" for="cnic">CNIC</label>
                                    <input type="text" class="form-control" id="cnic" name="cnic"
                                        placeholder="Enter CNIC" value="{{ $booking->cnic }}" required />
                                </div>
                                <div class="col-4">
                                    <label class="form-label" for="cnic_expired">CNIC Expired Date</label>
                                    <input type="date" class="form-control" id="cnic_expired" name="cnic_expired"
                                        value="{{ date('Y-m-d', strtotime($booking->cnic_expired)) }}"
                                        placeholder="Enter CNIC Expired" />
                                </div>

                                <div class="col-4">
                                    <label class="form-label" for="phone_number">Mobile</label>
                                    <input type="text" class="form-control" id="phone_number" name="phone_number"
                                        placeholder="Enter Phone Number" value="{{ $booking->phone_number }}" required />
                                </div>

                                <div class="col-4">
                                    <label class="form-label" for="dob">Date of Birth:</label>
                                    <input type="date" class="form-control" id="dob" name="dob"
                                        value="{{ $booking->dob }}">
                                </div>

                                <div class="col-4">
                                    <label class="form-label" for="address">Address</label>
                                    <input type="text" class="form-control" id="address" name="address"
                                        placeholder="Enter Address" value="{{ $booking->address }}" />
                                </div>
                                <div class="col-12">
                                    <label class="form-label" for="description"> Description</label>
                                    <textarea class="form-control" id="description" name="description" placeholder="Enter Description">{{ $booking->description }}</textarea>
                                </div>
                                <hr>
                                <!-- Checkboxes for complementary food options -->
                                <div class="col-12">
                                    <h4 class="py-2 mb-1">Complementary Food</h4>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="breakfastCheckbox"
                                            {{ isset($complementaryFood['breakfast']) && $complementaryFood['breakfast'] == 'yes' ? 'checked' : '' }}
                                            name="complementary_food[breakfast]">
                                        <label class="form-check-label" for="breakfastCheckbox">Breakfast</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="lunchCheckbox"
                                            {{ isset($complementaryFood['lunch']) && $complementaryFood['lunch'] == 'yes' ? 'checked' : '' }}
                                            name="complementary_food[lunch]">
                                        <label class="form-check-label" for="lunchCheckbox">Lunch</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="dinnerCheckbox"
                                            {{ isset($complementaryFood['dinner']) && $complementaryFood['dinner'] == 'yes' ? 'checked' : '' }}
                                            name="complementary_food[dinner]">
                                        <label class="form-check-label" for="dinnerCheckbox">Dinner</label>
                                    </div>
                                </div>
                                <hr>
                                <div class="col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" name="DropCheckbox" type="checkbox"
                                            id="DropCheckbox" {{ isset($pickAndDrop) ? 'checked' : '' }}>
                                        <label class="form-check-label" for="DropCheckbox">Pick & Drop</label>
                                    </div>
                                </div>

                                <div id="pick_DropFields" class="mt-3">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="form-label" for="pick">Pick Destination</label>
                                                <input type="text" name="pick" id="pick" class="form-control"
                                                    value="{{ old('pick', $pickAndDrop->pick ?? '') }}"
                                                    placeholder="Enter To Pick Destination">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="form-label" for="drop">Drop Destination</label>
                                                <input type="text" name="drop" id="drop" class="form-control"
                                                    value="{{ old('drop', $pickAndDrop->drop ?? '') }}"
                                                    placeholder="Enter To Drop Destination">
                                            </div>
                                        </div>
                                        <div class="col-6 mt-2">
                                            <label class="form-label" for="pickDateTime">Pick Date & Time</label>
                                            <input type="datetime-local" class="form-control" id="pickDateTime"
                                                value="{{ old('fare', $pickAndDrop->pickDateTime ?? '') }}" name="PickDateTime"
                                                placeholder="Enter Pick Date & Time" />
                                        </div>
                                        <div class="col-6 mt-2">
                                            <div class="form-group">
                                                <label class="form-label" for="fare">Estimated Fare</label>
                                                <input type="number" name="fare" id="fare" class="form-control"
                                                    value="{{ old('fare', $pickAndDrop->fare ?? '') }}"
                                                    placeholder="Enter To Estimated Fare">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <!-- Check Information Section -->
                                <h4 class="py-2 mb-1">Check Information</h4>
                                <div class="row" id="regularBookingFields">
                                    <div class="col-6">
                                        <label class="form-label" for="fromDateTime" id="fromDateTimeLabel">From Date
                                            & Time</label>
                                        <input type="date" value="{{ $booking->fromDateTime }}" class="form-control"
                                            id="fromDateTime" name="fromDateTime" placeholder="Enter From Date" />
                                    </div>
                                    <div class="col-6">
                                        <label class="form-label" for="toDateTime" id="toDateTimeLabel">To Date &
                                            Time</label>
                                        <input type="date" value="{{ $booking->toDateTime }}" class="form-control"
                                            id="toDateTime" name="toDateTime" placeholder="Enter To Date" />
                                    </div>
                                </div>
                                <!-- Shift Booking Checkbox -->
                                <div class="col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" name="shiftCheckbox" type="checkbox"
                                            id="shiftCheckbox">
                                        <label class="form-check-label" for="shiftCheckbox">Shift Booking</label>
                                    </div>
                                </div>

                                <!-- Shift Booking Fields -->
                                <div class="row" id="shiftBookingFields" style="display: none;">
                                    <div class="col-6 mt-2">
                                        <label class="form-label" for="shift_room_id">Shift Room</label>
                                        <select id="shift_room_id" name="shift_room_id" class="form-select select2"
                                            data-placeholder="Select Room">
                                            <option value=""></option>
                                            @foreach ($rooms as $room)
                                                <option value="{{ $room->id }}">{{ $room->floor_name }} -
                                                    {{ $room->room_type_name }} - {{ $room->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-6">
                                        <label class="form-label" for="shift_toDateTime">To Date & Time</label>
                                        <input type="date" value="" class="form-control" id="shift_toDateTime"
                                            name="shift_toDateTime" placeholder="Enter To Date" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-label" for="guest_counts">Guest Counts:</label>
                                        <div class="input-group">
                                            <input type="number" class="form-control" id="female_count"
                                                name="female_count" placeholder="Female" min="0" required
                                                value="{{ $booking->female_count }}">
                                            <input type="number" class="form-control" id="male_count"
                                                name="male_count" placeholder="Male" min="0" required
                                                value="{{ $booking->male_count }}">
                                            <input type="number" class="form-control" id="child_count"
                                                name="child_count" placeholder="Children" min="0"
                                                value="{{ $booking->child_count }}">
                                            <input type="number" class="form-control" id="infant_count"
                                                name="infant_count" placeholder="Infants" min="0"
                                                value="{{ $booking->infant_count }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-label" for="stay_length">Length of Stay (Nights):</label>
                                        <input type="number" class="form-control" id="stay_length" name="stay_length"
                                            placeholder="Number of Nights" min="1" disabled
                                            value="{{ $booking->stay_length }}">
                                    </div>
                                </div>
                                <hr>
                                <h4 class="py-2 mb-1">Reference Information </h4>
                                <div class="col-6">
                                    <label class="form-label" for="company_reference_id">Company Reference</label>
                                    <select id="company_reference_id" name="company_reference_id"
                                        class="form-select select2" data-placeholder="Select Company Reference">
                                        <option value=""></option>
                                        @foreach ($CompanyReferences as $CompanyReference)
                                            <option value="{{ $CompanyReference->id }}"
                                                {{ $booking->company_reference_id == $CompanyReference->id ? 'selected' : '' }}>
                                                {{ $CompanyReference->company_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="reference_id">Personal Reference</label>
                                    <select id="reference_id" name="reference_id" class="form-select select2"
                                        data-placeholder="Select Personal Referenc">
                                        <option value=""></option>
                                        @foreach ($References as $Reference)
                                            <option value="{{ $Reference->id }}"
                                                {{ $booking->reference_id == $Reference->id ? 'selected' : '' }}>
                                                {{ $Reference->reference_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <input type="hidden" name="room_id" value="{{ $booking->room_id }}">
                                <input type="hidden" name="property_id" value="{{ $property_id }}">
                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                                    <button type="reset" class="btn btn-label-secondary btn-reset"
                                        data-bs-dismiss="modal" aria-label="Close">
                                        Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="POS" aria-labelledby="POS-tab" role="tabpanel">
                    <div class="row">
                        <div class="col-12">
                            <div class="container-xxl flex-grow-1 " id="app">
                                <pos-create-component :property={{ getProperty() }}
                                    :customer_id={{ $booking->id }}></pos-create-component>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        {{-- </div> --}}
        <div class="sk-wave sk-primary" id="loader"
            style="display: none; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var shiftCheckbox = $('#shiftCheckbox');
            var shiftBookingFields = $('#shiftBookingFields');
            var regularBookingFields = $('#regularBookingFields');
            var fromDateTime = $('#fromDateTime');
            var toDateTime = $('#toDateTime');

            // Initially hide shift booking fields
            shiftBookingFields.hide();

            shiftCheckbox.change(function() {
                if (shiftCheckbox.is(':checked')) {
                    // If checkbox is checked, show shift booking fields and hide regular booking fields
                    shiftBookingFields.show();
                    regularBookingFields.hide();

                    // Clear regular booking fields value
                    // fromDateTime.val('');
                    // // toDateTime.val('');
                } else {
                    // If checkbox is unchecked, hide shift booking fields and show regular booking fields
                    shiftBookingFields.hide();
                    regularBookingFields.show();

                    // Restore regular booking fields value from original booking data
                    fromDateTime.val('{{ $booking->fromDateTime }}');
                    toDateTime.val('{{ $booking->toDateTime }}');
                }
            });

            function calculateNights() {
                const fromDateTime = $('#fromDateTime').val();
                const toDateTime = $('#toDateTime').val();

                if (fromDateTime && toDateTime) {
                    const fromDate = new Date(fromDateTime);
                    const toDate = new Date(toDateTime);
                    const timeDiff = toDate.getTime() - fromDate.getTime();
                    const daysDiff = timeDiff / (1000 * 3600 * 24);

                    if (daysDiff >= 0) {
                        $('#stay_length').val(Math.ceil(daysDiff));
                    } else {
                        $('#stay_length').val(0);
                    }
                }
            }

            $('#fromDateTime, #toDateTime').on('change', calculateNights);
            var DropCheckbox = $('#DropCheckbox');
            var pick_DropFields = $('#pick_DropFields');

            // Initially hide pick & drop fields if pickAndDrop is not set
            if (!DropCheckbox.is(':checked')) {
                pick_DropFields.hide();
            }

            DropCheckbox.change(function() {
                if (DropCheckbox.is(':checked')) {
                    pick_DropFields.show();
                } else {
                    pick_DropFields.hide();
                }
            });
        });
    </script>

@endsection
