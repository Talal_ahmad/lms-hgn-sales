@extends('admin.layouts.master')
@section('title', 'Dashboard')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            @foreach ($Property as $property)
                <div class="col-lg-3 col-md-6 col-sm-6 mb-4">
                    <div class="card text-center">
                        <div class="card-body">
                            <h5 class="card-title"> {{ $property->name }}</h5>
                            <p class="card-text">{{ $property->address }}</p>
                            <a class="btn btn-primary" href="{{ route('property.dashboard', $property->id) }}">Details</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection
